﻿// ==UserScript==
// @name           GCR Timed Publishing 1 Review Page v2
// @description    Timed Publishing 1 - Review Page
// @namespace      http://www.geocaching.com/admin
// @version        02.03
// @icon           http://i.imgur.com/GP6D2vX.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Timed_Publishing_1_Review.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Timed_Publishing_1_Review.user.js
// @grant          GM_info
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_xmlhttpRequest
// @grant          GM_addStyle
// @grant          GM_listValues
// @grant          GM_registerMenuCommand
// @grant          GM_setClipboard
// ==/UserScript==

/*

Function:
 Timed Publishing 1 - Review Page
 */
 
// This script was retird in March 2018. 
// This script used to run on: @include        http*://*.geocaching.com/admin/review.aspx?*
// Reasons:
// (1) 98% of cases in which this script was used are now covered by the native Time Publish tool on the review page.