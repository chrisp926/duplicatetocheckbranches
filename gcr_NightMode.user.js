// ==UserScript==
// @name           GCR Night Mode
// @description    Changes color to night mode for Review Queue and Log Review Queue
// @namespace      http://www.geocaching.com/admin
// @version        01.02
// @icon           http://i.imgur.com/a94PYHJ.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/-/blob/master/gcr_NightMode.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/-/blob/master/gcr_NightMode.user.js
// @include        http*://*.geocaching.com/admin/queue.aspx*
// @include        http*://*.geocaching.com/admin/lqueue.aspx*
// ==/UserScript==

/*
Change Log:

*v1.02  2020-12-17  Fixed update and download URLs for now file name
*v1.01  2020-07-31  Added change log, added LRQ coverage, adjusted color codes, rename script
*v1.00  Original version for April Fools joke (GCR AwesomeScript)

*/

(function() {
    'use strict';
    var innerBody = document.getElementById("bd");
    innerBody.style.backgroundColor = "#95A5A6";
    document.body.style.backgroundColor = "#000000";
})();