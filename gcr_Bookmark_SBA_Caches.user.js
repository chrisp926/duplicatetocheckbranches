﻿/*
Geocaching Bookmark Scheduled Archives v2

Greasemonkey user script: see http://greasemonkey.mozdev.org

Function:
 Adds an 'Archive in X Days' link, next to the Create Bookmark button.
 When clicked, it adds the specified number of days to the current date,
 and inserts it (in ISO format, for correct sorting), ahead of the cache
 name. Adding the date in this fashion will cause the bookmark list to
 sort in the order of archive dates, with the nearest at the top. 
 Clicking the link also changes the drop-down bookmark selector to the
 bookmark list you have chosen.
 
Usage:
 You can set a primary and secondary link. Each can have its own bookmark
 and number of days to advance the date. Settings are keyed to your
 profile name. To set up, first choose the bookmark from the selector.
 Then, from the Tool/Greasemonkey/User Script menu, select either "Edit
 Primary Link" or "Edit Secondary Link". Enter a label for the link,
 and the number of days to advance the current date, separated by a comma. 
 This date will be prepended to the bookmark entry name. A value of 
 0 (zero) indicates the current date. A negative value will generate a
 date in the past.


Revision History:
* v02.03 2021-05-06 - Fixed signedIn reference to work with new header
* v02.02 2017-08-21 - Fixed typo in line 124 (replaced '/^' with '/*' )
* V02.01 2017-08-17 - SignedInAs fix for new header release
* v02.0 2017-08-02 - Version reset to 2.0 and name change to add v2.

// ==UserScript==
// @name           GC Bookmark Scheduled Archives v2
// @description    Adds scheduled archive date to bookmark entries.
// @version        02.03
// @scriptGuid     136b493c-e127-45cc-8e5e-0daf6f0ee64a
// @namespace      http://www.geocaching.com/admin
// @include        http*://*geocaching.com/bookmarks/mark.aspx*
// @grant          GM_info
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_xmlhttpRequest
// @grant          GM_addStyle
// @grant          GM_deleteValue
// @grant          GM_getResourceText
// @grant          GM_getResourceURL
// @grant          GM_listValues
// @grant          GM_log
// @grant          GM_registerMenuCommand
// @grant          GM_setClipboard
// @icon           http://i.imgur.com/kSEDzJc.png
// ==/UserScript==

*/

	//// --Version Checking-- //
	//fCheckScriptVersion();
	//// --Version Checking-- //



	//  Create tools options.
	GM_registerMenuCommand('Schedule Archive: Set Primary Link', fSetPrimaryLink);	
	GM_registerMenuCommand('Schedule Archive: Set Secondary Link', fSetSecondaryLink);

	

	//  Get currently signed-on geocaching.com profile.
	var SignedInX = document.getElementsByClassName('username')[0];
	if (SignedInX) {
        var SignedInAs =  SignedInX.firstChild.data.trim();
		SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');
	} else { return; }
	
	
	//  Get primary link settings:
	var PrimaryBookmark = GM_getValue('PrimaryBookmark_' + SignedInAs, '');
	var PrimaryLabel = GM_getValue('PrimaryLabel_' + SignedInAs, 'Archive in 30 days');
	var PrimaryDaysToAdd = GM_getValue('PrimaryDaysToAdd_' + SignedInAs, '30') - 0;

	//  Get secondary link settings:
	var SecondaryBookmark = GM_getValue('SecondaryBookmark_' + SignedInAs, '');
	var SecondaryLabel = GM_getValue('SecondaryLabel_' + SignedInAs, '');
	var SecondaryDaysToAdd = GM_getValue('SecondaryDaysToAdd_' + SignedInAs, '0') - 0;


	//  Get needed DOM elements.
	var e_Heading = document.getElementById("divContentMain");
	
	var e_BookmarkName = document.getElementById("ctl00_ContentBody_Bookmark_tbName");
	
	var e_BookmarkList = document.getElementById("ctl00_ContentBody_Bookmark_ddBookmarkList");

	// var e_BtnSubmit = document.getElementById("ctl00_ContentBody_Bookmark_btnSubmit");
	var e_BtnSubmit = document.getElementById("ctl00_ContentBody_Bookmark_btnCreate");
	
	// Check if adding or editing.
	// var Heading = e_Heading.firstChild.data;
	var Heading = e_Heading.childNodes[1].firstChild.data;
	if (Heading.search(/*Create.*/) == -1) {
		return;
	}	
	

	
	if (e_BookmarkName && e_BookmarkList && PrimaryBookmark.length && PrimaryLabel.length) {
		
		//  Get cache name.
		var CacheName = e_BookmarkName.value;
		
		//  Create span for links.
		var SA_LinkSpan = document.createElement("span");
		SA_LinkSpan.id = 'SA_LinkSpan';
		SA_LinkSpan.style.fontWeight = 'bold';
		e_BtnSubmit.parentNode.insertBefore(SA_LinkSpan, e_BtnSubmit.nextSibling);

		//  Add standard link to page.	
		SA_LinkSpan.appendChild(document.createTextNode(' \u00A0 '));
		var SA_Link = document.createElement("a");
		SA_Link.id = 'SA_Link';
		SA_Link.setAttribute('IncDays', PrimaryDaysToAdd);
		SA_Link.setAttribute('BookMark', PrimaryBookmark);
		SA_Link.href = 'javascript:void(0)';
		SA_Link.appendChild(document.createTextNode(PrimaryLabel));
		SA_Link.addEventListener('click', Link_clicked, false);
		SA_LinkSpan.appendChild(SA_Link);

		//  Add custom link to page.
		if (SecondaryBookmark.length && SecondaryLabel.length) {
			SA_LinkSpan.appendChild(document.createTextNode(' \u00A0 '));
			var CL_Link = document.createElement("a");
			CL_Link.id = 'CL_Link';
			CL_Link.setAttribute('IncDays', SecondaryDaysToAdd);
			CL_Link.setAttribute('BookMark', SecondaryBookmark);
			CL_Link.href = 'javascript:void(0)';
			CL_Link.appendChild(document.createTextNode(SecondaryLabel));
			CL_Link.addEventListener('click', Link_clicked, false);
			SA_LinkSpan.appendChild(CL_Link);
		}
	}



//------------------------------------- Functions ------------------------------------- //

	
	//  Process link click.
	function Link_clicked() {
		//  Get days to increment, bookmark to use.
		var IncDays = this.getAttribute('IncDays') - 0;
		var Bookmark = this.getAttribute('BookMark');
		//  Perform date calculation.
		var d = new Date();
		d.setDate(d.getDate() + IncDays)	
		var curr_date = d.getDate(d) + '';
		if (curr_date.length < 2) {curr_date = '0' + curr_date}	
		var curr_month = d.getMonth(d) + 1;	// Month is zero indexed.
		curr_month += '';
		if (curr_month.length < 2) {curr_month = '0' + curr_month}	
		var curr_year = d.getFullYear(d) + '';
		var NewDate = curr_year  + "-" + curr_month + "-" + curr_date;			
		//  Insert date before cache name. 
		e_BookmarkName.value = NewDate + ' . . . ' + CacheName;	
		//  Select bookmark.
		for (i = 0; i < e_BookmarkList.length; i++) {
			if (e_BookmarkList.options[i].text == Bookmark) {
				e_BookmarkList.selectedIndex = i;
				i = e_BookmarkList.length;
			}
		}
	}

	
	//  Customize Primary link.
	function fSetPrimaryLink() {
		var EditLabel = PrimaryLabel + ', ' + PrimaryDaysToAdd;	
		var newList = fSelectorText(e_BookmarkList);
		var Resp = prompt(
				'This will create a link using the following bookmark:\n' +
				'\n' +
				'\t\t' + newList + '\n' +
				'\n' +
				'If this is not the not the Bookmark you want to use, press\n' +
				'the CANCEL button.\n' +
				'Otherwise, enter the text you want to use as the link label,\n' +
				'followed by a comma, and the number of days to advance the date.',				
				EditLabel);

		if (Resp == null) {
			alert('Primary Link Editing canceled.');
		} else {
			Resp = Resp.trim();
			if (Resp.length) {				
				var RegEx1 = new RegExp('^(.*)\,\s*(.*?)$');
				var RegRslt = RegEx1.exec(Resp);
				if (RegRslt) {
					var newLabel = RegRslt[1];
					var newDays = RegRslt[2];
					GM_setValue('PrimaryBookmark_' + SignedInAs, newList);
					GM_setValue('PrimaryLabel_' + SignedInAs, newLabel);
					GM_setValue('PrimaryDaysToAdd_' + SignedInAs, newDays);
				} else {
					alert('Unable to parse your entry. Try again.');
					return;
				}
			} else {
				alert('Entry blank. Editing canceled');
				return;
			}
			alert('Primary Link has been edited.\n\n' +
					'This change will appear when the page is refreshed.\n');
		}
	}

	
	//  Customize Secondary link.
	function fSetSecondaryLink() {
		var EditLabel = SecondaryLabel + ', ' + SecondaryDaysToAdd;	
		var newList = fSelectorText(e_BookmarkList);
		var Resp = prompt(
				'This will create a link using the following bookmark:\n' +
				'\n' +
				'\t\t' + newList + '\n' +
				'\n' +
				'If this is not the not the Bookmark you want to use, press\n' +
				'the CANCEL button.\n' +
				'Otherwise, enter the text you want to use as the link label,\n' +
				'followed by a comma, and the number of days to advance the date.\n' +
				'\n' +
				'If you do not want to use a secondary link, blank out the entry field.',
				EditLabel);

		if (Resp == null) {
			alert('Secondary Link Editing canceled.');
		} else {
			Resp = Resp.trim();
			if (Resp.length) {				
				var RegEx1 = new RegExp('^(.*)\,\s*(.*?)$');
				var RegRslt = RegEx1.exec(Resp);
				if (RegRslt) {
					var newLabel = RegRslt[1];
					var newDays = RegRslt[2];
					GM_setValue('SecondaryBookmark_' + SignedInAs, newList);
					GM_setValue('SecondaryLabel_' + SignedInAs, newLabel);
					GM_setValue('SecondaryDaysToAdd_' + SignedInAs, newDays);
				} else {
					alert('Unable to parse your entry. Try again.');
					return;
				}
			} else {
				alert('Entry blank. Secondary link turned off.\n' +
						'This change will appear when the page is refreshed.');
				GM_setValue('SecondaryBookmark_' + SignedInAs, '');
				GM_setValue('SecondaryLabel_' + SignedInAs, '');
				GM_setValue('SecondaryDaysToAdd_' + SignedInAs, '');
				return;
			}
			alert('Secondary Link has been edited.\n\n' +
					'This change will appear when the page is refreshed.\n');
		}
	}
	
	
	//  Returns select object currently displayed text.
	function fSelectorText(object) {
		return object.options[object.selectedIndex].text;
	}
	
	
