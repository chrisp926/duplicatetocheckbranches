﻿// ==UserScript==
// @name           GCR Review Waypoint Links v2
// @description    Adds links to waypoint coordinates to open nearby caches and maps.
// @namespace      http://www.geocaching.com/admin
// @version        02.04
// @icon           http://i.imgur.com/a94PYHJ.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Waypoint_Links.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Waypoint_Links.user.js
// @include        http*://*.geocaching.com/admin/review.aspx?*
// @grant          GM_getValue
// @grant          GM_setValue
// @grant          GM_registerMenuCommand
// ==/UserScript==

/*
Function:
 Adds links to waypoint coordinates to open nearby caches and maps.

*/

// This script was retird in November 2017. 
// Reasons:
// (1) The script has been broken for at least a couple of months, possibly much longer. 
// (2) The functionality this script provides doesn't seem to increase the native functionality of the review page significantly.
// (3) It seems that no reviewers are relying on this script to review.