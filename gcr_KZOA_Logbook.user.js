﻿// ==UserScript==
// @name		GCR Kill Zone/Owner Action for Log Book v2
// @description	Adds Kill Zone and Owner Action Needed links to Log Book
// @namespace   http://www.geocaching.com/admin
// @version     02.02
// @require		md5.js
// @updateURL   https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_KZOA_Logbook.user.js
// @downloadURL https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_KZOA_Logbook.user.js
// @include		http*://*.geocaching.com/seek/cache_logbook.aspx?guid=*
// @grant		GM_openInTab
// @grant		GM_addStyle
// @grant	    GM_log
// ==/UserScript==

/*

Function:
 Adds Kill Zone and Owner Action Needed links to Log Book.

*/

	// Images.
	var imgsrcOanWarn =
		"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
		"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%1C%1C%1" +
		"1%86%86%0EFFEmmy%A2%A2%0A%C7%C7%02%C6%C6%23%C8%C8%B2%DE%DE%1" +
		"0%DE%DE%C1%DE%DE%CE%FF%00%FF%F7%F7%00%FF%FF%00%E7%E7%08%F0%F" +
		"0%B0%D9%D0%09%95%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%" +
		"FF%FF%00%12%DF%CE%CE%00%00%00%87IDATx%DAc%D8%BD%7B%F76)%20%C" +
		"1%00%C43%9AW%83%19%BBr%AFX%81%19%3B%7B%EFi%81%19%CB%AE%C59%A" +
		"F%061V%1C%609%5C%0Dd%EC%CAM%60%3Ch%05b%F460%9E%D3%022%96%DF%" +
		"3D%C8rWx5%C3%EEuw%0F%F2%DD%7D%0Cd%E4%DC%BD%91%7B%F7b5%C3%AE%" +
		"DC%BB%D7T%EE%DE%D0b%D8v%F6%EEE%C6%BBw%85%19%B6%DC%BD%7B%A3%E" +
		"3%EE%DD%24%86%ED%A1%AE%AE!!!%E6%0C%BB%8B%94%40%A0%1A%00g%FCK" +
		"pi%10%0B%13%00%00%00%00IEND%AEB%60%82";

	var imgsrcOanKill =
		"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
		"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%00%00%0" +
		"0BBBRRRkkk%8C%8C%8C%94%94%94%9C%9C%A5%A5%A5%A5%B5%B5%B5%BD%B" +
		"D%BD%DE%DE%DE%FF%00%FF%FF%F7%EF%FF%FF%FF%FF%FF%FF%FF%FF%FFi%" +
		"9FN%B7%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%00%1" +
		"2%DF%CE%CE%00%00%00yIDATx%DA%1D%CE1%0A%C30%0C%05%D0%3F%94%80" +
		"%25z%A0%0E%3DJ%C1%04%BA%E4%20%9D%DD%B5%93%A6%80E%0D%DAu%89t%" +
		"C9%60%C8%5D%EA%E8Oo%F8%F0%3F%1C%3E%02%875v%B7%CE%B8%7CN%D4%0" +
		"CK%02Pa%B8%D1%D1%85G%CB%D6%A65%B0%A8%3E%03%5D%BFk%20%A7%92%0" +
		"2%BF%FB%FB%16h%DBu%0FLBy%C0%A8%3Cf%12%06U%E8%0B%8B%C0%B2%03%" +
		"3E%8D%09%3Eo%98%FF%01%FD%00%3D%DD%89%40%B0%E9%00%00%00%00IEN" +
		"D%AEB%60%82";

	var imgsrcKzWarn =
		"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
		"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%1C%1C%1" +
		"1%86%86%0EFFEmmy%A2%A2%0A%C7%C7%02%C6%C6%23%C8%C8%B2%DE%DE%1" +
		"0%DE%DE%C1%DE%DE%CE%FF%00%FF%F7%F7%00%FF%FF%00%E7%E7%08%F0%F" +
		"0%B0%D9%D0%09%95%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%" +
		"FF%FF%00%12%DF%CE%CE%00%00%00%87IDATx%DAc%D8%BD%7B%F76)%20%C" +
		"1%00%C43%9AW%83%19%BBr%AFX%81%19%3B%7B%EFi%81%19%CB%AE%C59%A" +
		"F%061V%1C%609%5C%0Dd%EC%CAM%60%3Ch%05b%F460%9E%D3%022%96%DF%" +
		"3D%C8rWx5%C3%EEuw%0F%F2%DD%7D%0Cd%E4%DC%BD%91%7B%F7b5%C3%AE%" +
		"DC%BB%D7T%EE%DE%D0b%D8v%F6%EEE%C6%BBw%85%19%B6%DC%BD%7B%A3%E" +
		"3%EE%DD%24%86%ED%A1%AE%AE!!!%E6%0C%BB%8B%94%40%A0%1A%00g%FCK" +
		"pi%10%0B%13%00%00%00%00IEND%AEB%60%82";

	var imgsrcKzKill =
		"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
		"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%00%00%0" +
		"0BBBRRRkkk%8C%8C%8C%94%94%94%9C%9C%A5%A5%A5%A5%B5%B5%B5%BD%B" +
		"D%BD%DE%DE%DE%FF%00%FF%FF%F7%EF%FF%FF%FF%FF%FF%FF%FF%FF%FFi%" +
		"9FN%B7%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%00%1" +
		"2%DF%CE%CE%00%00%00yIDATx%DA%1D%CE1%0A%C30%0C%05%D0%3F%94%80" +
		"%25z%A0%0E%3DJ%C1%04%BA%E4%20%9D%DD%B5%93%A6%80E%0D%DAu%89t%" +
		"C9%60%C8%5D%EA%E8Oo%F8%F0%3F%1C%3E%02%875v%B7%CE%B8%7CN%D4%0" +
		"CK%02Pa%B8%D1%D1%85G%CB%D6%A65%B0%A8%3E%03%5D%BFk%20%A7%92%0" +
		"2%BF%FB%FB%16h%DBu%0FLBy%C0%A8%3Cf%12%06U%E8%0B%8B%C0%B2%03%" +
		"3E%8D%09%3Eo%98%FF%01%FD%00%3D%DD%89%40%B0%E9%00%00%00%00IEN" +
		"D%AEB%60%82";

	// Text checking function.
	String.prototype.startsWith = function(str) {
		return (this.indexOf(str) == 0);
	}

	String.prototype.contains = function(str) {
		return (this.indexOf(str) >= 0);
	}

	String.prototype.endsWith = function(str) {
		return (this.lastIndexOf(str) == this.length() - str.length());
	}

	// Post load processing.
	window.addEventListener("load", fPostLoad, false);
	window.addEventListener("focus", fReposition, false);

	var cacheIcon = document.getElementsByClassName('CacheTypeIcon')[0];
	var windowTitle = 'Geocaching > Logbook for ' + cacheIcon.alt;
	windowTitle += ': ' + cacheIcon.nextSibling.firstChild.data;
	document.title = windowTitle;

	// Get GUID for this cache.
	var imgTypeIcon = document.getElementsByClassName("CacheTypeIcon")[0];
	var lnkTypeIcon = imgTypeIcon.parentNode;
	if (lnkTypeIcon.nodeName != 'A') {
		GM_log('Unable to locate cache link. Script ending.');
		return;
	}

	var CacheGUID = UrlParm('guid', true, lnkTypeIcon.href);

	// Add links to Reviewer page.
	var cacheIconH2 = document.getElementsByClassName('CacheTypeIcon')[0].parentNode;
	var revLink = document.createElement('a');
	revLink.id = 'revLink';
	revLink.href = '/admin/review.aspx?guid=' + CacheGUID + '&nc=12&';
	revLink.target = '_blank';
	revLink.title = 'Open cache in Review Mode';
	revLink.style.marginLeft = '18px';
	revLink.style.fontSize = '12px';
	revLink.appendChild(document.createTextNode('Reviewer Page'));
	cacheIconH2.appendChild(revLink);

	var revLinkLogs = document.createElement('a');
	revLinkLogs.id = 'revLinkLogs';
	revLinkLogs.href = '/admin/review.aspx?guid=' + CacheGUID + '&nc=12#log_table';
	revLinkLogs.title = 'Open cache in Review Mode,\npositioned at Logs.';
	revLinkLogs.target = '_blank';
	revLinkLogs.style.marginLeft = '8px';
	revLinkLogs.style.fontSize = '12px';
	revLinkLogs.appendChild(document.createTextNode('[@logs]'));
	cacheIconH2.appendChild(revLinkLogs);

	var postLog = document.createElement('a');
	postLog.id = 'postLog';
	postLog.href = '/seek/log.aspx?wid=' + CacheGUID;
	postLog.target = '_blank';
	postLog.title = 'Submit a log for this cache.';
	postLog.style.marginLeft = '14px';
	postLog.style.fontSize = '12px';
	postLog.appendChild(document.createTextNode('Post Log'));
	cacheIconH2.appendChild(postLog);

	// Add styles for links.
	GM_addStyle(".OAN_Background { background-color: rgb(252, 250, 164); " +
			"margin-right: 10px; font-variant: small-caps; font-weight: bolder; } ");
	GM_addStyle(".KZ_Background { background-color: rgb(229, 246, 255); " +
			"margin-right: 10px; font-variant: small-caps; font-weight: bolder; } ");
	GM_addStyle(".LogDate {	font-weight: bold !important } ");

	// Create links.
	var oakzDiv = document.createElement('div');
	oakzDiv.id = 'oakzDiv';

	var spanOanWarn = document.createElement('span');
	spanOanWarn.id = 'spanOanWarn';
	oakzDiv.appendChild(spanOanWarn);

	var lnkOanWarn = document.createElement("A");
	lnkOanWarn.title = 'Disable a Problem Cache';
	lnkOanWarn.classList.add('OAN_Background');
	lnkOanWarn.href = '/bookmarks/mark.aspx?view=legacy&guid=' + CacheGUID + '&WptTypeID=2&oan=y';
	spanOanWarn.appendChild(lnkOanWarn);

	var imgOanWarn = document.createElement("IMG");
	imgOanWarn.border = '0';
	imgOanWarn.align = 'absmiddle';
	imgOanWarn.src = imgsrcOanWarn;
	lnkOanWarn.appendChild(imgOanWarn);
	lnkOanWarn.appendChild(document.createTextNode(' Warn Problem Cache'));
	lnkOanWarn.addEventListener('click', fWarn_Oan_clicked, false);

	var lnkOanKill = document.createElement("A");
	lnkOanKill.id = 'lnkOanKill';
	lnkOanKill.title = 'Archive a Problem Cache';
	lnkOanKill.classList.add('OAN_Background');
	lnkOanKill.href = '/seek/log.aspx?wid=' + CacheGUID + '&LogType=5&oan=y';
	spanOanWarn.appendChild(lnkOanKill);

	var imgOanKill = document.createElement("IMG");
	imgOanKill.border = '0';
	imgOanKill.align = 'absmiddle';
	imgOanKill.src = imgsrcOanKill;
	lnkOanKill.appendChild(imgOanKill);
	lnkOanKill.appendChild(document.createTextNode(' Kill Problem Cache'));

	var spanKzWarn = document.createElement('span');
	spanKzWarn.style.marginLeft = '20px';
	spanKzWarn.id = 'spanKzWarn';
	oakzDiv.appendChild(spanKzWarn);

	var lnkKzWarn = document.createElement("A");
	lnkKzWarn.title = 'Warn a Long-Disabled Cache';
	lnkKzWarn.classList.add('KZ_Background');
	lnkKzWarn.href = '/bookmarks/mark.aspx?view=legacy&guid=' + CacheGUID + '&WptTypeID=2&kz=y';
	spanKzWarn.appendChild(lnkKzWarn);

	var imgKzWarn = document.createElement("IMG");
	imgKzWarn.border = '0';
	imgKzWarn.align = 'absmiddle';
	imgKzWarn.src = imgsrcKzWarn;
	lnkKzWarn.appendChild(imgKzWarn);
	lnkKzWarn.appendChild(document.createTextNode(' Warn Disabled Cache'));
	lnkKzWarn.addEventListener('click', fWarn_Kz_clicked, false);

	var lnkKzKill = document.createElement("A");
	lnkKzKill.id = 'lnkKzKill';
	lnkKzKill.title = 'Archive Long-Disabled Cache';
	lnkKzKill.classList.add('KZ_Background');
	lnkKzKill.href = '/seek/log.aspx?wid=' + CacheGUID + '&LogType=5&kz=y';
	spanKzWarn.appendChild(lnkKzKill);

	var imgKzKill = document.createElement("IMG");
	imgKzKill.border = '0';
	imgKzKill.align = 'absmiddle';
	imgKzKill.src = imgsrcKzKill;
	lnkKzKill.appendChild(imgKzKill);
	lnkKzKill.appendChild(document.createTextNode(' Kill Disabled Cache'));

	// Add links to page.
	var AllLogs = document.getElementById("AllLogs");
	if (AllLogs) {
		insertAheadOf(oakzDiv, AllLogs);
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Remove avatars.
	function fPostLoad() {
		// Check if kill warned caches requested.
		if (UrlParm('killoawarn') == 'true') {
			var working = true;
			fPostLoad1();
		}
		if (UrlParm('killkzwarn') == 'true') {
			var working = true;
			fPostLoad2();
		}

		// If not an automated function, then remove avatars to regain screen real estate.
		if (!working) {
			var avatars = document.getElementsByClassName('logOwnerAvatar');
			var ic = avatars.length;
			for (var i = ic-1; i >= 0; i--) {
				avatars[i].parentNode.removeChild(avatars[i]);
			}
		}
	}

	function fPostLoad1() {
		// Get most recent log.
		var tblAllLogs = document.getElementById('AllLogs');
		if (tblAllLogs) {
			var rctRow = tblAllLogs.rows[0];
			if (rctRow) {
				var logContentDivs = rctRow.getElementsByClassName('LogContent');
				if (logContentDivs) {
				    var hashCheck = '#' + calcMD5('OAN' + CacheGUID);
					if (logContentDivs[0].innerHTML.contains(hashCheck)) {
						var logTypeDiv = rctRow.getElementsByClassName('LogType')[0];
						var logTypeImg = logTypeDiv.getElementsByTagName('IMG')[0];
						if (logTypeImg.alt == 'Temporarily Disable Listing') {
							var lnkOanKill = document.getElementById('lnkOanKill');
							var killhref = lnkOanKill.href;
							window.location.assign(killhref);
						} else {
							fIconAlert();
						}
					} else {
						fIconAlert();
					}
				} else {
					var st1 = setTimeout(fPostLoad1, 250);
				}
			} else {
				var st1 = setTimeout(fPostLoad1, 250);
			}
		} else {
			var st1 = setTimeout(fPostLoad1, 250);
		}
	}

	function fPostLoad2() {
		// Get most recent log.
		var tblAllLogs = document.getElementById('AllLogs');
		if (tblAllLogs) {
			var rctRow = tblAllLogs.rows[0];
			if (rctRow) {
				var logContentDivs = rctRow.getElementsByClassName('LogContent');
				if (logContentDivs) {
				    var hashCheck = '#' + calcMD5('KZ' + CacheGUID);
					if (logContentDivs[0].innerHTML.contains(hashCheck)) {
						var logTypeDiv = rctRow.getElementsByClassName('LogType')[0];
						var logTypeImg = logTypeDiv.getElementsByTagName('IMG')[0];
						if (logTypeImg.alt == 'Post Reviewer Note') {
							var lnkKzKill = document.getElementById('lnkKzKill');
							var killhref = lnkKzKill.href;
							window.location.assign(killhref);
						} else {
							fIconAlert();
						}
					} else {
						fIconAlert();
					}
				} else {
					var st1 = setTimeout(fPostLoad2, 250);
				}
			} else {
				var st1 = setTimeout(fPostLoad2, 250);
			}
		} else {
			var st1 = setTimeout(fPostLoad2, 250);
		}
	}

	// Change tab icon to alert.
	function fIconAlert() {
		var favicon = document.createElement('link');
		favicon.id = 'litmusFavicon';
		favicon.rel = 'shortcut icon';
		favicon.type ="image/x-icon"
		favicon.href = imgsrcOanWarn;
		allHead = document.getElementsByTagName('head');
		allHead[0].appendChild(favicon);
	}

	// Reposition to start of logs.
	function fReposition() {
		if (sessionStorage.getItem('repositioned') != 'true') {
			var oakzDiv = document.getElementById('oakzDiv');
			var poslt = fFindPos(oakzDiv);
			document.documentElement.scrollTop = poslt[1] - 3;
			sessionStorage.setItem('repositioned', 'true')
		}

		// Remove small-print class from log dates.
		var cacheDates = document.getElementsByClassName('LogDate');
		var ic = cacheDates.length;
		for (var i = ic -1; i >= 0; i--) {
			cacheDates[i].classList.remove('minorDetails');
		}
	}

	// Find true left (index 0) and top (index 1) position of any object.
	function fFindPos(obj) {
		var curleft = curtop = 0;
		if (obj.offsetParent) {
			do {
				curleft += obj.offsetLeft;
				curtop += obj.offsetTop;
			} while (obj = obj.offsetParent);
			return [curleft,curtop];
		}
	}

	// Bookmark this cache, in a separate tab.
	function fWarn_Oan_clicked() {
		GM_openInTab(curDomain() + '/seek/log.aspx?wid=' + CacheGUID + '&LogType=22&oan=y')
	}

	// Bookmark this cache, in a separate tab.
	function fWarn_Kz_clicked() {
		GM_openInTab(curDomain() + '/seek/log.aspx?wid=' + CacheGUID + '&LogType=68&kz=y')
	}

	// Returns the current domain, including http or https protocol, without a trailing '/';
	function curDomain() {
		return location.protocol + '//' + document.domain;
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}

		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}

		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element ahead of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}
