// ==UserScript==
// @name           GCR Reviewer Forums Fix v2
// @description    Reviewer Forums Fix
// @namespace      http://www.geocaching.com/admin
// @version        02.01
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Reviewer_Forums_Fix.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Reviewer_Forums_Fix.user.js
// @include        http*://forums.groundspeak.com/REV/*
// @include        http*://forums.groundspeak.com/GC/*
// @grant          GM_getValue
// @grant          GM_setValue
// @grant          GM_addStyle
// @grant          GM_registerMenuCommand
// ==/UserScript==

/*

Function:
 Enhancements to Reviewer Forums.

*/

	var imgNewTab =
			"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
			"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
			"AHUwAADqYAAAOpgAABdwnLpRPAAAABl0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuNt" +
			"CDrVoAAAHYSURBVDhPrdLfS5NRHMdx%2F4Xu7MarMurGCDISMyromdthZGEllFBdjNjTRY" +
			"I5imW4DXQLs0RI7UaJcERZbjNJM%2FuhiT%2FoB6iQ3Wi3QWtOn2ex7d052q5cy6Tv7Tnn" +
			"xeecz8nL%2Bx%2BTSqVQzpfFr%2Fj6buMPtVIXvIHrqRN3SMcb0Wl4VklgWMN5X1vdm3WQ" +
			"0zHaRc07wbUJgW%2FaRstHK52z5Tz4bCG0YKFpsPzPgFJVmpahdhwvBC4JeSZtNH%2Bw0j" +
			"4jkXkL%2FgE9N6CQaCzGmd7j6K8FdWOChok1RCXxRpy5gehSjOqHJ6ju17ggU%2BivfiMy" +
			"ibrO9XCOBN%2B%2BR7F3HuRksJSq3kOclch5iVySSVzjAq98k%2FrQlewJTMPEfqsC0VHE" +
			"sa5iKnvWkNNPNGp6fDiGBFflwzaH27IDj98%2B58jNbWh3dmG9u3sVqeguJfgmTDy%2BTP" +
			"%2F7l1wcFjTKitdVqOo71%2BqgzFvA4cB2iexEa9vDwPQImT%2BSTqf5tDBHoO%2FeekAt" +
			"2jx29ru3csBTQFljIaOzk8r9e2WZON2Dj9hbu4USdz5jc1MbP5gBEomfHK0voehyPiuG8e" +
			"%2BAgn7El9hXu4PI%2BMjmAIUYCZNT%2FioM09w8kkwmmZqf2RDwC9JYNuRjjH%2FGAAAA" +
			"AElFTkSuQmCC";

	// If main forums, set alert background color and terminate.
	var curUrl = document.location + '';
	if (curUrl.match(/.*?\/GC\/.*?/gi)) {
		// Set menu option to enter name.
		GM_registerMenuCommand('Set Reviewer Name for Alert...', fSetReviewerAlert);

		// Get currently signed-on geocaching.com profile.
		var e_LogIn = document.getElementById("user_link");
		if (e_LogIn) {
			var SignedInAs = e_LogIn.firstChild.firstChild.data;
			var ReviewerID = GM_getValue('ReviewerID', '');

			// If signed in as reviewer, set background to alert color.
			if (SignedInAs.trim() == ReviewerID.trim()) {
				GM_addStyle("body { background-color: yellow; } !important");
				GM_addStyle("body { background-image: none; } !important");
			}
		}
		return;
	}

	// Create row class for pinned/unpinned divider bar.
	GM_addStyle("tr.lastpinned { border-bottom: 5px solid rgb(182, 199, 219); }");

	// Let avatars show in correct proportions.
	var avatarList = document.evaluate(
			"//img[starts-with(@src, 'http://img.geocaching.com/user/avatar/')]",
			document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
	ik = avatarList.snapshotLength;
	for (var i = 0; i < ik; i++) {
		var avatar = avatarList.snapshotItem(i);
		if (avatar.getAttribute('class') != 'photo') {
			avatar.setAttribute('height', '');
			avatar.setAttribute('style', 'width: 75px');
		}
	}

	// Add divider line after last pinned topic.
	if (UrlParm("showforum", true)) {
		var SpanList = document.evaluate(
				"//span[(@class='topic_prefix' and contains(text(), 'PINNED'))]",
				document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
		var ic = SpanList.snapshotLength;
		if (ic) {
			var lstpinrow = SpanList.snapshotItem(ic-1);
			lstpinrow = lstpinrow.parentNode.parentNode;
			var nuclass = lstpinrow.getAttribute('class');
			nuclass += ' lastpinned';
			lstpinrow.setAttribute('class', nuclass.trim());
		}
	}

	// Links in posts open in new tabs.
	var LnkList = document.evaluate("//a[@class='bbc_url']",
		document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
	var ik = LnkList.snapshotLength;
	for (i = 0; i < ik; i++) {
		var lnk = LnkList.snapshotItem(i);
		lnk.target = '_blank';
	}

	// Add open-in-new-tab icons next to 1st-unread-post icons.
	LnkList = document.evaluate("//a[@title='Go to first unread post']",
		document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
	ik = LnkList.snapshotLength;
	for (i = 0; i < ik; i++) {
		var x = LnkList.snapshotItem(i);
		var nutab = x.cloneNode(true);
		nutab.firstChild.src = imgNewTab;
		nutab.firstChild.title += ' (new tab)';
		nutab.target = '_blank';
		nutab.firstChild.style.marginLeft = '3px';
		insertAfter(nutab, x)
	}

	// Add open-in-new-tab icons next to forum topic links.
	var LnkList = document.evaluate(
			"//a[starts-with(@id, 'tid-link-') or @title='Go to forum']",
			document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
	var ik = LnkList.snapshotLength;
	for (var i = 0; i < ik; i++) {
		var x = LnkList.snapshotItem(i);
		var nutab = x.cloneNode(false);
		nutab.target = '_blank';
		var newImg = document.createElement('img');
		newImg.src = imgNewTab;
		newImg.title = nutab.title += ' (new tab)';
		newImg.style.marginLeft = '3px';
		nutab.appendChild(newImg);
		insertAfter(nutab, x)
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Set Reviewer Name for Alert.
	function fSetReviewerAlert() {
		if (!SignedInAs) {
			alert('You must be signed on with the profile that you want to\n' +
					'have alerted.');
			return;
		}
		var ReviewerID = GM_getValue('ReviewerID', '');
		var xRtnVar = confirm('Click OK to alert when the profile ' + SignedInAs +
				' is signed on.\n\n' +
				'Click CANCEL to turn off alert for any profile.');
		if (xRtnVar) {
			GM_setValue('ReviewerID', SignedInAs);
			alert('Alert set for profile ' + SignedInAs + '.\n\n' +
					'You will need to refresh any open pages for the change \n' +
					'to take effect.');
		} else {
			GM_setValue('ReviewerID', '');
			alert('Alert canceled.');
		}
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element ahead of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}
