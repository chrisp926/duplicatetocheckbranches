﻿// ==UserScript==
// @name           GCR GMail Review Links v2
// @description    GMail Review Links
// @namespace      http://www.geocaching.com/admin
// @version        02.03
// @icon           http://i.imgur.com/iEntr3d.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Gmail_Review_Links.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Gmail_Review_Links.user.js
// @include        http://mail.google.com/*
// @include        https://mail.google.com/*
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_registerMenuCommand
// @require http://code.jquery.com/jquery-3.3.1.min.js
// ==/UserScript==

/*

02.03	2020-12-17	removed old redundant code
02.02	2018-08-31	update to work with new gmail layout (settings might not work)

Function:
 Adds Reviewer Page Links To Logs Viewed In GMail. Renames cache
 page links to "Cache Page".

*/

var oldHash = '';
var TimeOutID = 0;
var TimeOutID2 = 0;
var rnd = Math.random();

// Menu options to set optional values.
GM_registerMenuCommand('Turn Cache Page Auto-Open On/Off', fToggleAutoOpen);
GM_registerMenuCommand('Set Number of Logs for Cache Page', fSetNumOfLogs);
GM_registerMenuCommand('Set Number of Nearby Caches on Review Page', fSetNumOfCaches);

// Get optional values.
var Logs_to_Show = (GM_getValue('LogsToShow', '20'));
var Caches_To_Show = (GM_getValue('CachesToShow', '12'));
TimeOutID = window.setInterval(chkHash, 4000);

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

function chkHash() {
	if (location.hash != oldHash) {
		oldHash = location.hash;
		if (oldHash != '#inbox') {
			chklinks();
		}
	}
}

// Locate "info" links, convert to original format, and create review link.
function chklinks(){
	var CacheUrl;
	var changed = false;
	if (TimeOutID2) {
		clearInterval(TimeOutID2);
		TimeOutID2 = 0;
	}

	var cachePageLinks = $('a[href*="coord.info/GC"]');

	cachePageLinks.each(function() {
		if (!$(this).data('edited')) {
			$(this).text('Cache page');
			$(this).css('font-weight', 'bold');
			var reviewLink = fInfo2Ref($(this).attr('href'), 'R');
			$(this).after('<a href="' + reviewLink + '" data-edited="1" style="margin-left: 10px; font-weight: bold;">Review page</a>');
			$(this).data('edited', '1');
		}
	});

	if (!changed) {
		TimeOutID2 = window.setInterval(chklinks, 500);
	}
}

// Converts INFO cache page URL to old style Cache page or Review Page URL (Pass "C" or "R").
// http://coord.info/GC2AEJX
// http://www.geocaching.com/seek/cache_details.aspx?wp=GC2AEJX
// http://www.geocaching.com/admin/review.aspx?wp=GC2AEJX
function fInfo2Ref(infoUrl, pagetype) {
	var RtnVar = "";
	var i = infoUrl.toUpperCase().lastIndexOf("/GC");
	if (i >= 0) {
		switch(pagetype.toUpperCase())
		{
		case 'C':
			RtnVar = "http://www.geocaching.com/seek/cache_details.aspx?wp=" + infoUrl.substr(i + 1);
			break;
		case 'R':
			RtnVar = "http://www.geocaching.com/admin/review.aspx?wp=" + infoUrl.substr(i + 1);
			break;
		}
	}

	return RtnVar;
}
// Test for positive numeric-only data.
function isPositiveNumeric(value) {
	if (value == null || !value.toString().match(/^\d*\.?\d*$/)) {
		return false;
	} else {
		return true;
	}
}

// Toggle Auto-open value.
function fToggleAutoOpen() {
	var getVar = (GM_getValue('AutoOpen', 'Off'));
	var xRtnVar = confirm('Click OK to automatically open the cache page when \n' +
			'an email generated from that cache is displayed.\n\n' +
			'Click CANCEL to turn off this feature.\n\n' +
			'Currently this feature is turned ' + getVar + '.');

	if (xRtnVar) {
		autoOpen = 'On';
	} else {
		autoOpen = 'Off';
	}

	GM_setValue('AutoOpen', autoOpen);
	alert('Auto Open of cache pages has been turned ' + autoOpen + '.');
}

// Set number of logs for cache page.
function fSetNumOfLogs() {
	var getVar = (GM_getValue('LogsToShow', '20'));
	var newNum = prompt('Number of logs to initially display on cache page?', getVar);
	if (newNum != null) {
		newNum = newNum.trim();
		if (newNum && isPositiveNumeric(newNum)) {
			newNum = parseInt(newNum,10).toString();
			Logs_To_Show = newNum;
			GM_setValue('LogsToShow', newNum);
			alert('Logs to display has been set to ' + newNum + '.\n' +
					'You may need to refresh the page to see the change.');
		} else {
			alert('Invalid entry. Please use integers only');
			fSetNumOfLogs();
		}
	} else {
		alert('Update canceled');
	}
}

// Set number of nearby caches for review page.
function fSetNumOfCaches() {
	var getVar = (GM_getValue('CachesToShow', '12'));
	var newNum = prompt('Number of nearby caches to initially display on Review page?', getVar);
	if (newNum != null) {
		newNum = newNum.trim();
		if (newNum && isPositiveNumeric(newNum)) {
			newNum = parseInt(newNum,10).toString();
			Caches_To_Show = newNum;
			GM_setValue('CachesToShow', newNum);
			alert('Caches to display has been set to ' + newNum + '.\n' +
					'You may need to refresh the page to see the change.');
		} else {
			alert('Invalid entry. Please use integers only');
			fSetNumOfCaches();
		}
	} else {
		alert('Update canceled');
	}
}
