/*
  This file is deprecated in favor of gcr_Review_Page.txt.

  This file will be removed after a suitable deprecation period to allow people to upgrade to the newer version.
  It's advisable to make changes done in gcr_Review_Page.txt here as well during that period for maximum compatibility.
*/

[class^="icon-"], [class*=" icon-"] {
  display: inline-block;
  width: 14px;
  height: 14px;
  line-height: 14px;
  vertical-align: text-top;
  background-image: url("http://admin.geocaching.com/Content/images/glyphicons-halflings-white.png");
  background-position: 14px 14px;
  background-repeat: no-repeat;
  margin-top: 1px;
}

.icon-white, .nav-pills > .active > a > [class^="icon-"], .nav-pills > .active > a > [class*=" icon-"], .nav-list > .active > a > [class^="icon-"], .nav-list > .active > a > [class*=" icon-"], .navbar-inverse .nav > .active > a > [class^="icon-"], .navbar-inverse .nav > .active > a > [class*=" icon-"], .dropdown-menu > li > a:hover > [class^="icon-"], .dropdown-menu > li > a:hover > [class*=" icon-"], .dropdown-menu > .active > a > [class^="icon-"], .dropdown-menu > .active > a > [class*=" icon-"], .dropdown-submenu:hover > a > [class^="icon-"], .dropdown-submenu:hover > a > [class*=" icon-"] {
  background-image: url("images/glyphicons-halflings-white.png");
}

.icon-ok {
  background-position: -288px 0px;
}

.icon-exclamation-sign {
  background-position: 0px -120px;
}

.icon-remove {
  background-position: -312px 0px;
}
