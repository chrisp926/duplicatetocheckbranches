﻿// ==UserScript==
// @name           GCR Timed Publishing 4 Results Page v2
// @description    Timed Publishing 4 - Results Page
// @version        02.03
// @scriptGuid     49f15fab-c017-401c-8cca-756a398b47b3
// @namespace      http://www.geocaching.com/admin
// @grantZZZ       GM_info
// @grantZZZ       GM_getValue
// @grantZZZ       GM_openInTab
// @grantZZZ       GM_setValue
// @grantZZZ       GM_xmlhttpRequest
// @grantZZZ       GM_addStyle
// @grantZZZ       GM_listValues
// @grantZZZ       GM_registerMenuCommand
// @grantZZZ       GM_setClipboard
// @grantZZZ       GM_deleteValue
// @grantZZZ       GM_getResourceText
// @grantZZZ       GM_getResourceURL
// @grantZZZ       GM_log
// @icon           http://i.imgur.com/GP6D2vX.png
// ==/UserScript==
/* 

// This script was retird in March 2018. 
// This script used to run on: @include        http*://*geocaching.com/admin/CachePublish.aspx?IDS=*
// Reasons:
// (1) 98% of cases in which this script was used are now covered by the native Time Publish tool on the review page.


