﻿// ==UserScript==
// @name           GCR Word Highlighter v2
// @description    Highlights Specified Words In Cache Descriptions
// @namespace      http://www.geocaching.com/admin
// @version        02.02
// @icon           http://i.imgur.com/MleOpfw.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Word_Highlighter.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Word_Highlighter.user.js
// @include        http*://*.geocaching.com/admin/review.aspx?*
// @grant          GM_getValue
// @grant          GM_setValue
// @grant          GM_addStyle
// @grant          GM_registerMenuCommand
// ==/UserScript==

/*

Change Log:
*v2.02	2020-07-31	added Change Log & removed include for http*://*.geocaching.com/seek/cache_details.aspx?*
*v2.01	last version before implementing changelog

Function:
 Highlights specified words in the short & long descriptions.

*/

	// Set class for highlighting.
	GM_addStyle('.gchl { background-color: #FFFF00; color: #000000; ' +
			'font-weight: bold; display: inline; }');

	// Get currently signed-on geocaching.com profile.
	var SignedInAs = document.getElementById("ctl00_LoginUrl");
	if (!SignedInAs) { SignedInAs = document.getElementById('ctl00_LogoutUrl'); }
	var SignedInAs = SignedInAs.parentNode.childNodes[1].firstChild.data.trim();
	SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');

	// Set menu option for settings.
	GM_registerMenuCommand('Set Word Highlighter Options', fSetOptions);

	// Set holding strings for highlighting tags.
	var leftHilite = '\u0152\u0152\u0152';
	var rightHilite = '\u0153\u0153\u0153';

	// Get settings.
	var wordList = GM_getValue('WordList_' + SignedInAs, '');

	if (!wordList.length) { return; }

	var aWords = wordList.split(';');

	var shortDes = document.getElementById('ctl00_ContentBody_ShortDescription');
	var longDes = document.getElementById('ctl00_ContentBody_LongDescription');
	var shortDesRev = document.getElementById('ctl00_ContentBody_CacheDetails_ShortDesc');
	var longDesRev = document.getElementById('ctl00_ContentBody_CacheDetails_LongDesc');

	if (shortDes) { fHiLite(shortDes); }
	if (longDes) { fHiLite(longDes); }
	if (shortDesRev) { fHiLite(shortDesRev); }
	if (longDesRev) { fHiLite(longDesRev); }

//****************************************************************************************//
//                                                                                      //
//                       Functions                                                      //
//                                                                                      //
//****************************************************************************************//

	function fHiLite(desNode) {
		var jc = aWords.length;
		// Get all text nodes for this description node.
		var xPathSearch = ".//text()";
		var TextList = document.evaluate(
				xPathSearch,
				desNode,
				null,
				XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
				null);

		var ic = TextList.snapshotLength;
		for (var i = 0; i < ic; i++) {
			var xItem = TextList.snapshotItem(i);
			var z = xItem.data;
			for (var j = 0; j < jc; j++) {
				var chkWord = aWords[j].trim();
				if (chkWord) {
					chkWord = chkWord.replace(/\s+/g, '\\s\+');
					chkWord = chkWord.replace(/%/g, '\\w\*\?');
					var RegEx1 = new RegExp('\\b(' + chkWord + ')\\b', 'ig');
					z = z.replace(RegEx1, leftHilite + '$1' + rightHilite);
				}
			}
			xItem.data = z;
		}

		RegEx1 = new RegExp(leftHilite, 'g');
		var hold = desNode.innerHTML.replace(RegEx1, '<span class="gchl">');
		RegEx1 = new RegExp(rightHilite, 'g');
		desNode.innerHTML = hold.replace(RegEx1, '</span>');
	}

	//********************************************************************************//
	//                                                                                //
	//                               Settings Functions                               //
	//                                                                                //
	//********************************************************************************//

	function fSetOptions() {
		if (!fCreateSettingsDiv('Word Highlighter Script Options')) { return; }
		var divSet = document.getElementById('gm_divSet');

		// Insert code to create interface controls here.

		vDftVal = GM_getValue('WordList_' + SignedInAs, 'Lorem; ipsum; consectetur; adipiscing');
		vLabel = 'List of words or phrases to highlight. ' +
				'Separate words or phrases with a semicolon(;). Case is irrelevant.';
		var txtaraWordList = fCreateSetting('WordList', 'textarea', vLabel, '', vDftVal, '215px');
		var infoWordList = document.createElement('span');
		infoWordList.innerHTML =
			'<p>Available wildcards and expressions:</p>' +
			'<p> &bullet; Use a percent sign % as a general wildcard. "rest%" will match "rest", ' +
			'"restaurant", and "restroom".</p>' +
			'<p> &bullet; To make a single non-blank character optional, follow it with a question mark. For example, ' +
			'"trains?" will match "train" and "trains". "e-?mail" matches "email" and "e-mail".</p>' +
			'<p> &bullet; To make a sequence of letters optional, surround them with parentheses ( ), and follow ' +
			'with a question mark ?. For example, "trespass(ing)?" matches "trespass" and ' +
			'"trespassing". This is also the way to make a blank space optional. For example, ' +
			'"path( )?tag" will match the word "pathtag" and the phrase "path tag".</p>' +
			'<p> &bullet; To make groups of alternate sequences, surround them with parentheses, and ' +
			'separate each group with the vertical bar character |. For example "(over|under)pass" will match ' +
			'both "overpass" and "underpass". You could combine this with optional characters, so '+
			'that "(over|under)pass(es)?", matches "overpass", "underpass", "overpasses", and ' +
			'"underpasses".</p><br>';
		txtaraWordList.parentNode.insertBefore(infoWordList, txtaraWordList);

		// Create Save/Cancel Buttons.
		var ds_ButtonsP = document.createElement('div');
		ds_ButtonsP.setAttribute('class', 'SettingButtons');
		var ds_SaveButton = document.createElement('button');
		ds_SaveButton = document.createElement('button');
		ds_SaveButton.appendChild(document.createTextNode("Save"));
		ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
		var ds_CancelButton = document.createElement('button');
		ds_CancelButton.style.marginLeft = '6px';
		ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		ds_ButtonsP.appendChild(ds_SaveButton);
		ds_ButtonsP.appendChild(ds_CancelButton);
		divSet.appendChild(ds_ButtonsP);

		fOpenSettingsDiv();
	}

	function fSaveButtonClicked() {

		// Insert code to save settings here.
		var txtaraWordList = document.getElementById('txtaraWordList');
		GM_setValue('WordList_' + SignedInAs, txtaraWordList.value.trim());

		fCloseSettingsDiv();
	}

	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}

	//********************************************************************************//
	//                                                                                //
	//                          Settings Interface Functions                          //
	//                                                                                //
	//********************************************************************************//

	function fCreateSettingsDiv(sTitle) {

		// If div already exists, reposition browser, and show alert.
		var divSet = document.getElementById("gm_divSet");
		if (divSet) {
			var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			window.scrollTo(window.pageXOffset, YOffsetVal);
			alert('Edit Setting interface already on screen.');
			return false;
		}

		// Set styles for titles and elements.
		GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 6px; !important; } ' );
		GM_addStyle('.SettingLabel {font-weight: bold; margin-left: 6px !important;} ' ); +

		// Create blackout div.
		document.body.setAttribute('style', 'height:100%;');
		var divBlackout = document.createElement('div');
		divBlackout.id = 'divBlackout';
		divBlackout.setAttribute('style', 'z-index: 900; background-color: rgb(0, 0, 0); ' +
				'visibility: hidden; opacity: 0; position: fixed; left: 0px; top: 0px; '+
				'height: 100%; width: 100%; display: block;');

		// Create div.
		divSet = document.createElement('div');
		divSet.id = 'gm_divSet';
		divSet.setAttribute('style', 'position: absolute; z-index: 1000; visibility: hidden; ' +
				'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		popwidth = parseInt(window.innerWidth * 0.5);
		divSet.style.width = popwidth + 'px';

		// Create heading.
		var ds_Heading = document.createElement('div');
		ds_Heading.setAttribute('class', 'SettingTitle');
		ds_Heading.appendChild(document.createTextNode(sTitle));
		divSet.appendChild(ds_Heading);

		// Add div to page.
		var toppos =  parseInt(window.pageYOffset +  60);
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.top = toppos + 'px';
		divSet.style.left = leftpos + 'px';
		divSet.setAttribute('YOffsetVal', window.pageYOffset);

		// Add blackout and setting divs.
		document.body.appendChild(divBlackout);
		document.body.appendChild(divSet);
		window.addEventListener('resize', fSetLeftPos, true);

		return true;
	}

	/*
	vID = GetSetting ID, and type + vID is control ID.
	vType = checkbox (input + type.checkbox), text (input), textarea, select
	vLabel = Text for control label.
	vTitle = Title text for input control and label, or vLabel if not specified.
	vDftVal = May be text, true/false, or matches a vSelVal array element.
	vSize = Length of input (text) box, or height of textarea, in pixels.
	vSelVal = Array of select option values.
	vSelTxt = Array of select option text.
	*/
 	function fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt) {
		var divSet = document.getElementById('gm_divSet');
		var kParagraph = document.createElement('p');
		kParagraph.id = 'p' + vID;
		kParagraph.setAttribute('class', 'SettingElement');
		switch (vType) {
			case 'checkbox':
				var kElem = document.createElement('input');
				kElem.id = 'cb' + vID;
				kElem.type = 'checkbox';
				kElem. checked = vDftVal;
				break;
			case 'text':
				if (!vSize) { vSize = '150px'; }
				var kElem = document.createElement('input');
				kElem.id = 'txt' + vID;
				kElem.style.width = vSize;
				kElem.value = vDftVal;
				kElem.addEventListener('focus', fSelectAllText, true);
				break;
			 case 'textarea':
				if (!vSize) { vSize = '80px'; }
				var kElem = document.createElement('textarea');
				kElem.id = 'txtara' + vID;
				kElem.style.width = '100%';
				kElem.style.height = vSize;
				kElem.value = vDftVal;
				break;
			case 'select':
				var kElem = document.createElement('select');
				kElem.id = 'sel' + vID;
				if (vSelVal) {
					if (vSelVal.constructor == Array) {
						for (var i in vSelVal) {
							var kOption = document.createElement('option');
							kOption.value = vSelVal[i];
							kOption.selected = (vSelVal[i] == vDftVal);
							if ((vSelTxt.constructor == Array) && (vSelTxt.length >= i-1)) {
								var kTxtNode = vSelTxt[i];
							} else {
								var kTxtNode = vSelVal[i];
							}
							kOption.appendChild(document.createTextNode(kTxtNode));
							kElem.appendChild(kOption);
						}
					}
				}
				break;
		}

		var kLabel = document.createElement('label');
		kLabel.setAttribute('class', 'SettingLabel');
		kLabel.setAttribute('for', kElem.id);
		kLabel.appendChild(document.createTextNode(vLabel));
		if (!vTitle) { vTitle = vLabel; }
		kElem.title = vTitle;
		kLabel.title = kElem.title;

		if (vType == 'textarea') {
			kParagraph.appendChild(kLabel);
			kParagraph.appendChild(document.createElement('br'));
			kParagraph.appendChild(kElem);
		} else {
			kParagraph.appendChild(kElem);
			kParagraph.appendChild(kLabel);
		}
		divSet.appendChild(kParagraph);

		return kElem;
	}

	// Resize/reposition on window resizing.
	function fSetLeftPos() {
		var divSet = document.getElementById('gm_divSet');
		if (divSet) {
			var popwidth = parseInt(window.innerWidth * .5);
			divSet.style.width = popwidth + 'px';
			var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
			divSet.style.left = leftpos + 'px';
		}
	}

	function fOpenSettingsDiv() {
		// Add blackout and setting divs.
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		divSet.style.visibility = 'visible';
		divBlackout.style.visibility = 'visible';
		var op = 0;
		var si = window.setInterval(fShowBlackout, 40);

		// Function to fade-in blackout div.
		function fShowBlackout() {
			op = op + .05;
			divBlackout.style.opacity = op;
			if (op >= .75) {
				window.clearInterval(si);
			}
		}
	}

	function fCloseSettingsDiv() {
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		divSet.parentNode.removeChild(divSet);
		divBlackout.parentNode.removeChild(divBlackout);
		window.removeEventListener('resize', fSetLeftPos, true);
	}

	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}

	function fSelectAllText() {
		this.select();
	}
