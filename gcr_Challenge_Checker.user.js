// ==UserScript==
// @name        GCR Challenge Checker v2
// @description Auto-Run Challenge Checkers
// @namespace   http://www.geocaching.com/admin
// @version     02.01
// @run-at		document-idle
// @updateURL   https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Challenge_Checker.user.js
// @downloadURL https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Challenge_Checker.user.js
// @include     http*://project-gc.com/Challenges/*?u=*
// ==/UserScript==

/*

Function:
  Auto-Run Challenge Checkers

*/

	var user = UrlParm('u', true);
	var wpid = UrlParm('wpid', true).toUpperCase();
	var lr = '\n';

	user = window.atob(decodeURIComponent(user));
	if (user) {
		var pfid = document.getElementById('profile_name');
		var runbtn = document.getElementById('runChecker');
		if (pfid && runbtn) {
			pfid.value = decodeURIComponent(user);
			runbtn.click();
		}
	}

	if (wpid) {
		var tagdiv = document.getElementsByClassName('cacheGroup')[0];
		if (tagdiv) {
			var tagid = tagdiv.getAttribute('data-gccode').trim().toUpperCase();
			if (tagid) {
				if (tagid != wpid) {
					alert(`Checker Waypoint of ${tagid} does not${lr}match Cache Waypoint of ${wpid}.`);
				}
			}
		}
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'};
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}
