// ==UserScript==
// @author       srebeelis
// @name         gcr_LitmusConflictCopy
// @namespace    gcr_LitmusConflictCopy
// @version      01.03
// @updateURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_LitmusConflictCopy.user.js
// @downloadURL  https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_LitmusConflictCopy.user.js
// @icon         https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/Icons/gcr_LitmusConflictCopy.png
// @include      *://admin.geocaching.com/LitmusTest/*
// @include      *://*.geocaching.com/admin/review.aspx*
// @description  Adds a link to the litmus page and the litmus section of the cache review page for each cache which has a proximity problem which copies the gc code and name of the cache to the clipboard
//
// Change history
// ==============
//  v01.00  2018-03-10   initial version, migrated from GcLitmusConflictCopy WebExtension by srebeelis
//  v01.01  2018-03-16   adding support for the litmus section on the review page
//  v01.02  2018-03-16   adding support for Ctrl-click to copy markdown link
//  v01.03  2018-03-16   deactivating script and telling users to uninstall script
//
// ==/UserScript==

(function() {
    'use strict';

    alert('gcr_LitmusConflictCopy functionality will be migrated into GCR Review V2 script.\n\nPlease uninstall the gcr_LitmusConflictCopy script');
    return;

    var DEBUG=false; // true enables logging
    var log = function(){
        if(DEBUG){
            console.log.apply(console, arguments);
        }
    };

    var imgChainLink = "data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%10%00%00%00%10%08%06%00%00%00%1F%F3%FFa%00%00%00%01sRGB%00%AE%CE%1C%E9%00%00%00%04gAMA%00%00%B1%8F%0B%FCa%05%00%00%00%09pHYs%00%00%0E%C3%00%00%0E%C3%01%C7o%A8d%00%00%00%19tEXtSoftware%00paint.net%204.0.134%03%5Bz%00%00%01%7CIDAT8O%A5%8E%BFK%02a%18%C7%85%20%0E%FA%07%FA%0B%0E%1A%A4%F4%0FP%5Cn%F0G%A0%CB9E%94r%A4%D6%21%16%08%BA%9Cp%8B%E0%A0%82n%CE%22%E8%184%DC%E0%E1%EC%D4%21%0EW%08%0D.iH%60E%F0%F4~_%A8%E9%1D%3A%3C%F8%DE%F1%7C%BE%9F%E7%E1%7CD%B4S%84%D0K%84%D0K%84%D0K%84%D0K%84%D0K%84%D0K%F8%2B%14%0A%1D%E4r%B9%3B%D34W%BD%5E%8F%AA%D5%EA%E2B%D3%1F%D1%15%0A%85C6%BB%E0%E8%E1%C1%FF%3B%10%0C%06%F7%E3%F1x%BD%D9l%D2r%B9%24%D7ui%3C%1E%93a%18O%10%2A%95%8A%3D%9F%CF%3F%C0%27%93%09%C1%83%8F%3D~%20%10%08%9C%96J%A57%C7qV%E9t%FA%3E%16%8B%7Dg%B3Yj%B5Z7%10t%5D%27%CC%E0%E8%FB%FD%FE%02%3E%F6~%FF%A0%5E%AB%D5%B6%EDv%5Bc%E2%26%1C%0EK%96eI%D3%E9T%82%60%DB%B64%1C%0E%25%C6e%D6%BF%C0%83%8F%3D~%C0%EF%F7_i%9A%B6%EEt%3A%B7%A9T%EA%21%12%89%7C%29%8AB%C9d%92%F5%E4c%DFc%CC%E0%E8%E1%C1%C7%1E%3Fp%C4%9ED%221k4%1A%EB%C1%60%F0%3A%1A%8D%A8%DB%EDR%B1X%E4%07%CA%E52%9F%C1%D1%C3%83%8F%3D~%40%96%E5%3D%F6%7B%27%D1h%F49%93%C9%BC%E7%F3%F9-%CB%A7%AA%AAg%10%CE%2F%AFgl%DE%80%A3%87%07%1F%7B%FC%00%5E%BBD%08%FF%1F%F2%FD%00%A9u%9E%E8%12%80%94%99%00%00%00%00IEND%AEB%60%82";

    log("~~~ GCRLCC: GcLitmusConflictCopy ~~~");
    if (window.top === window.self) {
        var itemCount = addGcConflictCopyToLitmusPage(0);
        itemCount = addGcConflictCopyToLitmusSectionOfReviewPage(itemCount);
    } else {
        log("GCRLCC: No-op because we're in a frame");
    }

    //-----------------------------------------------------------------------------
    function addGcConflictCopyToLitmusPage(itemCountOffset) {
        var elements = document.getElementsByClassName("ResultContainer");

        //    <div class=ResultContainer>
        //      <ul class=unstyled>
        //        <li>
        //          <a href=...seek/cahche_details>FNxxxx</a>
        //           -
        //          <a href=...seek/cache_details>Final Location</a>
        //          Waypoint too close to
        //          <a href=...seek/cache_details>Final Location</a>
        //          Waypoint <
        //          <img src=... icon>
        //           o
        //          <a class="" href="....seek/cache_details">Cache Name</a>
        //           o
        //          <a href=="...admin/review...">GC-Code</a>
        //          -
        //          <img src=... direction>
        //          xx.x m NW
        //        </li>
        //      </ul>
        //    <div>
        var itemCount = itemCountOffset;

        for (var element of elements) {
            log("GCRLCC: - element=", element);
            if ("Errors" == element.parentNode.className ) {
                log("GCRLCC:   --> parsing error results for distance conflicts");
                if (element.childNodes.length == 3) {
                    var ul = element.childNodes[1];
                    if ("UL" == ul.tagName && "unstyled" == ul.className) {
                        for (var item of ul.childNodes) {
                            if ("LI" == item.tagName) {
                                log("GCRLCC:       - item=", item);

                                // states of i: 0=waiting for "Waypoint <"
                                //              1=Cache Name contained in 3 parts
                                //              3=this contains cache name
                                //              5=this part contains GC code
                                var i = 0;
                                var cacheName = null;
                                var gcCode = null;
                                var base = null;
                                for (var part of item.childNodes) {
                                    log("GCRLCC:         - part=", part);
                                    if (" Waypoint < " == part.nodeValue ||
                                        " too close to cache " == part.nodeValue ||
                                        " Waypoint too close to cache " == part.nodeValue) {
                                        if (0 == i) {
                                            log("GCRLCC:           - Waypoint > string detected, next part is cache name");
                                            i=1;
                                        } else {
                                            log("GCRLCC:           - Waypoint > string detected in unexpected state i=", i);
                                        }
                                    } else if (3 == i) {
                                        if ("A" == part.nodeName && part.href.includes("www.geocaching.com/seek/cache_details")) {
                                            i = i+1;
                                            cacheName = part.innerText;
                                            log("GCRLCC:           - detected cache name: ", cacheName);
                                        } else {
                                            log("GCRLCC:           - cache name not detected with index i=", i);
                                        }
                                    } else if (5 == i) {
                                        if ("A" == part.nodeName && part.href.includes("www.geocaching.com/admin/review")) {
                                            i = 0;
                                            gcCode = part.innerText;
                                            log("GCRLCC:           - detected gc code: ", gcCode);
                                            base = part;
                                        } else {
                                            log("GCRLCC:           - gc code not detected with index i=", i);
                                        }
                                    } else if (0 != i) {
                                        i = i+1;
                                    }
                                }

                                if (cacheName != null && gcCode != null) {
                                    log("GCRLCC:         -> detected ["+gcCode+"] ["+cacheName+"], base=", base);
                                    cacheName = cacheName.replace(gcCode+' ', '');
                                    cacheName = cacheName.replace(gcCode, '');
                                    log("GCRLCC:         -> cleaned up cache name ["+gcCode+"] ["+cacheName+"], base=", base);

                                    itemCount = itemCount + 1;
                                    addLinkButton(base, itemCount, cacheName, gcCode);
                                } else {
                                    log("GCRLCC:         -> failed to extract cache information");
                                }
                            }
                        }
                    } else {
                        log("GCRLCC:       did not find UL element", ul);
                    }
                } else {
                    log("GCRLCC:       did not find 3 childNodes while looking for UL element");
                }
            } else if ("Warnings" == element.parentNode.className) {
                log("GCRLCC:   --> not interested in warning results");
            } else if ("Pass" == element.parentNode.className) {
                log("GCRLCC:   --> not interested in pass results");
            } else {
                log("GCRLCC:   --> unexpected parent class", element.parentNode.className);
            }
        }
        return itemCount;
    }

    //-----------------------------------------------------------------------------
    function addGcConflictCopyToLitmusSectionOfReviewPage(itemCountOffset) {
        /* children of <li> item
        proximity conflict with listing coords:    0:[icon] 1:[link to cache under review] "too close to" 2:[icon] 3:[link to conflicting cache] " - " 4:[directional image] "distance and bearing"
        proximity conflict with waypoint of cache: 0:[icon] 1:[link to cache under review] "too close to" 2:[waypoint code link] 3:[waypoint type link] "waypoint" 4:[icon] 5:[link to conflicting cache] " - "6:[directional image] "distance and bearing"
        */
        var elements = document.querySelector('#ctl00_ContentBody_rltErrorListViewContainer ul');
        log('GCRLCC: - elements=', elements);

        var itemCount = itemCountOffset;
        if (elements) {
            for(var element of elements.children) {
                log('GCRLCC:   - element=', element);
                var index = null;
                if (element.children && element.children.length > 5) {
                    // waypoint case
                    index = 5;
                } else if (element.children && element.children.length > 3) {
                    // listing coord case
                    index = 3;
                }
                if (index) {
                    var link = element.children[index];
                    var text = link.innerText;
                    log('GCRLCC:     - link', link);
                    log('GCRLCC:     - text', text);
                    var gcCode = '';
                    var cacheName = text;

                    // try to split name into code and name
                    var parts = text.split(' ', 1);
                    log('GCRLCC:     - parts', parts);
                    if (parts.length > 0) {
                        if (parts[0].startsWith('GC')) {
                            gcCode = parts[0];
                            cacheName = cacheName.replace(gcCode+' ', '');
                            cacheName = cacheName.replace(gcCode, '');

                            log('GCRLCC:     - gcCode', gcCode);
                            log('GCRLCC:     - cacheName', cacheName);
                        }
                    }

                    itemCount = itemCount + 1;
                    addLinkButton(link, itemCount, cacheName, gcCode);

                } else {
                    log('GCRLCC:     - Could not determine case');
                }
            }
        }

        return itemCount;
    }

    //-----------------------------------------------------------------------------
    function addLinkButton(base, itemCount, cacheName, gcCode) {
        var imgLink = document.createElement('img');
        imgLink.id = "imgLitmusLink"+itemCount;
        imgLink.src = imgChainLink;
        imgLink.style.margin = "0px 0px 0px 10px";
        imgLink.title = "Click: Copy GC-Code and Cache Name to clipboard\r\nCtrl-Click: Copy GC-Code and Cache Name as Markdown Link";
        imgLink.style.cursor = "pointer";
        imgLink.addEventListener('click', copyData, false);
        if (gcCode && gcCode != '') {
            imgLink.value = gcCode + ": "+cacheName;
            imgLink.data = {code: gcCode, name: cacheName};
        } else {
            imgLink.value = cacheName;
            imgLink.data = null;
        }

        insertAfter(imgLink, base);
        log("GCRLCC:            - added imgLink", imgLink);
    }

    //-----------------------------------------------------------------------------
    function insertAfter(newElement, anchorElement) {
        anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
    }

    //-----------------------------------------------------------------------------
    function copyData(arg) {
        var result = arg.target.value;
        if (result != null) {
            if (arg.ctrlKey && arg.target.data && arg.target.data.code) {
                result = '['+arg.target.value+'](http://coord.info/'+arg.target.data.code+')';
            }
            copy(result);
            log("GCRLCC: Returning cache information: ", result);
        } else {
            log("GCRLCC: Nothing to return", result);
        }
    }

    //-----------------------------------------------------------------------------
    function copy(contentToCopy) {
        var txtToCopy = document.createElement('input');
        txtToCopy.style.left = '-300px';
        //txtToCopy.style.left = '30px';
        //txtToCopy.style.top = '30px';
        txtToCopy.style.position = 'absolute';
        txtToCopy.value = contentToCopy;
        document.body.appendChild(txtToCopy);
        txtToCopy.select();

        log("GCRLCC: Copying ", txtToCopy.value);
        var res = document.execCommand('copy');
        log("GCRLCC: Copy result ", res);

        txtToCopy.parentNode.removeChild(txtToCopy);
    }
})();
