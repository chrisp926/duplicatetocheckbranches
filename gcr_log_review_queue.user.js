﻿// ==UserScript==
// @name           GCR Log Review Queue v2
// @description    Enhances the Log Review Queue
// @namespace      http://www.geocaching.com/admin
// @version        02.02
// @icon           http://i.imgur.com/a94PYHJ.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_log_review_queue.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_log_review_queue.user.js
// @include        http*://*.geocaching.com/admin/lqueue.aspx*
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_addStyle
// @grant          GM_log
// ==/UserScript==

/*

Function:
 Adds additional functions to the Log Review Queue

*/

  var UpArrowImg =
    'data:image/gif,GIF89a%0C%00%0E%00%B3%00%00%00%00%00c%7B%94k%' +
    '8C%9Cs%8C%9C%84%9C%AD%8C%9C%AD%94%A5%B5%A5%B5%BD%B5%BD%CE%B5' +
    '%C6%CE%C6%CE%D6%CE%D6%DE%D6%DE%DE%E7%EF%EF%F7%F7%F7%FF%FF%FF' +
    '!%F9%04%01%00%00%00%00%2C%00%00%00%00%0C%00%0E%00%00%04E%10%' +
    'C8I%AB%BD%0B%A1e%D7%08%600p%D2%12%9E%01%E7%08%A1b%82%82%93%8' +
    '4%CB%F3(ab%80%8Am%BF%85B%E0%C0%F0%3D%18%87%00a%072%86%0A%B8%' +
    'A6%AF%E5%F8%04%9C%81%18%A0%C1r%0A%1A%93%C6%C0H%00%03%22%00%3B'

  var TableSizerUpImg =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
    "AAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJJREFUeNqkU1tLVFEY" +
    "Xec2MzpzZpqZzKZGGyeLLgZWWtJDFiVRYb4UBUaSIETvFRQIUb3UQ1QP3SAy6QZBhS%2FV" +
    "QzcrpyyjVLpIiTpkM6PO6NzOdU7fzAn6AW1YZ2%2FO3t%2F6vm%2FttZmB%2Fs%2FID4Hn" +
    "wBka2GL33ERR2UFGzW5lJ0fKDEUW4J4b18XS93Zt%2BoowE36aUTUYYKFpGvh8sEFgdRWy" +
    "N9im6PxF14uznHe0C3b9O5hcEgoX9MTFuoXxuoO7pfIVXZbIl72qLM0ADJjBfAWaDMa%2F" +
    "7DiTiLf7O5vgkD8AfmK1whwqIQHkokB48zWkNu7%2Fqg8PbFRl%2BTcrZTPgSoONFlVqD1" +
    "xqgIOh4PUUMIdQvQvY0wsEFgBLqcqVQPn9VrhfXV%2FCV1RdsAk82BzDg3N7jpU8OA6b%2" +
    "FgWopcA4oZxYajsBsQaofwBwXsCGwn7Jw0NwxYZ2srP929li34J6x8jgWmt%2FB7CEDiQJ" +
    "i7cBdU%2F%2B9WCvBjY9BxzzAQ%2FA8xMo7r4D3uPdwQrO2fuE0Q%2FgctNUIx0WfcDqjr" +
    "%2FB1LyhEzQiqQJqrpFe9LuYChp7Basq1fIkZNBIJszgIsLkONB%2FhkSsoFQi4GoGIudI" +
    "cEo9eMcUlM6xSgSQNQ%2BvKfp3XfRuSFMBrnzvFsKn08BPmuvvmi1MDQKhq4BCa7p4JQZI" +
    "JXRNAiZZaWL0BhOoRTzjgxKGWaLyVwuFMwkkO5CmmbqBDMSGaCvQAN1mf8smo%2BOvFX%2" +
    "Bgm1%2FXgnAIeW8AZBHE8mQWkyAfHEWhzRmyTYorg3VTEyaHhx%2ByDDkw%2FGP4pPPAYU" +
    "i%2BNRi6ZWaBRJiOmgTZeMGuiffA0EuyyJHziDtLb6ciY4%2BZUKgHcjYNsXzR0YUe16lI" +
    "%2Bx4kux%2FBOw9wllJS6jlL%2BkyNEW%2BRExUnbiO1akvfSN%2B7BqvATzE9PW8KVWfS" +
    "KYj%2BiuZgZeVl7XmXPfnsHpSxbuhyAoK7CvaaRohNLYjZZt0c%2F%2Fiu1WrhFY4XUCAo" +
    "PKicDlVVYbA2t295dZvLIWxjMhnyMCtAYKZk3t7769vXq%2BloOCS6nPlnRHrR1zAM%2FM" +
    "%2F4I8AAveYyMfSGOwwAAAAASUVORK5CYII%3D";

  var TableSizerDnImg =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
    "AAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFJREFUeNqkU1lrE1EY" +
    "PffOTNI2k9Qk1baY2jYVLbjiCoLUohUXVBBRUXFFUd98UETBBRX0TfRBtIhSkfoguIMKWp" +
    "dK69IiplVxqTWtttUkTduMyWwZv2mCf8A7HJjL3HPud873DWsLhWAvUeAQLB08z1cUzw3s" +
    "hp5czKPfS6BrEhtR2G%2B4i97IRvyCNNj9WNENWOAwDAOiTbYsgJsaNH9wu2Y6zuU%2FPS" +
    "34w3cha%2B%2FAtCg0Z6Wv31tVEZu9a02ydPIdZ%2B%2F7DZqaHCQWWHtbGyxdBRtdeZQN" +
    "9B8qqVsBOdUCBEjZIwASAxIG8AtIR4Gu%2BRehVG%2F9aH4LVasptZcnlSGIo0qXOQ31UN" +
    "m5GsiMyHOJPJpQfQpYrwPz9gDj6b7JQOmNbfA2Xq4UyyedzXVK4GkmQPD5DxbcPIJc8wMw" +
    "y%2FZEcBHy%2FMP5wOmxqwUKCTOAglt74Yl8XsULAkt5XnFZlet7%2B%2BycUB1QSQfULN" +
    "lrE%2FWMQC7L7E1CEbkSI3A1XoPo9S%2FnkmfkRke4FYIZzxDtm4yskJEV0CmvVLYy0oKb" +
    "uhZuRI6hzuSMI2gNEVnMfhwijDkJTLhEqZlZC1OAabVEnA8M2ntbtI8u0XyioZufTNk3Tx" +
    "kA8n%2FShwIyGtxMh2zDdgVpYOSGTGkqtSb0CFoPvRYHYEk8ylO%2Fw3WsfCbiSjH0GJ2L" +
    "kPK91cQjRUhZT4T%2BduDhjuEcIp0kXVaDtNP1kid%2B9bzQA%2BXPxTmb0P0y4w8tz4A6" +
    "KtfOxV7Rt0BtFTG7MNhBY8FL4FiwAtHOjlucpQ10d3w77t65D2rRLHy5mu3AZ5qH%2BnU0" +
    "QI3AlZUUYhQDYeBrAzBq%2FxnE3YX1id6uB6y5uRlqUoFnzLgDFT73ib7Da5F4fh%2F%2B" +
    "EhpEGRAogj%2FUkViEfEselB2rR2L6otbOllc1TkmIsaampuEqU4oCOVC%2BPjg2eN58ct" +
    "s11HAdWt8baoQCUR4L16SFkFduQcTpvdrz9vVWhyRogijin4CVTlPbNVhCjrd44tTtHpe4" +
    "hCnJUgtMoiemOVyvf374WKv8%2FtEs53uGZ4IxBmbZv%2BJ%2FrL8CDAAtGDlMBzStGQAA" +
    "AABJRU5ErkJggg%3D%3D";

  var ShowLogImg =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
    "AACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMA" +
    "APn%2FAACA6QAAdTAAAOpgAAA6mAAAF2%2BSX8VGAAACjUlEQVR42nSTT0hUURTGf2%2Fe" +
    "vDeOr9R5jmkQRJAobZSYICKkZmDKQjKoZYsIWrQwWtQ6aNWuIIL%2BrMKIyJ2kgv2BkAgh" +
    "JZJKybJM0yK1%2Bafz3txzW9Q8mLIDl8s9fN93zvkO1%2Bjt7eVPnAiHw3ds28bzvCwgrB" +
    "OmaY4Xi8X95XdYa41hGKcsy7qdTCYZGxsjkUhsrK%2BvB0BrHdwiQn9%2F%2F95yDiBsGM" +
    "Zp27ZvpFIpXNcFIBKJEI1GK8hKKXzfR2tNhUAkErmRSqWIxWIVrZZBIlJx%2FhHwPC8zMj" +
    "JSk8lkAGhubiYejwdkpVTQ%2FroCIlLo6OioOXd3AafK5MiOJvr6%2BgJSmSAipNPpIFdh" +
    "IsDB9hhVG6qIRqN0d3cHwHLl%2F3pQBhzbHQ%2BAf8%2B9WvS5fP8ls99zNDi15k53KRAI" +
    "ifxe99WHc9x6vPAP%2Bd6TSXquP2Oz63DtbJo97duNwc8xb%2BBT7JKI%2FB5BKcXEbJ46" +
    "1yGbzTI0NITWmjc%2FbGZ%2B2lw5s4%2FFnEfOV1S7dRxKtlmDT16dH%2FzsSjDCtZNbg6" +
    "pdXV2ICKN3Rkm01PLhxxqOU8W2ho1MfSvwbHqJxnhNcSVT6AiVjVrJe6zkfZRSKKVY83wW" +
    "lwt4onmzmOfRu28ArPqK5VwRwTCV0i1BBxcfzBOxDC4ebUApRQjNproo%2BWKJ1Z9F5lfW" +
    "APBLgqkFRJXMkPE68KCp1sSprSaXyzE8PIyIEC1VMzlTYHdTnF1tmwE4ntjC9Jclxme%2F" +
    "WiUlL8JKKUspRU%2Fa%2FbNCk87OTkSEAyL0j87y9MVbCtsbmfm4gCr5jE%2FM5zLfP7Ye" +
    "brXmwsDcwMCAs97X1VpjAMlGmJgX%2B70fMhxLecvPbyampt5mD7Vc4NcAdVvJgF%2BVng" +
    "EAAAAASUVORK5CYII%3D";

  var ShowLogTextImg =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
    "AACXBIWXMAAAsSAAALEgHS3X78AAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHja" +
    "nVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjo" +
    "CMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89%2BbN%2FrXXPues852zzwfACAyWSDNRNYAM" +
    "qUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz%2FSMBAPh%2BPDwrIsAHvgABeNMLCADATZvAMB" +
    "yH%2Fw%2FqQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAn" +
    "f%2BbTAICd%2BJl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2" +
    "ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5" +
    "RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA%2Fg88wAAKCRFRHgg%2FP9eM4Ors" +
    "7ONo62Dl8t6r8G%2FyJiYuP%2B5c%2BrcEAAAOF0ftH%2BLC%2BzGoA7BoBt%2FqIl7gRo" +
    "XgugdfeLZrIPQLUAoOnaV%2FNw%2BH48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl%2FAV%2F" +
    "1s%2BX48%2FPf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H%2FLcL%2F%2Fwd0yLESW" +
    "K5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s%2BwM%2B3zUAsGo%2BAXuRLahdYwP2Sy" +
    "cQWHTA4vcAAPK7b8HUKAgDgGiD4c93%2F%2B8%2F%2FUegJQCAZkmScQAAXkQkLlTKsz%2" +
    "FHCAAARKCBKrBBG%2FTBGCzABhzBBdzBC%2FxgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAd" +
    "KmAv1EAdNMBRaIaTcA4uwlW4Dj1wD%2FphCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4" +
    "IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDL" +
    "VDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8%2BQ8cwwOgYBzPEbDAuxsNCsT" +
    "gsCZNjy7EirAyrxhqwVqwDu4n1Y8%2BxdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0Ed" +
    "oJNwkDhFHCJyKTqEu0JroR%2BcQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpF" +
    "TSEtJG0m5SI%2BksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG%2BQh8lsKnWJAcaT4" +
    "U%2BIoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6Wt" +
    "opXTGmgXaPdpr%2Bh0uhHdlR5Ol9BX0svpR%2BiX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK" +
    "%2BYTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI" +
    "%2BpXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q%2FpH5Z%2FYkGWcNMw09DpF" +
    "GgsV%2FjvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY%2FR27iz2qqaE5QzNKM1ezUvOU" +
    "Zj8H45hx%2BJx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aed" +
    "pr1Fu1n7gQ5Bx0onXCdHZ4%2FOBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up%2B6Ynr" +
    "5egJ5Mb6feeb3n%2Bhx9L%2F1U%2FW36p%2FVHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFD" +
    "XcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TD" +
    "tMx83MzaLN1pk1mz0x1zLnm%2Beb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVV" +
    "pds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw%2B" +
    "6TvZN9un2N%2FT0HDYfZDqsdWh1%2Bc7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPL" +
    "KcRpnVOb00dnF2e5c4PziIuJS4LLLpc%2BLpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26%2" +
    "FuNu5p7ofcn8w0nymeWTNz0MPIQ%2BBR5dE%2FC5%2BVMGvfrH5PQ0%2BBZ7XnIy9jL5FX" +
    "rdewt6V3qvdh7xc%2B9j5yn%2BM%2B4zw33jLeWV%2FMN8C3yLfLT8Nvnl%2BF30N%2FI%" +
    "2F9k%2F3r%2F0QCngCUBZwOJgUGBWwL7%2BHp8Ib%2BOPzrbZfay2e1BjKC5QRVBj4Ktgu" +
    "XBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz" +
    "30T6RJZE3ptnMU85ry1KNSo%2Bqi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt%2" +
    "F87fOH4p3iC%2BN7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCn" +
    "nCHcJnIi%2FRNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG" +
    "0yPTq9MYOSkZBxQqohTZO2Z%2Bpn5mZ2y6xlhbL%2BxW6Lty8elQfJa7OQrAVZLQq2Qqbo" +
    "VFoo1yoHsmdlV2a%2FzYnKOZarnivN7cyzytuQN5zvn%2F%2FtEsIS4ZK2pYZLVy0dWOa9" +
    "rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfe" +
    "vc1%2B1dT1gvWd%2B1YfqGnRs%2BFYmKrhTbF5cVf9go3HjlG4dvyr%2BZ3JS0qavEuWTP" +
    "ZtJm6ebeLZ5bDpaql%2BaXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO%2FPLi8ZafJzs" +
    "07P1SkVPRU%2BlQ27tLdtWHX%2BG7R7ht7vPY07NXbW7z3%2FT7JvttVAVVN1WbVZftJ%2" +
    "B7P3P66Jqun4lvttXa1ObXHtxwPSA%2F0HIw6217nU1R3SPVRSj9Yr60cOxx%2B%2B%2Fp" +
    "3vdy0NNg1VjZzG4iNwRHnk6fcJ3%2FceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYl" +
    "u6T8w%2B0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32" +
    "oPb%2B%2B6EHTh0kX%2Fi%2Bc7vDvOXPK4dPKy2%2BUTV7hXmq86X23qdOo8%2FpPTT8e7" +
    "nLuarrlca7nuer21e2b36RueN87d9L158Rb%2F1tWeOT3dvfN6b%2FfF9%2FXfFt1%2Bci" +
    "f9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v%2B3Njv3H9qwHeg89HcR%2FcGhYPP%2FpH1jw" +
    "9DBY%2BZj8uGDYbrnjg%2BOTniP3L96fynQ89kzyaeF%2F6i%2FsuuFxYvfvjV69fO0ZjR" +
    "oZfyl5O%2FbXyl%2FerA6xmv28bCxh6%2ByXgzMV70VvvtwXfcdx3vo98PT%2BR8IH8o%2" +
    "F2j5sfVT0Kf7kxmTk%2F8EA5jz%2FGMzLdsAAAAEZ0FNQQAAsY58%2B1GTAAAAIGNIUk0A" +
    "AHolAACAgwAA%2Bf8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAJgSURBVHjapFNLSxth" +
    "FD3fzCSZakls2qodLOkLIa66cBEKQhZCQShmVXd2USl2IUgX%2FoL6G%2BqiG9uV3bgolI" +
    "aCC6O2VGkkxIqlxldjJkWT2Ex8zOM73SQiBaHggcN9cu9Z3CtI4iJQcEFoDUcIcQ1AWz08" +
    "T5ao2yLJvdMBuq4%2Ficfjw5FIJCalPHebEAJCCGxtbX3RdX3i%2BPh4EoZh9CYSCabTaf" +
    "4vlpeXmUgkaBhGL6LR6PTs7OxpsXZ4RJKUJK3aIUlyv1Tm3n6JZvE387sFup7k3Nw8u7q6" +
    "phVFUa60t7cjmUxidHQUAb8Pw8PPMTMzA0mJxwMD0HUdruvC8zw4joPt7W0EgyGEQqHrCo" +
    "ATksjn80inv0FVVSwtLcI0TTiOg88LC%2FCkhG3bsG0bjuvWaaO5%2BbKrAfDVajUMDg7i" +
    "UX8%2F%2FlgWPnxMQgBwbAepuXmUSyV4ngfX9SClB09KKKoGCvoVkj%2FW1tagaRpaQi1Q" +
    "hAK%2F3w%2BhKCCJQCAAoag4PDqClB40TUM4HMZGLgdBrKt9fX2FVCr1EEBLU9MlHFQOUK" +
    "1WYVWrsCwLpVIJm7kcbt%2B6Bdu2sVvYxeLiV7x9M7mpqr4RQfKmEOJqa2vrs56enlgwGJ" +
    "Q%2Bv18KApIUlUo53N3dfWdsbAyvJiYw9W4ql%2Fu5%2Fn5nZ%2Bc1ySxcT3aSRP0nOgAY" +
    "AG7U2RGJRD5lMhmapsmnQ0OrAB40%2BklCy21skGSg897dE5K%2F%2Fr2%2BeDyuFgoFvB" +
    "wfL5YrlRGSC%2FULdgEAmexKWya7op2dekYRotHoi1gsVgRwv5HPfl8VDV9c9J3%2FDgB0" +
    "PZIXTbW4FwAAAABJRU5ErkJggg%3D%3D";

  var SettingsImg =
    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
    "AACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHja" +
    "nVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjo" +
    "CMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89%2BbN%2FrXXPues852zzwfACAyWSDNRNYAM" +
    "qUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz%2FSMBAPh%2BPDwrIsAHvgABeNMLCADATZvAMB" +
    "yH%2Fw%2FqQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAn" +
    "f%2BbTAICd%2BJl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2" +
    "ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5" +
    "RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA%2Fg88wAAKCRFRHgg%2FP9eM4Ors" +
    "7ONo62Dl8t6r8G%2FyJiYuP%2B5c%2BrcEAAAOF0ftH%2BLC%2BzGoA7BoBt%2FqIl7gRo" +
    "XgugdfeLZrIPQLUAoOnaV%2FNw%2BH48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl%2FAV%2F" +
    "1s%2BX48%2FPf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H%2FLcL%2F%2Fwd0yLESW" +
    "K5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s%2BwM%2B3zUAsGo%2BAXuRLahdYwP2Sy" +
    "cQWHTA4vcAAPK7b8HUKAgDgGiD4c93%2F%2B8%2F%2FUegJQCAZkmScQAAXkQkLlTKsz%2" +
    "FHCAAARKCBKrBBG%2FTBGCzABhzBBdzBC%2FxgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAd" +
    "KmAv1EAdNMBRaIaTcA4uwlW4Dj1wD%2FphCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4" +
    "IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDL" +
    "VDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8%2BQ8cwwOgYBzPEbDAuxsNCsT" +
    "gsCZNjy7EirAyrxhqwVqwDu4n1Y8%2BxdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0Ed" +
    "oJNwkDhFHCJyKTqEu0JroR%2BcQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpF" +
    "TSEtJG0m5SI%2BksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG%2BQh8lsKnWJAcaT4" +
    "U%2BIoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6Wt" +
    "opXTGmgXaPdpr%2Bh0uhHdlR5Ol9BX0svpR%2BiX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK" +
    "%2BYTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI" +
    "%2BpXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q%2FpH5Z%2FYkGWcNMw09DpF" +
    "GgsV%2FjvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY%2FR27iz2qqaE5QzNKM1ezUvOU" +
    "Zj8H45hx%2BJx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aed" +
    "pr1Fu1n7gQ5Bx0onXCdHZ4%2FOBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up%2B6Ynr" +
    "5egJ5Mb6feeb3n%2Bhx9L%2F1U%2FW36p%2FVHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFD" +
    "XcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TD" +
    "tMx83MzaLN1pk1mz0x1zLnm%2Beb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVV" +
    "pds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw%2B" +
    "6TvZN9un2N%2FT0HDYfZDqsdWh1%2Bc7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPL" +
    "KcRpnVOb00dnF2e5c4PziIuJS4LLLpc%2BLpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26%2" +
    "FuNu5p7ofcn8w0nymeWTNz0MPIQ%2BBR5dE%2FC5%2BVMGvfrH5PQ0%2BBZ7XnIy9jL5FX" +
    "rdewt6V3qvdh7xc%2B9j5yn%2BM%2B4zw33jLeWV%2FMN8C3yLfLT8Nvnl%2BF30N%2FI%" +
    "2F9k%2F3r%2F0QCngCUBZwOJgUGBWwL7%2BHp8Ib%2BOPzrbZfay2e1BjKC5QRVBj4Ktgu" +
    "XBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz" +
    "30T6RJZE3ptnMU85ry1KNSo%2Bqi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt%2" +
    "F87fOH4p3iC%2BN7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCn" +
    "nCHcJnIi%2FRNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG" +
    "0yPTq9MYOSkZBxQqohTZO2Z%2Bpn5mZ2y6xlhbL%2BxW6Lty8elQfJa7OQrAVZLQq2Qqbo" +
    "VFoo1yoHsmdlV2a%2FzYnKOZarnivN7cyzytuQN5zvn%2F%2FtEsIS4ZK2pYZLVy0dWOa9" +
    "rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfe" +
    "vc1%2B1dT1gvWd%2B1YfqGnRs%2BFYmKrhTbF5cVf9go3HjlG4dvyr%2BZ3JS0qavEuWTP" +
    "ZtJm6ebeLZ5bDpaql%2BaXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO%2FPLi8ZafJzs" +
    "07P1SkVPRU%2BlQ27tLdtWHX%2BG7R7ht7vPY07NXbW7z3%2FT7JvttVAVVN1WbVZftJ%2" +
    "B7P3P66Jqun4lvttXa1ObXHtxwPSA%2F0HIw6217nU1R3SPVRSj9Yr60cOxx%2B%2B%2Fp" +
    "3vdy0NNg1VjZzG4iNwRHnk6fcJ3%2FceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYl" +
    "u6T8w%2B0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32" +
    "oPb%2B%2B6EHTh0kX%2Fi%2Bc7vDvOXPK4dPKy2%2BUTV7hXmq86X23qdOo8%2FpPTT8e7" +
    "nLuarrlca7nuer21e2b36RueN87d9L158Rb%2F1tWeOT3dvfN6b%2FfF9%2FXfFt1%2Bci" +
    "f9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v%2B3Njv3H9qwHeg89HcR%2FcGhYPP%2FpH1jw" +
    "9DBY%2BZj8uGDYbrnjg%2BOTniP3L96fynQ89kzyaeF%2F6i%2FsuuFxYvfvjV69fO0ZjR" +
    "oZfyl5O%2FbXyl%2FerA6xmv28bCxh6%2ByXgzMV70VvvtwXfcdx3vo98PT%2BR8IH8o%2" +
    "F2j5sfVT0Kf7kxmTk%2F8EA5jz%2FGMzLdsAAAAEZ0FNQQAAsY58%2B1GTAAAAIGNIUk0A" +
    "AHolAACAgwAA%2Bf8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAOLSURBVHjaXNLfT9V1" +
    "HMfx5%2Bd7vpxzBAzkN8KBg7Kh4rTmrI4VBgd0Q0BWsYwuakuiUVNTGTWiG7YaBkUtL3Lz" +
    "otY0OGHhjwIvAJfB6YywulAUUQEPHNA4gZzD4df33QXrpuc%2F8Npje6lzbW2dusnkLCkt" +
    "tQLLAEpTfP5pM4KBGMKRo0fNNdXVrsJ9RSWhhRCBYOCje8N3at89dgyVbrN9fH3wxnttra" +
    "2%2Bp57cmRycn8cwDK7%2B6kYAMQzMVktvVVWVw%2Bv18k7V2%2BefeHxb2VwwmPhJY9MD" +
    "LTYq%2Bv1NmZk109P%2BJETOokBD8axjB3lOJ2davsstLy93hEIhXC4XubnO7Ozs7QsbNm" +
    "a2lZWVvaFpmpAUG3PiVOOJFFdr6wENvgRBBA7VNREfF5cOMDs7i8Oxi%2FVpaRvHJyepqK" +
    "jYOTMzs6ibxEAwUMoYbzl9yipihIqLS%2Fyaok5pisGbt4amp6fp6%2BvD6XSSmppCRMRu" +
    "BgYGQh6P5zdt6p85fP5ZRDR0PWwBUOfbf%2FggKxA63Cl%2BKrflX7WnpwPQ1dWFxWLB6%" +
    "2FVSX1%2F%2Fld%2Fv%2F0sXDJQICgCD1ZQK9F%2BTRTGaszztGIuf0ebquTDmvX%2Bvo6" +
    "Mj2e12Xx4aGvoGQA%2B9eAl7kpn5pQV0XfGTBiMBnZdOFpLa3Miix8NpW5q721YbdCR7D%" +
    "2FG%2FVOLha2QkWgguLWC1apiAP6%2BbpKz1aY4nr8P%2BxUm6D77FMrTH%2BcZLw1PWYo" +
    "1fw9Y%2FphARdAWIgFo1MD4qsj49hq8d3UQVhIf2Hcy35jQ2MPf9uf0Xe3slKcxkymTFGM" +
    "61AaCtrMjqYQQmRkTCYuMZdvvYsjeDs9%2B6Bh4Kx3uqa4hvaqQ4N4%2FbgeWV%2B1r4jv" +
    "8ImlKr65MjIuGJSQz3TrLpeRt3LlxusWeYn7FNTjTNiPHhz8%2FlENfQwKslpQyOBfq9WG" +
    "wAmiEwMWrMrduQws3uMTbn27jb0d0TZVcHVlUKu2%2Byfs4w%2BH1PATF1tbxcvJ%2BRG3" +
    "OjAPqjEa4k7EqMGOwaZcseO7c7%2B%2Fqj7SrXf%2BYVHmiw26wQzcoVn0%2BNKCXhhYVs" +
    "vniJvGCAW3k5P6InFLyeUHFXtjc9FPPWur%2FXAhEmMOugmQGzAqsVrJEQFsWRqKiZsaIi" +
    "maislNciI%2BNVRNZeloa638QcbVOhqTbgMd3EmiXF0rLGsoECzWIC3cyK6VHY8uxEjFIv" +
    "TIv8smgYnn8HAOD1i6l3b2VwAAAAAElFTkSuQmCC";

  // Text checking function.
  String.prototype.startsWith = function(str) {
    return (this.indexOf(str) == 0);
  }

  String.prototype.contains = function(str) {
    return (this.indexOf(str) >= 0);
  }

  String.prototype.endsWith = function(str) {
    return this.match(str + "$") == str;
  }

  // Create class styles.
  GM_addStyle('.gms-dist-alert {background-color: yellow;}');
  GM_addStyle('.tag_opened_UC { background-color:rgb(255, 255, 102) !important }');
  GM_addStyle('.tag_opened_WP { background-color:rgb(0, 178, 101) !important }');
  GM_addStyle('.tag_opened_NA { background-color:rgb(255, 160, 122) !important }');
  GM_addStyle('.mult_move { background-color:Coral !important}');
  GM_addStyle(".quickbutton {border:solid 1px rgb(68,142,53); " +
      "border-radius:5px; -moz-border-radius:5px; background-color:rgb(239,239,239); " +
      "padding:1px; }");

  // Get currently signed-on geocaching.com profile.
  var SignedInAs = document.getElementById('ctl00_LoginUrl');
  if (!SignedInAs) { SignedInAs = document.getElementById('ctl00_LogoutUrl'); }
  SignedInAs = SignedInAs.parentNode.childNodes[1].firstChild.data.trim();
  SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');

  var allIcons = document.getElementsByClassName('logtype-icon');
  for (var i = allIcons.length - 1; i >= 0; i--) {
    if (allIcons[i].nextSibling.nodeName.toUpperCase() == 'SPAN') {
      removeNode(allIcons[i].nextSibling);
    }
  }

  // Identify column positions, to facilitate updating to future site changes.
  var cell_logType = 0;
  var cell_icon    = 1;
  var cell_pm      = 2;
  var cell_cacheId = 3;
  var cell_logDate = 4;
  var cell_coords  = 5;
  var cell_cache   = 6;
  var cell_region  = 7;
  var cell_logBy   = 8;
  var cell_worked  = 9;

  // Set global timer handles.
  var NaTimer;
  var UcTimer;
  var WpTimer;

  // Set value for localStorage key.
  var logStorId = '05d3e23f-ba26-4a5f-824b-e0d8738b180e';

  // Set value for sessionStorage keys.
  var viewLogTextId = 'b595a61c-0131-4eb1-8127-991433429e36';
  var wrkStorId = 'dc423c0e-7a08-4950-abef-6ca7a5590537';

  // Assign character codes to variables.
  var logDelim = String.fromCharCode(167);  // Section character

  // Use Compact heading if requested.
  var useCompactHeadings = GM_getValue('UseCompactHeading_' + SignedInAs, true);
  if (useCompactHeadings) {
    var parX = document.getElementsByClassName('NoSpacing')[0];
    if (parX) {
      parX.parentNode.removeChild(parX);
    }
    var h2Line = document.getElementById('ctl00_ContentBody_lbHeading').parentNode;
    if (h2Line) {
      if (h2Line.nextSibling.nodeName == '#text') {
        h2Line.parentNode.removeChild(h2Line.nextSibling);
      }
      h2Line.parentNode.removeChild(h2Line);
    }
    parX = document.getElementsByClassName('ShowLogCheckbox');
    parX[0].style.marginTop = '-14px';
    parX[1].style.marginTop = '3px';
        parX[2].style.marginTop = '3px';
    parX = document.getElementsByClassName('ReverseSpacing');
    parX[0].style.marginTop = '3px';
    parX[0].style.marginBottom = '0px';
  }

  // Reduce page title text to make room for buttons.
  var e_ctl00_HDHomeLink = document.getElementById('ctl00_HDHomeLink');
  if (e_ctl00_HDHomeLink) {
    e_ctl00_HDHomeLink.nextSibling.data = ' - Log Review Queue';
  }

  // Fix pager font size and alignment
  var pbw = document.getElementsByClassName('PageBuilderWidget');
  if (pbw.length) {
    pbw[0].width = '55%';
    pbw[1].align = 'left';
    pbw[2].width = '55%';
    pbw[3].align = 'left';
  }

  // Add table resizer controls.
  var spanTblSizer = document.createElement('span');
  spanTblSizer.id = 'spanTblSizer';
  var aTblSizerUp = document.createElement('a');
  aTblSizerUp.href = 'javascript:void(0)';
  aTblSizerUp.setAttribute('adjust', -0.02);
  aTblSizerUp.style.marginLeft = '7px';
  aTblSizerUp.title = 'Adjust table size';
  var imgTblSizerUp = document.createElement('img');
  imgTblSizerUp.src = TableSizerUpImg;
  imgTblSizerUp.style.verticalAlign = 'text-bottom';
  imgTblSizerUp.classList.add('quickbutton');
  aTblSizerUp.appendChild(imgTblSizerUp);
  var aTblSizerDn = aTblSizerUp.cloneNode(true);
  aTblSizerDn.setAttribute('adjust', 0.02);
  aTblSizerDn.firstChild.src = TableSizerDnImg;
  spanTblSizer.appendChild(aTblSizerUp);
  spanTblSizer.appendChild(aTblSizerDn);

  aTblSizerUp.addEventListener("click", fChangeTableSpacing, true);
  aTblSizerDn.addEventListener("click", fChangeTableSpacing, true);

  var headerDiv = document.evaluate("//div[contains(@class, 'yui-u first')]",
        document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
  if (headerDiv) {
    headerDiv.childNodes[1].appendChild(spanTblSizer);
  }

  // Add image link to show/hide activity log.
  var spanActLog = document.createElement('span');
  spanActLog.style.marginLeft = '7px';
  spanActLog.id = 'spanActLog';
  var lnkActLog = document.createElement('a');
  var imgActLog = document.createElement('img');
  imgActLog.src = ShowLogImg;
  imgActLog.style.verticalAlign = 'text-bottom';
  imgActLog.classList.add('quickbutton');
  lnkActLog.appendChild(imgActLog);
  lnkActLog.title = 'Show Last 250 Actions.';
  lnkActLog.href = 'javascript:void(0)';
  spanActLog.appendChild(lnkActLog);
  insertAfter(spanActLog, spanTblSizer);
  lnkActLog.addEventListener("click", fShowLogData, true);

  // Add image link to show/hide log text.
  var spanLogText = document.createElement('span');
  spanLogText.style.marginLeft = '7px';
  spanLogText.id = 'spanLogText';
  var lnkLogText = document.createElement('a');
  var imgLogText = document.createElement('img');
  imgLogText.src = ShowLogTextImg;
  imgLogText.style.verticalAlign = 'text-bottom';
  imgLogText.classList.add('quickbutton');
  lnkLogText.appendChild(imgLogText);
  lnkLogText.title = 'Show/Hide Log Text.';
  lnkLogText.href = 'javascript:void(0)';
  lnkLogText.setAttribute('showlogtext', GM_getValue('DspLogs_' + SignedInAs, true) == true);
  spanLogText.appendChild(lnkLogText);

  // Add image link to display settings interface.
  var spanSettings = document.createElement('span');
  spanSettings.style.marginLeft = '7px';
  spanSettings.id = 'spanSettings';
  var lnkSettings = document.createElement('a');
  var imgSettings = document.createElement('img');
  imgSettings.src = SettingsImg;
  imgSettings.style.verticalAlign = 'text-bottom';
  imgSettings.classList.add('quickbutton');
  lnkSettings.appendChild(imgSettings);
  lnkSettings.title = 'Edit Log Queue Script Settings';
  lnkSettings.href = 'javascript:void(0)';
  spanSettings.appendChild(lnkSettings);

  insertAfter(spanLogText, spanActLog);
  lnkLogText.addEventListener("click", fToggleLogText, true);
  insertAfter(spanSettings, spanLogText);
  lnkSettings.addEventListener("click", fSetOptions, true);

  // Show log text, if selected.
  fShowLogText();

  // Add check-all to table heading.
  var tabMain = fFindQueueTable();
  if (!tabMain) {
    GM_log('Unable to locate queue table. Script aborted.');
    return;
  }

  // Restore user-selected table spacing.
  tabMain.classList.add('LogTable');
  fSetTableSpacing();

  // Set a fixed width for the coordinates column.
  tabMain.rows[0].cells[cell_coords].style.width = '16.5em';

  // Add checkbox control to work/unwork all visible entries.
  var wrkdCell = tabMain.rows[0].cells[cell_worked];
  wrkdCell.style.textAlign = 'center';
  wrkdCell.appendChild(document.createElement('br'));
  var chkAll = document.createElement('input');
  chkAll.type = 'checkbox';
  chkAll.title = 'Check/Uncheck All Displayed';
  wrkdCell.appendChild(chkAll);
  chkAll.addEventListener("click", fCheckAllNone, true);

  // Add Needs Archive auto-work options
  var p_na = document.getElementsByClassName('ShowLogCheckbox')[0];
  if (p_na) {
    var spanNaAutoWork = document.createElement('span');
    spanNaAutoWork.id = 'spanNaAutoWork';
    spanNaAutoWork.style.marginLeft = '25px';
    spanNaAutoWork.appendChild(document.createTextNode('Auto-Work:'));

    var lnkAwDisabled = document.createElement('a');
    lnkAwDisabled.id = 'lnkAwDisabled';
    lnkAwDisabled.setAttribute('task', 'Disable');
    lnkAwDisabled.appendChild(document.createTextNode('Disabled'));
    lnkAwDisabled.style.marginLeft = '8px';
    lnkAwDisabled.title = 'Set all visible disabled NA entries to Worked status.';
    lnkAwDisabled.href = 'javascript:void(0)';
    spanNaAutoWork.appendChild(lnkAwDisabled);

    var lnkAwArchived = document.createElement('a');
    lnkAwArchived.id = 'lnkAwArchived';
    lnkAwArchived.setAttribute('task', 'Archive');
    lnkAwArchived.appendChild(document.createTextNode('Archived'));
    lnkAwArchived.style.marginLeft = '10px';
    lnkAwArchived.title = 'Set all visible archived NA entries to Worked status.';
    lnkAwArchived.href = 'javascript:void(0)';
    spanNaAutoWork.appendChild(lnkAwArchived);

    var lnkAwDuplicate = document.createElement('a');
    lnkAwDuplicate.id = 'lnkAwDuplicate';
    lnkAwDuplicate.setAttribute('task', 'Duplicate');
    lnkAwDuplicate.appendChild(document.createTextNode('Duplicates'));
    lnkAwDuplicate.style.marginLeft = '10px';
    lnkAwDuplicate.title = 'Set all visible Duplicate NA entries to Worked status. ' +
        'First entries will be retained.';
    lnkAwDuplicate.href = 'javascript:void(0)';
    spanNaAutoWork.appendChild(lnkAwDuplicate);

    var lnkAwNaOpened = document.createElement('a');
    lnkAwNaOpened.id = 'lnkAwNaOpened';
    lnkAwNaOpened.setAttribute('task', 'NaOpened');
    lnkAwNaOpened.appendChild(document.createTextNode('Opened'));
    lnkAwNaOpened.style.marginLeft = '10px';
    lnkAwNaOpened.title = 'Set all visible NA entries tagged as Opened to Worked status.';
    lnkAwNaOpened.href = 'javascript:void(0)';
    spanNaAutoWork.appendChild(lnkAwNaOpened);

    lnkAwDisabled.addEventListener("click", fSetToWorked, true);
    lnkAwArchived.addEventListener("click", fSetToWorked, true);
    lnkAwDuplicate.addEventListener("click", fSetToWorked, true);
    lnkAwNaOpened.addEventListener("click", fSetToWorked, true);

    var spanNaMessage = document.createElement('span');
    spanNaMessage.id = 'spanNaMessage';
    spanNaMessage.appendChild(document.createTextNode(''));
    spanNaMessage.style.marginLeft = '15px';
    spanNaMessage.style.color = 'DarkGreen';
    spanNaMessage.style.fontWeight = 'bold';
    spanNaAutoWork.appendChild(spanNaMessage);

    p_na.appendChild(spanNaAutoWork);
  }

  // Add Update Coordinates auto-work options
  var p_uc = document.getElementsByClassName('ShowLogCheckbox')[1];
  if (p_uc) {
    var spanUcAutoWork = document.createElement('span');
    spanUcAutoWork.id = 'spanUcAutoWork';
    spanUcAutoWork.style.marginLeft = '25px';
    spanUcAutoWork.appendChild(document.createTextNode('Auto-Work:'));

    var lnkAwUcOpened = document.createElement('a');
    lnkAwUcOpened.id = 'lnkAwUcOpened';
    lnkAwUcOpened.setAttribute('task', 'UcOpened');
    lnkAwUcOpened.appendChild(document.createTextNode('Opened'));
    lnkAwUcOpened.style.marginLeft = '10px';
    lnkAwUcOpened.title = 'Set all visible UC entries tagged as Opened to Worked status.';
    lnkAwUcOpened.href = 'javascript:void(0)';
    spanUcAutoWork.appendChild(lnkAwUcOpened);

    lnkAwUcOpened.addEventListener("click", fSetToWorked, true);

    var spanUcMessage = document.createElement('span');
    spanUcMessage.id = 'spanUcMessage';
    spanUcMessage.appendChild(document.createTextNode(''));
    spanUcMessage.style.marginLeft = '15px';
    spanUcMessage.style.color = 'DarkGreen';
    spanUcMessage.style.fontWeight = 'bold';
    spanUcAutoWork.appendChild(spanUcMessage);

    p_uc.appendChild(spanUcAutoWork);
  }

  // Add Update Waypoints auto-work options
  var p_wp = document.getElementsByClassName('ShowLogCheckbox')[2];
  if (p_wp) {
    var spanWpAutoWork = document.createElement('span');
    spanWpAutoWork.id = 'spanWpAutoWork';
    spanWpAutoWork.style.marginLeft = '25px';
    spanWpAutoWork.appendChild(document.createTextNode('Auto-Work:'));

    var lnkAwWpOpened = document.createElement('a');
    lnkAwWpOpened.id = 'lnkAwWpOpened';
    lnkAwWpOpened.setAttribute('task', 'WpOpened');
    lnkAwWpOpened.appendChild(document.createTextNode('Opened'));
    lnkAwWpOpened.style.marginLeft = '10px';
    lnkAwWpOpened.title = 'Set all visible WP entries tagged as Opened to Worked status.';
    lnkAwWpOpened.href = 'javascript:void(0)';
    spanWpAutoWork.appendChild(lnkAwWpOpened);

    lnkAwWpOpened.addEventListener("click", fSetToWorked, true);

    var spanWpMessage = document.createElement('span');
    spanWpMessage.id = 'spanWpMessage';
    spanWpMessage.appendChild(document.createTextNode(''));
    spanWpMessage.style.marginLeft = '15px';
    spanWpMessage.style.color = 'DarkGreen';
    spanWpMessage.style.fontWeight = 'bold';
    spanWpAutoWork.appendChild(spanWpMessage);

    p_wp.appendChild(spanWpAutoWork);
  }

  // Get auto-run litmus settings.
  var AutoLitmusNa = GM_getValue('AutoLitmusNa_' + SignedInAs, true);
  var AutoLitmusUc = GM_getValue('AutoLitmusUc_' + SignedInAs, true);

  // Get list of caches already worked in this session.
  var workList = window.sessionStorage.getItem(wrkStorId);
  if (!workList) { workList = ''; }
  var arWorkList = workList.split(',');

  // Add Up-Arrow image link to cache IDs.
  var xPathSearch = "//a[contains(@href, '/admin/review.aspx?guid=')]";
  var xList = document.evaluate(
    xPathSearch, tabMain, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
  var ic = xList.snapshotLength;
  for (var i = 0; i < ic; i++) {
    var aWptId = xList.snapshotItem(i);
    var wptId = aWptId.firstChild.data;
    var guid = UrlParm('guid', true, aWptId.href);
    var rowWptId = fUpGenToType(aWptId, 'TR');
    aWptId.id = 'wptid_' + rowWptId.rowIndex.toString();
    aWptId.parentNode.setAttribute('style', 'white-space: nowrap');
    var aUpArrowR = document.createElement('a');
    aUpArrowR.id = 'aUpArrowR_' + rowWptId.rowIndex.toString();
    aUpArrowR.title = 'Open top ' + (i + 1).toString() + ' caches';
    aUpArrowR.setAttribute('row', rowWptId.rowIndex);
    aUpArrowR.setAttribute('wptid', wptId);
    aUpArrowR.setAttribute('guid', guid);
    aUpArrowR.href = 'javascript:void(0)';

    // Get log type.
    var ltImg = tabMain.rows[rowWptId.rowIndex].cells[cell_logType].firstChild;
    if (ltImg.src.endsWith('/images/logtypes/7.png')) {
      var logType = 'NA';
      if (AutoLitmusNa) { aWptId.href += '&AutoLitmus=On'; }
    }
    else if (ltImg.src.endsWith('/images/logtypes/47.png')) {
      logType = 'UC';
      if (AutoLitmusUc) { aWptId.href += '&AutoLitmus=On'; }
    } else if (ltImg.src.endsWith('/images/plus-minus.png') || ltImg.src.endsWith('/images/other-changes.jpg') || ltImg.src.endsWith('/images/type-changed.png')) {
      logType = 'WP';
    } else { logType = ''; };

    aWptId.name = logType + '_revlink';

    imgUpArrow = document.createElement('img');
    imgUpArrow.src = UpArrowImg;
    aUpArrowR.appendChild(imgUpArrow);
    aUpArrowR.setAttribute('log_type', logType);
    aUpArrowR.style.marginRight = '6px';
    insertAheadOf(aUpArrowR, aWptId);

    var aUpArrowC = aUpArrowR.cloneNode(true);

    aUpArrowC.id = 'aUpArrowC_' + rowWptId.rowIndex.toString();
    aUpArrowC.setAttribute('pagetype', 'cache');
    aUpArrowC.name = logType + '_C_' + wptId;
    aUpArrowR.name = logType + '_R_' + wptId;
    aUpArrowR.title += ' in Review mode';
    aUpArrowR.setAttribute('pagetype', 'review');
    aUpArrowR.addEventListener("click", fUpArrowClicked, true);

    var pageLinks = rowWptId.getElementsByClassName('lnk');
    var lnkReview = pageLinks[0];
    var lnkCache = pageLinks[1];
    insertAheadOf(aUpArrowC, lnkCache);
    aUpArrowC.addEventListener("click", fUpArrowClicked, true);
    lnkReview.addEventListener("click", fPageLinkClicked, true);
    lnkCache.addEventListener("click", fPageLinkClicked, true);

    // Check if this has already been worked.
    var wrkChkVal = logType + wptId;
    if (arWorkList.indexOf(wrkChkVal) > -1) {
      tabMain.rows[rowWptId.rowIndex].cells[cell_worked].classList.add('tag_opened_' + logType);
    }
  }

  // Generate list of coordinate changes
  if (GM_getValue('DspDist_' + SignedInAs, true) == true) {
    var DistWarn = GM_getValue('DistWarn_' + SignedInAs, 100) - 0;
    var DistType = GM_getValue('DistType_' + SignedInAs, 'ft');
    xPathSearch = ".//img[contains(@src, '/other-changes.jpg')] | .//img[contains(@src, '/images/logtypes/47.png')]";
    xList = document.evaluate(
      xPathSearch, tabMain, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
    ic = xList.snapshotLength;

    // Extract feet and meter data, and add after coordinates.
    for (var i = 0; i < ic; i++) {
      var imgCoor = xList.snapshotItem(i);
      var logCoor = imgCoor.title;
      if (logCoor) {
        var distErr = false;
        var RegEx1 = new RegExp('.*Distance from original: (.*?) feet or (.*?) meters.*');
        var RegRslt = RegEx1.exec(logCoor);
        if (RegRslt) {
          var feet = RegRslt[1] - 0;
          var meters = RegRslt[2] - 0;
        } else {
          feet = 0;
          meters = 0;
          distErr = true;
        }

        var dist = feet.toString() + ' feet / ' + meters.toString() + ' meters';
        if (distErr) { dist = '* * Invalid Distance Data * *'; }
        var logRow = imgCoor.parentNode.parentNode;
        logRow.style.verticalAlign = 'top';
        logRow.cells[cell_coords].appendChild(document.createElement('br'));
        var spanDist = document.createElement('span');
        spanDist.appendChild(document.createTextNode(dist));
        if (DistWarn) {
          if ((DistType == 'ft' && feet >= DistWarn) ||
              (DistType == 'm' && meters >= DistWarn)) {
            spanDist.setAttribute('class', 'gms-dist-alert');
          }
        }

        logRow.cells[cell_coords].appendChild(spanDist);
      }

      // Check for multiple Update Coordinates logs for the same cache.
      var wptId = logRow.cells[cell_cacheId].lastChild.firstChild.data.trim();
      var dupChk = document.getElementsByName('UC_R_' + wptId);
      if (dupChk.length > 1) {
        for (var j = 0; j < dupChk.length; j++) {
          var dupRow = dupChk[j].getAttribute('row', '') - 0;
          tabMain.rows[dupRow].cells[cell_logType].setAttribute('class', 'mult_move');
          tabMain.rows[dupRow].cells[cell_logType].title = 'Multiple moves detected ' +
              'for this cache. Sort by GC Code to group them together.';
        }
      }
    }
  }

  // Set default log display by class.
  if (fShowLogText()) {
    GM_addStyle('.cachelogtext { display: block; font-size:95%; padding:3px; }');
  } else {
    GM_addStyle('.cachelogtext { display: none; font-size:95%; padding:3px; }');
  }

  // Generate list of NA entries and add log text.
  var xPathSearch = "//img[contains(@src, 'logtypes/7.png')]";
  var LogList = document.evaluate(
    xPathSearch, document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
  var ic = LogList.snapshotLength;

  // Extract feet and meter data, and add after coordinates.
  for (var i = 0; i < ic; i++) {
    var imgNA = LogList.snapshotItem(i);
    var logNA = imgNA.title.trim();
    if (logNA) {
      var logRow = imgNA.parentNode.parentNode;
      logRow.style.verticalAlign = 'top';
      logRow.cells[cell_coords].removeAttribute('nowrap');
      var logSpan = document.createElement('span');
      logSpan.classList.add('cachelogtext');
      logSpan.appendChild(document.createTextNode(logNA));
      logRow.cells[cell_coords].appendChild(logSpan);
    }
  }

  // Generate list of UC entries and add coordinates and log text.
  var xPathSearch = "//img[contains(@src, 'logtypes/47.png')]";
  var LogList = document.evaluate(
    xPathSearch, document, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
  var ic = LogList.snapshotLength;

  // Extract feet and meter data, and add after coordinates.
  for (var i = 0; i < ic; i++) {
    var imgNA = LogList.snapshotItem(i);
    var logNA = imgNA.title.trim();
    if (logNA) {
      logLines = logNA.split('\n');
      var logText = ''
      for (var j = 8; j < logLines.length; j++) {
        logText += logLines[j];
        if (j < (logLines.length - 1)) {
          logText += '<br>';
        }
      }

      var logRow = imgNA.parentNode.parentNode;
      logRow.style.verticalAlign = 'top';
      logRow.cells[cell_coords].removeAttribute('nowrap');
      logRow.cells[cell_coords].appendChild(document.createElement('br'));

      var logSpan = document.createElement('span');
      logSpan.classList.add('cachelogtext');
      logSpan.innerHTML = logText;
      logRow.cells[cell_coords].appendChild(logSpan);
    }
  }

  // Add listeners to the work column, to create an activity log.
  var totRows = tabMain.rows.length;
  for (var i = 0; i < totRows; i++) {
    tabMain.rows[i].cells[cell_worked].addEventListener('DOMNodeInserted', fWorkChanged, true);
  }

  GM_addStyle("UL.gm-hidden { display:table; visibility:collapse; }");

  var coltblint = window.setInterval(fCollapseLists, 150);
  var hoverTimer = new Array();
  var hoverElement = new Array();

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

  // Place country and state lists in collapsible tables.
  function fCollapseLists() {
    var placeLists = document.getElementsByClassName('selectlist-list');
    if (placeLists.length) {
      clearInterval(coltblint);
      var placeSelect = document.getElementsByClassName('selectlist-select');
      var ic = placeSelect.length;
      for (var i = 0; i < ic; i++) {
        placeSelect[i].setAttribute('hover-index', i);
        placeSelect[i].addEventListener('mouseover', fShowPlaceList, true);
        placeSelect[i].addEventListener('mouseout', fHidePlaceList, true);
      }

      var ic = placeLists.length;
      for (var i = 0; i < ic; i++) {
        hoverElement[i] = placeLists[i];
        placeLists[i].setAttribute('hover-index', i);
        placeLists[i].classList.add('gm-hidden');
        placeLists[i].addEventListener('mouseover', fShowPlaceList, true);
        placeLists[i].addEventListener('mouseout', fHidePlaceList, true);
        var cn = placeLists[i].childNodes.length;
        if (cn) {
          var pnames = '';
          for (var j = 0; j < cn; j++) {
            if (pnames.length) { pnames += ' / '; }
            pnames += (placeLists[i].childNodes[j].firstChild.data);
          }
          placeSelect[i].options[0].firstChild.data = pnames;
          placeSelect[i].title = pnames;
        }
      }
    }
  }

  function fShowPlaceList() {
    var idx = this.getAttribute('hover-index');
    clearTimeout(hoverTimer[idx]);
    var delay = function() { fHoverShowDelay(idx); };
    hoverTimer[idx] = setTimeout(delay, 250);
  }

  function fHoverShowDelay(idx) {
    hoverElement[idx].classList.remove('gm-hidden');
  }

  function fHidePlaceList() {
    var idx = this.getAttribute('hover-index');
    clearTimeout(hoverTimer[idx]);
    var delay = function() { fHoverHideDelay(idx); };
    hoverTimer[idx] = setTimeout(delay, 250);
  }

  function fHoverHideDelay(idx) {
    hoverElement[idx].classList.add('gm-hidden');
  }

  // Locate and return queue table.
  function fFindQueueTable() {
    var difQ = document.getElementsByClassName('QueueTable')[0];
    if (difQ) {
      for (var i = 0; i < difQ.childNodes.length; i++) {
        if (difQ.childNodes[i].nodeName == 'TABLE') {
          return difQ.childNodes[i];
          break;
        }
      }
    }
  }

  // Set customized table spacing.
  function fSetTableSpacing() {
    var curLineHeight = GM_getValue("TableLineHeight_" + SignedInAs, '1.2');
    GM_addStyle(".LogTable { line-height:" + curLineHeight + "em !important; }");
  }

  // Change table spacing when controls clicked.
  function fChangeTableSpacing() {
    var delta = parseFloat(this.getAttribute('adjust', '0'));
    var curLineHeight = parseFloat(GM_getValue("TableLineHeight_" + SignedInAs, '1'));
    curLineHeight = roundNumber(curLineHeight + delta, 3);
    GM_setValue("TableLineHeight_" + SignedInAs, curLineHeight.toString());
    fSetTableSpacing();
  }

  // Round number to specific number of decimal places.
  function roundNumber(num, dec) {
    var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
    return result;
  }

  // Check/uncheck all displayed.
  function fCheckAllNone() {
    if (this.checked) {
      var checks = document.getElementsByClassName('lqueueWorking');
    } else {
      checks = document.getElementsByClassName('undoWorked');
    }

    // Since checkboxes are removed, process in reverse order to maintain indexes.
    var s = checks.length-1;
    var evObjm1 = document.createEvent('MouseEvents');
    for (var i = s; i >= 0; i--) {
      evObjm1.initEvent('click', true, true);
      checks[i].dispatchEvent(evObjm1);
    }
  }

  // Set caches with NA logs to Worked status.
  function fSetToWorked() {
    var numWorked = 0;
    var evObjm1 = document.createEvent('MouseEvents');
    var tabMain = fFindQueueTable();
    if (tabMain) {
      // Determine which link called this function.
      var task = this.getAttribute('task', '');

      // If working dups, create checking array.
      if (task == 'Duplicate') {
        var aDupChk = new Array();
      }

      // Generate list of NA or UC icons within the queue table.
      if (task != 'UcOpened' && task != 'WpOpened') {
        var xPathSearch = ".//img[contains(@src, 'logtypes/7.png')]"; // task == 'NaOpened'
      } else if (task != 'NaOpened' && task != 'WpOpened') {
        var xPathSearch = ".//img[contains(@src, 'logtypes/47.png')]"; // task == 'UcOpened'
      } else {
        var xPathSearch = ".//img";
      }

      var xList = document.evaluate(
        xPathSearch, tabMain, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
      var ic = xList.snapshotLength;

      // Loop through list.
      for (var i = 0; i < ic; i++) {
        var imgx = xList.snapshotItem(i);

        // Find row element.
        var logRow = fUpGenToType(imgx, 'TR');

        // If working dups, get cache id, and see if already in checking array. If it is,
        // mark as a dup. Otherwise, add to checking array.
        if (task == 'Duplicate') {
          var cid = document.getElementById('wptid_' +
              logRow.rowIndex.toString()).firstChild.data;
          var isDup = (aDupChk.indexOf(cid, 0) != -1);
          if (!isDup) {
            aDupChk.push(cid);
          } else {
            logRow.classList.add('Duplicate');
          }
        // If checking NA opened.
        } else if (task == 'NaOpened') {
          var isOpened = (logRow.getElementsByClassName('tag_opened_NA').length > 0);
        // If checking UC opened.
        } else if (task == 'UcOpened') {
          var isOpened = (logRow.getElementsByClassName('tag_opened_UC').length > 0);
        //  If checking WP opened.
        } else if (task == 'WpOpened') {
            var isOpened = (logRow.getElementsByClassName('tag_opened_WP').length > 0);
        //If checking status, get status of cache from class settings.
        } else {
          var cacheCell = logRow.cells[cell_cacheId];
          var stsArc = (cacheCell.getElementsByClassName('OldWarning Strike').length > 0);
          var stsDis = (cacheCell.getElementsByClassName('Strike').length > 0);
        }

        // If it meets any of the criteria, simulate a mouse click on the checkbox.
        if (stsDis || isDup || isOpened) {
          if ((task == 'Archive' && stsArc) || (task == 'Disable' && !stsArc) ||
              isDup || isOpened) {
            var wrkChkBox = logRow.cells[cell_worked].getElementsByTagName('input')[0];
            if (wrkChkBox) {
              evObjm1.initEvent('click', true, true);
              wrkChkBox.dispatchEvent(evObjm1);

              // Increment number of items worked.
              ++numWorked;
            }
          }
        }
      }
    }

    // Display total number of items worked.
    if (task != 'UcOpened' && task != 'WpOpened') {
      fShowNaNotifier(numWorked.toString() + ' Worked');
    } else if (task != 'UcOpened' && task != 'NaOpened') {
            fShowWpNotifier(numWorked.toString() + ' Worked');
        } else {
      fShowUcNotifier(numWorked.toString() + ' Worked');
    }
  }

  // Show NA notifier text.
  function fShowNaNotifier(nText) {
    if (NaTimer) {
      window.clearTimeout(NaTimer);
    }
    document.getElementById('spanNaMessage').firstChild.data = nText;
    NaTimer = window.setTimeout(fRemoveNaNotifier, 2000);
  }

  // Clear NA notifier text.
  function fRemoveNaNotifier() {
    if (NaTimer) {
      window.clearTimeout(NaTimer);
    }
    document.getElementById('spanNaMessage').firstChild.data = '';
  }

  // Show UC notifier text.
  function fShowUcNotifier(nText) {
    if (UcTimer) {
      window.clearTimeout(UcTimer);
    }
    document.getElementById('spanUcMessage').firstChild.data = nText;
    UcTimer = window.setTimeout(fRemoveUcNotifier, 2000);
  }

  // Clear UC notifier text.
  function fRemoveUcNotifier() {
    if (UcTimer) {
      window.clearTimeout(UcTimer);
    }
    document.getElementById('spanUcMessage').firstChild.data = '';
  }

    // Show WP notifier text.
  function fShowWpNotifier(nText) {
    if (WpTimer) {
      window.clearTimeout(WpTimer);
    }
    document.getElementById('spanWpMessage').firstChild.data = nText;
    WpTimer = window.setTimeout(fRemoveWpNotifier, 2000);
  }

  // Clear Wp notifier text.
  function fRemoveWpNotifier() {
    if (WpTimer) {
      window.clearTimeout(WpTimer);
    }
    document.getElementById('spanWpMessage').firstChild.data = '';
  }


  // Open group of caches.
  function fUpArrowClicked() {
    // Get Open Log Book setting, and sequence setting.
    var OpenLogBook = GM_getValue('OpenLogBook_' + SignedInAs, false);
    var ReverseSeq = GM_getValue('ReverseSeq_' + SignedInAs, false);
    var endRow = this.getAttribute('row', 0) - 0;
    var pagetype = this.getAttribute('pagetype', '');
    var aSeqArray = new Array;
    var Url = location.protocol + '//' + document.domain;
    if (!OpenLogBook) {
      Url += (pagetype == 'cache') ? '/seek/cache_details.aspx?guid=' : '/admin/review.aspx?guid=';
    } else {
      Url += (pagetype == 'cache') ? '/seek/cache_logbook.aspx?guid=' : '/admin/review.aspx?guid=';
    }

    // Get positioning settings.
    var PosAtNaR = GM_getValue('OpenNaReviewAt_' + SignedInAs, 'Top');  // Top, Maps, Logs
    var PosAtUcR = GM_getValue('OpenUcReviewAt_' + SignedInAs, 'Top');  // Top, Maps, Logs
    var PosAtNaC = GM_getValue('OpenNaCacheAt_' + SignedInAs, 'Top'); // Top, Logs
    var PosAtUcC = GM_getValue('OpenUcCacheAt_' + SignedInAs, 'Top'); // Top, Logs
    for (var i = endRow; i > 0; i--) {
      var nxtCache = document.getElementById('aUpArrowR_' + i.toString());
      var log_type = this.getAttribute('log_type', '');
      var guid = nxtCache.getAttribute('guid', '');
      if (pagetype == 'review') {
        if (log_type == 'NA' && AutoLitmusNa) { guid += '&AutoLitmus=On'; }
        if (log_type == 'UC' && AutoLitmusUc) { guid += '&AutoLitmus=On'; }
      }

      // Get hash value to position page.
      var vHash = fGetHashValue(log_type, pagetype, PosAtNaR, PosAtUcR, PosAtNaC, PosAtUcC);

      // Add URL to sequencing array.
      if (!ReverseSeq) {
        aSeqArray.unshift(Url + guid + vHash);
      } else {
        aSeqArray.push(Url + guid + vHash);
      }

      // Set indicator to 'opened' status for this cache.
      tabMain.rows[i].cells[cell_worked].classList.add('tag_opened_' +
          nxtCache.getAttribute('log_type', ''));
    }

    for (i in aSeqArray) {
      GM_openInTab(aSeqArray[i]);
    }
  }

  // Determine and return hash value for URL.
  function fGetHashValue(log_type, pagetype, PosAtNaR, PosAtUcR, PosAtNaC, PosAtUcC) {
    var vHash = '';
    if (log_type == 'NA' && pagetype == 'review') {
      if (PosAtNaR == 'Maps') {
        vHash = '#map_canvas';
      } else if (PosAtNaR == 'Logs') {
        vHash = '#log_table';
      }
    } else if (log_type == 'UC' || log_type == 'WP' && pagetype == 'review') {
      if (PosAtUcR == 'Maps') {
        vHash = '#map_canvas';
      } else if (PosAtUcR == 'Logs') {
        vHash = '#log_table';
      }
    } else if (log_type == 'NA' && pagetype == 'cache') {
      if (PosAtNaC == 'Logs') { vHash = '#ctl00_ContentBody_lblFindCounts'; }
    } else if (log_type == 'UC' || log_type == 'WP' && pagetype == 'cache') {
      if (PosAtUcC == 'Logs') { vHash = '#ctl00_ContentBody_lblFindCounts'; }
    }

    return vHash;
  }

  // Tag as opened when links individually clicked.
  function fPageLinkClicked() {
    var myRow = fUpGenToType(this, 'TR');
    if (myRow) {
      var cache_nm = myRow.cells[cell_cacheId].firstChild.name;
      var xLinks = document.getElementsByName(cache_nm);
      var ic = xLinks.length;
      for (var i = 0; i < ic; i++) {
        var xRow = xLinks[i].getAttribute('row', '') -0;
        var xLogType = xLinks[i].getAttribute('log_type', '');
        tabMain.rows[xRow].cells[cell_worked].setAttribute('class', 'tag_opened_' + xLogType);
        tabMain.rows[xRow].cells[cell_worked].title = 'Cache has been opened.';
                console.log('tab opened');
      }
    } else {
      GM_Log('Unable to determine row for clicked link.');
    }
  }

  // Process change in contents of the "work" cell. Add data to activity log.
  function fWorkChanged() {
    var logAction = '';
    if (this.firstChild.nodeName == 'INPUT') {
      logAction = 'Unworked';
    } else if (this.firstChild.nodeName == 'A') {
      logAction = 'Worked';
    }

    if (logAction) {
      var wptId = this.parentNode.cells[cell_cache].firstChild.getAttribute('wptid');
      var logType = this.parentNode.cells[cell_cache].firstChild.getAttribute('log_type');
      var cTitle = this.parentNode.cells[cell_cache].childNodes[1].firstChild.data;
      var logEntry = (fIsoTimeStamp() + ' ' + logAction + ' [' + logType + '] ' +
          wptId + ' - ' + cTitle);
      var isDuplicate = this.classList.contains('Duplicate');
      fSaveLogData(logEntry, wptId, logAction, logType, isDuplicate);
    }
  }

  // Adds log entry to local storage.
  function fSaveLogData(logEntry, wptId, logAction, logType, isDuplicate) {
    var allLogs = window.localStorage.getItem(logStorId);
    if (!allLogs) { allLogs = ''; }
    var arLogs = allLogs.split(logDelim);
    var newLen = arLogs.unshift(logEntry);
    if (newLen > 250) {
      arLogs.splice(250, 250 - newLen);
    }

    allLogs = arLogs.join(logDelim);
    window.localStorage.setItem(logStorId, allLogs);

    // Do not add to sanity checker if working duplicates.
    if ((logAction == 'Worked') && !isDuplicate) {
      var workList = window.sessionStorage.getItem(wrkStorId);
      if (!workList) { workList = ''; }
      var arWorkList = workList.split(',');
      arWorkList.push(logType + wptId);
      workList = arWorkList.join(',');
      window.sessionStorage.setItem(wrkStorId, workList);
    }
  }

  // Show log data.
  function fShowLogData() {
    var divLog = document.getElementById('divLog');
    if (divLog) {
      // Remove div area, if already shown.
      removeNode(divLog);
    } else {
      // Create div area.
      var divHd = document.getElementById('hd');
      divLog = document.createElement('div');
      divLog.id = 'divLog';
      divLog.style.height = '25em';
      divLog.style.overflow = 'auto';
      divLog.style.backgroundColor = 'rgb(255, 255, 221)';
      divLog.style.borderBottomWidth = '1px'
      divLog.style.borderBottomStyle = 'solid';
      divLog.style.borderBottomColor = 'rgb(68, 142, 53)';
      divLog.style.padding = '5px';
      insertAfter(divLog, divHd);
      var lnkClearLog = document.createElement('a');
      lnkClearLog.appendChild(document.createTextNode('Clear All Logs'));
      lnkClearLog.href = 'javascript:void(0)';
      divLog.appendChild(lnkClearLog);
      lnkClearLog.addEventListener("click", fClearLog, true);
      divLog.appendChild(document.createElement('p'));
      // Add logs.
      var allLogs = window.localStorage.getItem(logStorId);
      var arLogs = allLogs.split(logDelim);
      var ic = arLogs.length;
      for (var i = 0; i < ic; i++) {
        divLog.appendChild(document.createTextNode(arLogs[i]));
        divLog.appendChild(document.createElement('br'));
      }
    }
  }

  // Returns boolean true if logs should be displayed, or boolean false if not.
  function fShowLogText() {
    var rtnvar = window.sessionStorage.getItem(viewLogTextId);
    if (!rtnvar) {
      rtnvar = GM_getValue('DspLogs_' + SignedInAs, true);
    }

    return eval(rtnvar);
  }

  function fToggleLogText() {
    var showlogs = !fShowLogText();
    window.sessionStorage.setItem(viewLogTextId, showlogs.toString());
    if (showlogs) {
      GM_addStyle('.cachelogtext { display: block; font-size:95%; padding:3px; }');
    } else {
      GM_addStyle('.cachelogtext { display: none; font-size:95%; padding:3px; }');
    }
  }

  // Clear activity log.
  function fClearLog() {
    var resp = confirm('Clear Activity Log\nThis action cannot be reversed.\n\nAre you sure?');
    if (resp) {
      window.localStorage.removeItem(logStorId);
      var divLog = document.getElementById('divLog');
      removeNode(divLog);
    }
  }

  // Returns a local time ISO style date/time stamp.
  function fIsoTimeStamp() {
    var d = new Date();
    return d.getFullYear() + '-' +
    fPad(d.getMonth() + 1, 2) + '-' +
    fPad(d.getDate(), 2) + 'T' +
    fPad(d.getHours(), 2) + ':' +
    fPad(d.getMinutes(), 2) + ':' +
    fPad(d.getSeconds(), 2) + '.' +
    fPad(d.getMilliseconds(), 3);
  }

  // Pad a number with leading zeros.
  function fPad(number, length) {
    var str = "" + number;
    while(str.length < length) {
      str = '0' + str;
    }

    return str;
  }

  // Returns a URL parameter.
  //  ParmName - Parameter name to look for.
  //  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
  //  UrlString - (optional) String to search. If omitted, document URL is used.
  function UrlParm(ParmName, IgnoreCase, UrlString) {
    var RegRslt, sc = '', RtnVal = '';
    if (IgnoreCase) {sc = 'i'}
    if(UrlString) {
      var PageUrl = UrlString;
    } else {
      PageUrl = document.location + '';
    }

    var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
    var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
    RegRslt = RegEx1.exec(ParmString);
    if (RegRslt) {RtnVal = RegRslt[2]}

    return RtnVal;
  }

  // Move up the DOM tree until a specific DOM type is reached.
  function fUpGenToType(gNode, gType) {
    var gNode;
    var gType = gType.toUpperCase();
    while (gNode.nodeName.toUpperCase() != gType) {
      gNode = gNode.parentNode;
    }

    return gNode;
  }

  // Insert element after an existing element.
  function insertAfter(newElement, anchorElement) {
    anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
  }

  // Insert element aheadd of an existing element.
  function insertAheadOf(newElement, anchorElement) {
    anchorElement.parentNode.insertBefore(newElement, anchorElement);
  }

  // Remove element and everything below it.
  function removeNode(element) {
    element.parentNode.removeChild(element);
  }

  //************************************************************************************//
  //                                                                                    //
  //                 Setting Functions                                                  //
  //                                                                                    //
  //************************************************************************************//

  function fSetOptions() {
    if (!fCreateSettingsDiv('Log Review Queue Script Options')) { return; };

    var divSet = document.getElementById('gm_divSet');

    // Generate one unique DOM name for all setting controls being created.
    settingName = 'sc_' + Math.floor(Math.random()*100000001).toString();

    //****************************************************************************************//
    //                                                                                        //
    //                           Start of Control Creation                                    //
    //                                                                                        //
    // fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt)           //
    //                                                                                        //
    //  vID = Identifier for control, and for saving setting data.                            //
    //  vType = checkbox (input + type.checkbox), text (input), textarea, select              //
    //  vLabel = Text for control label. Above textareas, or to the right of all others.      //
    //  vTitle = Title text for input control and label, or vLabel if not specified.          //
    //  vDftVal = May be text, true/false, or matches a vSelVal array element.                //
    //  vSize = Length of input (text) box, or height of textarea (i.e., '20px').             //
    //  vSelVal = Array of select option values.                                              //
    //  vSelTxt = Array of select option text. If omitted, SelVal values are used.            //
    //                                                                                        //
    //****************************************************************************************//

    GM_addStyle('legend.gmsettings { background: cornsilk; border: solid 2px slategray; border-radius: 8px; padding: 6px; }');
    GM_addStyle('fieldset.gmsettings { background: lightyellow; border-radius: 10px; padding: 5px; border: solid 4px green} ' );

    // =============================== General ===================================== //
    var vDftVal = GM_getValue('UseCompactHeading_' + SignedInAs, true);
    var vLabel = 'Use Compact Headings.';
    var cbUseCompactHeading = fCreateSetting('UseCompactHeading', 'checkbox', vLabel, '', vDftVal);

    vDftVal = GM_getValue('ReverseSeq_' + SignedInAs, false);
    vLabel = 'Reverse the Multi-Open Sequence Order.';
    var cbReverseSeq = fCreateSetting('ReverseSeq', 'checkbox', vLabel, '', vDftVal);

    vDftVal = GM_getValue('OpenLogBook_' + SignedInAs, false);
    vLabel = 'Multi-Open Log Book instead of Cache Page.';
    var cbOpenLogBook = fCreateSetting('OpenLogBook', 'checkbox', vLabel, '', vDftVal);

    fGroupSettings('General Settings', ['UseCompactHeading', 'ReverseSeq', 'OpenLogBook']);
    // ============================================================================= //

    // =============================== Needs Archive =============================== //
    vDftVal = GM_getValue('DspLogs_' + SignedInAs, true);
    vLabel = 'Display Log Text for Need Archive logs.';
    var cbDspLogs = fCreateSetting('DspLogs', 'checkbox', vLabel, '', vDftVal);

    vDftVal = GM_getValue('AutoLitmusNa_' + SignedInAs, true);
    vLabel = 'Auto-Run Litmus for \'Need Archive\' Review Pages.';
    var cbAutoLitmusNa = fCreateSetting('AutoLitmusNa', 'checkbox', vLabel, '', vDftVal);

    vDftVal = GM_getValue('OpenNaReviewAt_' + SignedInAs, 'Top');
    vLabel = 'Position for Multi-Open \'Needs Archive\' Review Pages.';
    var vSelVal = ['Top', 'Maps', 'Logs'];
    var selOpenNaReviewAt = fCreateSetting('OpenNaReviewAt', 'select', vLabel, '', vDftVal, '', vSelVal, '');

    vDftVal = GM_getValue('OpenNaCacheAt_' + SignedInAs, 'Top');
    vLabel = 'Position for Multi-Open \'Needs Archive\' Cache Pages.';
    var vSelVal = ['Top', 'Logs'];
    var selOpenNaCacheAt = fCreateSetting('OpenNaCacheAt', 'select', vLabel, '', vDftVal, '', vSelVal, '');

    fGroupSettings('"Needs Archive" Log Settings', ['DspLogs', 'AutoLitmusNa',
        'OpenNaReviewAt', 'OpenNaCacheAt']);
    // ============================================================================= //

    // =============================== Update Coorinates =========================== //
    vDftVal = GM_getValue('DspDist_' + SignedInAs, true);
    vLabel = 'Display Distances for Update Coordinate logs.';
    var cbDspDist = fCreateSetting('DspDist', 'checkbox', vLabel, '', vDftVal);

    vDftVal = GM_getValue('DistWarn_' + SignedInAs, 100);
    vLabel = 'Numeric Distance in Feet or Meters to Warn of Excessive Move. ' +
        'Leave blank to turn off this function.';
    var txtDistWarn = fCreateSetting('DistWarn', 'text', vLabel, '', vDftVal, '4em');

    vDftVal = GM_getValue('DistType_' + SignedInAs, 'ft');
    vLabel = 'Warning Distance Units.';
    var vSelVal = ['ft', 'm'];
    var vSelTxt = ['Feet', 'Meters'];
    var selDistType = fCreateSetting('DistType', 'select', vLabel, '', vDftVal, '', vSelVal, vSelTxt);

    vDftVal = GM_getValue('AutoLitmusUc_' + SignedInAs, true);
    vLabel = 'Auto-Run Litmus for \'Update Coordinates\' Review Pages.';
    var cbAutoLitmusUc = fCreateSetting('AutoLitmusUc', 'checkbox', vLabel, '', vDftVal);

    vDftVal = GM_getValue('OpenUcReviewAt_' + SignedInAs, 'Top');
    vLabel = 'Position for Multi-Open \'Update Coordinates\' Review Pages.';
    var vSelVal = ['Top', 'Maps', 'Logs'];
    var selOpenUcReviewAt = fCreateSetting('OpenUcReviewAt', 'select', vLabel, '', vDftVal, '', vSelVal, '');

    vDftVal = GM_getValue('OpenUcCacheAt_' + SignedInAs, 'Top');
    vLabel = 'Position for Multi-Open \'Update Coordinates\' Cache Pages.';
    var vSelVal = ['Top', 'Logs'];
    var selOpenUcCacheAt = fCreateSetting('OpenUcCacheAt', 'select', vLabel, '', vDftVal, '', vSelVal, '');

    fGroupSettings('"Update Coordinates" Log Settings', ['DspDist', 'DistWarn',
        'DistType', 'AutoLitmusUc', 'OpenUcReviewAt', 'OpenUcCacheAt']);
    // ============================================================================= //

    //****************************************************************************************//
    //                                                                                      //
    //                            End of Control Creation                                   //
    //                                                                                      //
    //****************************************************************************************//

    fOpenSettingsDiv();
  }

  // Cancle button clicked.
  function fCancelButtonClicked() {
    var resp = confirm('Any changes will be lost. Close Settings?');
    if (resp) {
      fCloseSettingsDiv();
    }
  }

  // Save button clicked. Store all settings.
  function fSaveButtonClicked() {
    // Get array of setting controls.
    var scs = document.getElementsByName(settingName);

    // Process each control.
    var scl = scs.length;
    for (var i = 0; i < scl; i++) {
      var sc = scs[i];
      var rawid = sc.getAttribute('rawid', '');
      var ctrltype = sc.getAttribute('ctrltype', '');
      if (ctrltype == 'checkbox') {
        GM_setValue(rawid + '_' + SignedInAs, sc.checked);
      } else {
        GM_setValue(rawid + '_' + SignedInAs, sc.value);
      }
    }

    fCloseSettingsDiv();
  }

  // Create Div for settings GUI.
  function fCreateSettingsDiv(sTitle) {
    // If div already exists, reposition browser, and show alert.
    var divSet = document.getElementById("gm_divSet");
    if (divSet) {
      var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
      window.scrollTo(window.pageXOffset, YOffsetVal);
      alert('Edit Setting interface already on screen.');
      return false;
    }

    // Set styles for titles and elements.
    GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; ' +
        'text-align: center; margin-bottom: 12px; !important; } ' );
    GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; ' +
        'margin-right: 6px; margin-bottom: 12px; !important; } ' );
    GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; ' +
        'margin-right: 6px; margin-bottom: 6px; !important; } ' );
    GM_addStyle('.SettingLabel {font-weight: bold; margin-left: 6px !important;} ' ); +

    // Create blackout div.
    document.body.setAttribute('style', 'height:100%;');
    var divBlackout = document.createElement('div');
    divBlackout.id = 'divBlackout';
    divBlackout.setAttribute('style', 'z-index: 900; background-color: rgb(0, 0, 0); ' +
        'visibility: hidden; opacity: 0; position: fixed; left: 0px; top: 0px; '+
        'height: 100%; width: 100%; display: block;');

    // Create div.
    divSet = document.createElement('div');
    divSet.id = 'gm_divSet';
    divSet.setAttribute('style', 'position: absolute; z-index: 1000; visibility: hidden; ' +
        'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
    popwidth = parseInt(window.innerWidth * .7);
    divSet.style.width = popwidth + 'px';

    // Create heading.
    var ds_Heading = document.createElement('div');
    ds_Heading.setAttribute('class', 'SettingTitle');
    ds_Heading.appendChild(document.createTextNode(sTitle));
    divSet.appendChild(ds_Heading);

    // Add div to page.
    var toppos =  parseInt(window.pageYOffset +  60);
    var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
    divSet.style.top = toppos + 'px';
    divSet.style.left = leftpos + 'px';
    divSet.setAttribute('YOffsetVal', window.pageYOffset);

    // Add blackout and setting divs.
    document.body.appendChild(divBlackout);
    document.body.appendChild(divSet);
    window.addEventListener('resize', fSetLeftPos, true);

    return true;
  }

  /*
  vID = GetSetting ID, and type + vID is control ID.
  vType = checkbox (input + type.checkbox), text (input), textarea, select
  vLabel = Text for control label.
  vTitle = Title text for input control and label, or vLabel if not specified.
  vDftVal = May be text, true/false, or matches a vSelVal array element.
  vSize = Length of input (text) box, or height of textarea, in pixels.
  vSelVal = Array of select option values.
  vSelTxt = Array of select option text.
  */
  // Create settings from passed data.
  function fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt) {
    var divSet = document.getElementById('gm_divSet');
    var kParagraph = document.createElement('p');
    kParagraph.id = 'p' + vID;
    kParagraph.setAttribute('class', 'SettingElement');
    switch (vType) {
      case 'checkbox':
        var kElem = document.createElement('input');
        kElem.id = 'cb' + vID;
        kElem.type = 'checkbox';
        kElem. checked = vDftVal;
        break;
      case 'text':
        if (!vSize) { vSize = '150px'; }
        var kElem = document.createElement('input');
        kElem.id = 'txt' + vID;
        kElem.style.width = vSize;
        kElem.value = vDftVal;
        kElem.addEventListener('focus', fSelectAllText, true);
        break;
       case 'textarea':
        if (!vSize) { vSize = '80px'; }
        var kElem = document.createElement('textarea');
        kElem.id = 'txtara' + vID;
        kElem.style.width = '100%';
        kElem.style.height = vSize;
        kElem.value = vDftVal;
        break;
      case 'select':
        var kElem = document.createElement('select');
        kElem.id = 'sel' + vID;
        if (vSelVal) {
          if (vSelVal.constructor == Array) {
            for (var i in vSelVal) {
              var kOption = document.createElement('option');
              kOption.value = vSelVal[i];
              kOption.selected = (vSelVal[i] == vDftVal);
              if ((vSelTxt.constructor == Array) && (vSelTxt.length >= i-1)) {
                var kTxtNode = vSelTxt[i];
              } else {
                var kTxtNode = vSelVal[i];
              }
              kOption.appendChild(document.createTextNode(kTxtNode));
              kElem.appendChild(kOption);
            }
          }
        }
        break;
    }

    var kLabel = document.createElement('label');
    kLabel.setAttribute('class', 'SettingLabel');
    kLabel.setAttribute('for', kElem.id);
    kLabel.appendChild(document.createTextNode(vLabel));
    if (!vTitle) { vTitle = vLabel; }
    kElem.title = vTitle;
    kLabel.title = kElem.title;
    kElem.name = settingName;
    kElem.setAttribute('ctrltype', vType);
    kElem.setAttribute('rawid', vID);

    if (vType == 'textarea') {
      kParagraph.appendChild(kLabel);
      kParagraph.appendChild(document.createElement('br'));
      kParagraph.appendChild(kElem);
    } else {
      kParagraph.appendChild(kElem);
      kParagraph.appendChild(kLabel);
    }

    divSet.appendChild(kParagraph);

    return kElem;
  }

  // Routine for creating setting groupings.
  // Use after the settings group has been established.
  // Pass the heading text, and an array of setting IDs.
  // Example:
  // [Create SettingName1 here]
  // [Create SettingName2 here]
  // [Create SettingName3 here]
  // fGroupSettings('Group Heading Text', ['SettingName1', 'SettingName2', 'SettingName3']);
  function fGroupSettings(legendText, aID) {
    var divSet = document.getElementById('gm_divSet');
    var fldset = document.createElement('fieldset');
    fldset.classList.add('gmsettings');
    divSet.appendChild(fldset);
    var fldsetlgd = document.createElement('legend');
    fldsetlgd.classList.add('gmsettings');
    fldset.appendChild(fldsetlgd);
    fldsetlgd.appendChild(document.createTextNode(legendText));
    var ic = aID.length;
    for (var i = 0; i < ic; i++) {
      fldset.appendChild(document.getElementById('p' + aID[i]));
    }
  }

  // Resize/reposition on window resizing.
  function fSetLeftPos() {
    var divSet = document.getElementById('gm_divSet');
    if (divSet) {
      var popwidth = parseInt(window.innerWidth * .7);
      divSet.style.width = popwidth + 'px';
      var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
      divSet.style.left = leftpos + 'px';
    }
  }

  function fOpenSettingsDiv() {
    // Create buttons.
    fCreateSaveCancleButtons();

    // Add blackout and setting divs.
    var divBlackout = document.getElementById('divBlackout');
    var divSet = document.getElementById('gm_divSet');
    divSet.style.visibility = 'visible';
    divBlackout.style.visibility = 'visible';
    var op = 0;
    var si = window.setInterval(fShowBlackout, 40);

    // Function to fade-in blackout div.
    function fShowBlackout() {
      op = op + .05;
      divBlackout.style.opacity = op;
      if (op >= .75) {
        window.clearInterval(si);
      }
    }
  }

  // Create Save/Cancel Buttons.
  function fCreateSaveCancleButtons() {
    var divSet = document.getElementById('gm_divSet');
    var ds_ButtonsP = document.createElement('div');
    ds_ButtonsP.setAttribute('class', 'SettingButtons');
    var ds_SaveButton = document.createElement('button');
    ds_SaveButton = document.createElement('button');
    ds_SaveButton.appendChild(document.createTextNode("Save"));
    ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
    var ds_CancelButton = document.createElement('button');
    ds_CancelButton.style.marginLeft = '6px';
    ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
    ds_CancelButton.appendChild(document.createTextNode("Cancel"));
    ds_ButtonsP.appendChild(ds_SaveButton);
    ds_ButtonsP.appendChild(ds_CancelButton);
    divSet.appendChild(ds_ButtonsP);
  }

  function fCloseSettingsDiv() {
    var divBlackout = document.getElementById('divBlackout');
    var divSet = document.getElementById('gm_divSet');
    divSet.parentNode.removeChild(divSet);
    divBlackout.parentNode.removeChild(divBlackout);
    window.removeEventListener('resize', fSetLeftPos, true);
  }

  function fCancelButtonClicked() {
    var resp = confirm('Any changes will be lost. Close Settings?');
    if (resp) {
      fCloseSettingsDiv();
    }
  }

  function fSelectAllText() {
    this.select();
  }

  // Insert element after an existing element.
  function insertAfter(newElement, anchorElement) {
    anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
  }

  // Insert element ahead of an existing element.
  function insertAheadOf(newElement, anchorElement) {
    anchorElement.parentNode.insertBefore(newElement, anchorElement);
  }