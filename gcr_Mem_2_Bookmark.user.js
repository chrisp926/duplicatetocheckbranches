// ==UserScript==
// @name           GCR Mem 2 Bookmark v2
// @description    Memory To Bookmark
// @namespace      http://www.geocaching.com/admin
// @version        02.02
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Mem_2_Bookmark.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Mem_2_Bookmark.user.js
// @include        http*://*.geocaching.com/bookmarks/view.aspx?*
// @include        http*://*.geocaching.com/bookmarks/bulk.aspx?ListID=*
// @grant          GM_getValue
// @grant          GM_setValue
// @grant          GM_registerMenuCommand
// ==/UserScript==

/*

Function:
 Used cache entries stored to memory by the Review Queue script
 to create bookmark entries. The process is destructive to the
 list of caches, i.e., the entries are removed from the memory
 list when the bookmark entry is created.

Usage:
 If at least one entry is stored, a link will appear on the bookmark
 page, in the Quick Entry box. Clicking the link will start the
 process, which cannot be halted except by turning off Greasemonkey
 (click the monkey icon). A status message will count down the Number
 of remaining entries. When complete, the status message will be gone.

*/

	// Unique ID for cross-script local memory storage.
	var memStorId = '955f75d3-1f69-46bc-89db-28e6242f76e7';

	// Make sure quick-add gadget present.
	var quickAdd = document.getElementById("ctl00_ContentBody_QuickAdd");
	if (!quickAdd) { return; }

	var qaTop = fFindPos(quickAdd)[1] - 7;

	// Run main function.
	fCreateBookmark();

	// Update counter when storage changed.
	window.addEventListener('storage', fCreateBookmark, false);

	// Menu option to create entry box.
	GM_registerMenuCommand('Open Cache List Entry Box', fEnterList);

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Create link or add entry, depending on run mode.
	function fCreateBookmark() {
		var runMode = eval(GM_getValue("BookmarkRunMode", false));
		var wptList = window.localStorage.getItem(memStorId);
		var spanLink = document.getElementById('spanLink');

		if  (!runMode) {
			if (!wptList && spanLink) {
				spanLink.parentNode.removeChild(spanLink);
			}

			if (quickAdd && wptList) {
				if (!document.getElementById('spanLink')) {
					spanLink = document.createElement('span');
					spanLink.id = 'spanLink';
					quickAdd.appendChild(spanLink);
					var goLink = document.createElement('a');
					goLink.href = 'javascript:void(0)';
					goLink.appendChild(document.createTextNode('Create Entries'));
					goLink.addEventListener('click', fStartAdding, true);
					spanLink.appendChild(goLink);
					var spanMemTot = document.createElement('span');
					spanMemTot.id = 'spanMemTot';
					spanMemTot.style.marginLeft = '4px';
					spanMemTot.appendChild(document.createTextNode(""));
					insertAfter(spanMemTot, goLink);
				}
				fUpdateMemoryTotal();
			}
		} else {
			var wptList = window.localStorage.getItem(memStorId);
			if (wptList) {
				var arWpt = wptList.split(',');
				var spanMemTot = document.createElement('span');
				spanMemTot.appendChild(document.createTextNode("Entries Left: " + arWpt.length));
				quickAdd.appendChild(spanMemTot);
				txtEntry = document.getElementsByName('wp')[0];
				txtEntry.value = arWpt.shift();
				window.localStorage.setItem(memStorId, arWpt.join(','));
				var TimeOutID = window.setTimeout(fClickAddBookmark, 150);
				if (!arWpt.length) {
					GM_setValue("BookmarkRunMode", false);
					window.localStorage.removeItem(memStorId);
				}
			}
		}
	}

	// Initiate run mode.
	function fStartAdding() {
		GM_setValue("BookmarkRunMode", true);
		var spanLink = document.getElementById("spanLink");
		if (spanLink) { removeNode(spanLink); }
		fCreateBookmark();
	}

	// Click button to add bookmark entry.
	function fClickAddBookmark() {
		var abButton = document.evaluate("//input[contains(@id,'btnAddBookmark')]",
				document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
		if (abButton) {
			abButton.click();
		}
	}

	// Show memory total.
	function fUpdateMemoryTotal() {
		// Get current storage value, and convert to array.
		var curStore = window.localStorage.getItem(memStorId);
		if (curStore) {
			var memtot = curStore.split(',').length;
		} else {
			memtot = 0;
		}

		var spanMemTot = document.getElementById("spanMemTot");
		spanMemTot.firstChild.data = memtot;
	}

	function fEnterList() {
		var divEntry = document.getElementById('divEntry');
		if (!divEntry) {
			divEntry = document.createElement('div');
			divEntry.id = 'divEntry';
			divEntry.setAttribute('style', 'position: absolute; z-index: 1000; ' +
					'width: 151px; height:342px; ' +
					'top: ' + qaTop + 'px; left: 832px; ' +
					'padding: 3px; border:1px solid rgb(223, 225, 210); ' +
					'background: rgb(235, 236, 237);');

			document.body.appendChild(divEntry);

			var lnkAddEntries = document.createElement('a');
			lnkAddEntries.id = 'lnkAddEntries';
			lnkAddEntries.style.marginLeft = '8px';
			lnkAddEntries.appendChild(document.createTextNode('Add to Memory'));
			lnkAddEntries.href = 'javascript:void(0);';
			lnkAddEntries.addEventListener('click', fSaveToMemory, true);
			divEntry.appendChild(lnkAddEntries);

			var spanCloseEntries = document.createElement('span');
			spanCloseEntries.setAttribute('style', 'float:right;');
			var lnkCloseEntries = document.createElement('a');
			lnkCloseEntries.id = 'lnkCloseEntries';
			lnkCloseEntries.title = 'Close';
			lnkCloseEntries.setAttribute('class', 'noul');
			lnkCloseEntries.appendChild(document.createTextNode("\u2612"));
			lnkCloseEntries.href = 'javascript:void(0);';
			lnkCloseEntries.addEventListener('click', fCloseEntryBox, true);
			spanCloseEntries.appendChild(lnkCloseEntries);
			divEntry.appendChild(spanCloseEntries);

			var divCacheEntry = document.createElement('div');
			divCacheEntry.setAttribute('style', 'position: absolute; ' +
					'width: 136px; height:313px; ' +
					'top: 24px; left: 10px; ' );
			divEntry.appendChild(divCacheEntry);
			var textareaCacheEntry = document.createElement('textarea');
			textareaCacheEntry.id = 'textareaCacheEntry';
			textareaCacheEntry.setAttribute('style', 'position: absolute; ' +
					'width: 133px; height:311px; ' +
					'background: rgb(255, 255, 255);');
			divCacheEntry.appendChild(textareaCacheEntry);
		} else {
			alert('List is already displayed.');
		}
	}

	// Close waypoint entry box.
	function fCloseEntryBox() {
		var divEntry = document.getElementById('divEntry');
		if (divEntry) {
			divEntry.parentNode.removeChild(divEntry);
		}
	}

	// Validate a cache waypoint ID (to 7 characters). Returns True or False.
	function fValidGC(gcid) {
		gcid = gcid.trim();
		var RegEx1 = new RegExp('^GC([1-9]|[A-F])([0-9]|[A-F]){1,3}$', 'i');
		if (RegEx1.test(gcid)) {
			return true;
		} else {
			var RegEx2 = new RegExp('^GC([1-9]|[A-H]|[JKMNPQRT]|[V-Z])([0-9]|[A-H]|[V-Z]|[JKMNTPQR]){3,4}$', 'i');

			return RegEx2.test(gcid);
		}
	}

	// Save caches to memory.
	function fSaveToMemory() {
		var textareaCacheEntry = document.getElementById('textareaCacheEntry');
		var intext = textareaCacheEntry.value;

		// Convert commas, semicolons, and spaces to new-line characters.
		RegEx1 = new RegExp('[ ;,]', 'gm');
		intext = intext.replace(RegEx1, '\n');

		// Remove any blank lines.
		RegEx1 = new RegExp('\\s*\n{2,}', 'gm');
		intext = intext.replace(RegEx1, '\n');

		// Split into array.
		var clst = intext.split('\n');
		var clen = clst.length;
		var badgc = '';

		// Validate array entries.
		for (var i = 0; i < clen; i++) {
			var gc = clst[i].trim();
			if ((!fValidGC(gc)) && (gc.length)) {
				badgc += gc + '\n';
			}
		}

		// If errrors found, display errors.
		if (badgc && intext) {
			alert('The following cache IDs are not valid:\n\n' + badgc);
		// Otherwise, add to memory.
		} else {

			// Get current storage value, and convert to array.
			var curStore = window.localStorage.getItem(memStorId);
			if (curStore) {
				var aCurStore = curStore.split(',');
			} else {
				aCurStore = new Array();
			}

			// Add caches that passed filter to the array.
			var revlinks = document.getElementsByName("revlnk");
			for (var i = 0; i < clen; i++) {
				var gc = clst[i].trim().toUpperCase();
				if ((aCurStore.indexOf(gc) == -1) && gc.length) {
					aCurStore.push(gc);
				}
			}

			// Save array back to storage.
			curStore = aCurStore.join(',');
			window.localStorage.setItem(memStorId, curStore);

			// Clear list and update total;
			textareaCacheEntry.value = '';
			fCreateBookmark();
		}
	}

	// Find true left (index 0) and top (index 1) position of any object.
	// Example: var poslt = fFindPos(obj);
	//  leftpos = poslt[0]; toppos = poslt[1];
	function fFindPos(obj) {
		var curleft = curtop = 0;
		if (obj.offsetParent) {
			do {
				curleft += obj.offsetLeft;
				curtop += obj.offsetTop;
			} while (obj = obj.offsetParent);
			return [curleft,curtop];
		}
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element aheadd of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}
