﻿// ==UserScript==
// @name           GCR Timed Publishing 3 Bookmark Page v2
// @description    Timed Publishing 3 - Bookmark Page
// @version        02.03
// @scriptGuid     8b4a9560-750c-4655-8f24-eb5a39147b87
// @namespace      http://www.geocaching.com/admin
// @grant          GM_addStyle
// @grant          GM_getValue
// @grant          GM_setValue
// @grantZZZ       GM_info
// @grantZZZ       GM_openInTab
// @grantZZZ       GM_xmlhttpRequest
// @grantZZZ       GM_listValues
// @grantZZZ       GM_registerMenuCommand
// @grantZZZ       GM_setClipboard
// @grantZZZ       GM_deleteValue
// @grantZZZ       GM_getResourceText
// @grantZZZ       GM_getResourceURL
// @grantZZZ       GM_log
// @icon           http://i.imgur.com/GP6D2vX.png
// ==/UserScript==
/* 

// This script was retird in March 2018. 
// This script used to run on: @include        http*://*.geocaching.com/bookmarks/view.aspx?guid=*  and   http*://*.geocaching.com/bookmarks/bulk.aspx?ListID=*
// Reasons:
// (1) 98% of cases in which this script was used are now covered by the native Time Publish tool on the review page.
