﻿// ==UserScript==
// @name           GCR Kill Zone v2
// @description    Automates month-end clean-up functions
// @namespace      http://www.geocaching.com/admin
// @version        02.07
// @icon           http://i.imgur.com/0DJCjkY.png
// @require        md5.js
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Kill_Zone.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Kill_Zone.user.js
// @include        http*://*.geocaching.com/seek/log.aspx*
// @include        http*://*.geocaching.com/bookmarks/mark.aspx*
// @include        http*://*.geocaching.com/seek/cache_details.aspx*
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_addStyle
// @grant          GM_registerMenuCommand
// @grant          window.close
// ==/UserScript==

/*

Function:
 Automates month-end clean-up functions.

*/

	GM_registerMenuCommand('Edit "Kill Zone" Script Settings', fEditSettings);

	// NOTICE: Do not edit the text below. This is only default text.
	// Instead, use 'Edit Settings' from the User Script Commands menu.
	var dftRevNote =
			"I noticed that this cache has been temporarily disabled for " +
			"a period of time well in excess of the period of \"a few " +
			"weeks\" as contemplated by the cache guidelines published on " +
			"Geocaching.com. While I feel that Geocaching.com should hold " +
			"the location for you and block other caches from entering " +
			"the area around this cache for a reasonable amount of time, " +
			"we can't do so forever. Please either repair/replace this " +
			"cache, or archive it (using the [i]archive listing[/i] link " +
			"in the upper right) so that someone else can place a cache " +
			"in the area, and geocachers can once again enjoy visiting " +
			"this location." +
			"\n\n" +
			"If you plan on repairing this cache, please [b]log a note to " +
			"the cache[/b] (not email) within the next %D% days so I don't " +
			"archive the listing for non-communication.";

	var dftArcNote =
			"As there's been no cache to find for a long time, I'm archiving " +
			"it to keep it from showing up in search lists, " +
			"and to prevent it from blocking other cache placements. " +
			"\n\n" +			
			"Please note that if geocaches are archived by a reviewer " +
			"or Geocaching HQ for lack of maintenance, " +
			"they are not eligible for unarchival.";

	var KZ_BookMark = GM_getValue("KZ_BookMark", '');
	var RevNote = GM_getValue("KZ_RevNote", dftRevNote);
	var ArcNote = GM_getValue("KZ_ArcNote", dftArcNote);
	var KZ_Days_to_Respond = parseInt(GM_getValue("KZ_Days_to_Respond", "30"));
	var KZ_Auto_Close_Tabs = GM_getValue("KZ_Auto_Close_Tabs", true);

	// Get current domain.
	var domain = thisDomain();

	// Get page url.
	var PageUrl = document.location + "";
	var CacheGUID = '';

	if (PageUrl.match(/.*?\.geocaching\.com\/seek\/cache_details\.aspx/i)) {
		// Exit script if not signed on as a reviewer.
		var revLink = document.evaluate(
				"//a[contains(@href, '/admin/review.aspx?guid=')]",
				document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null
				).singleNodeValue;

		if (!revLink) { return; }

		// Images.
		var imgsrcDisable =
				"data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAA" +
				"Af8%2F9hAAAAsklEQVQ4y2Oora1lQMaPeEVjgXg7EH8G4v9A%2FAGItwBxCL" +
				"ra%2F%2F%2F%2FMyBrlIZq%2FI8HrwNiUQwDoJpfEdAMw8%2BBWA7dgF1Eao" +
				"bhM3ADoH7%2BTwYOhBmwi0wDVsAMeAsSQA9hfBhqwG2YAf%2FJNOADzIBXlL" +
				"rgMJlhsBFmQAKZBvjBDGAGxSuJmg%2BjJyQVElLiE1DKxZYX5IhwyWFYMsYw" +
				"AMmgQGimuQHVdBWIVwOxN9bcCCIowQBpn%2BtjcPrBbwAAAABJRU5ErkJggg" +
				"%3D%3D";

		var imgsrcTrafficCone =
				"data:image/gif;base64,R0lGODlhEAAQALMJAP7u5%2F6yeP5zTv5PLuNQ" +
				"L8EkGP6ZTP%2F%2F%2FwAAAP%2F%2F%2FwAAAAAAAAAAAAAAAAAAAAAAACH5" +
				"BAEAAAkALAAAAAAQABAAAARKMMkpEb0XHYsxKlxHfaGYaMVmVsYBrqcxvKYm" +
				"p2XWHm4%2BIQjZYAj0AA1IAgFYQgAARyVQ%2BgsAkExlgVYJWJGCLfcXBAuK" +
				"IiZ6EgEAOw%3D%3D";

		GM_addStyle(".KZ_Background { background-color: rgb(229, 246, 255); " +
				"margin-right: 10px; font-variant: small-caps; font-weight: bolder; }");

		// Find Navagation table.
		var xPathSearch = "//img[contains(@src, '/images/icons/16/bookmark_list.png')]";
		var ImgList = document.evaluate(
			xPathSearch,
			document,
			null,
			XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
			null);
		var ic = ImgList.snapshotLength;
		if (ic > 0) {
			// Move up the DOM until you find the link. Get cache GUID.
			var tblNav = ImgList.snapshotItem(0).parentNode;
			while (tblNav.nodeName != 'A') {
				tblNav = tblNav.parentNode;
			}

			CacheGUID = UrlParm('guid', true, tblNav.href);

 			// Create Div for Owner Action & Kill Zone, if it doesn't yet exist.
			if (!document.getElementById('oakzDiv')) {
				var oakzDiv = document.createElement('div');
				oakzDiv.id = 'oakzDiv';
				var divBreadcrumbs = document.getElementById("ctl00_divBreadcrumbs");
				insertAfter(oakzDiv, divBreadcrumbs);
			} else {
				oakzDiv = document.getElementById('oakzDiv');
			}

			// Add links to div.
			var spanKill = document.createElement('span');
			spanKill.id = 'spanKill';
			spanKill.style.marginLeft = '20px';

			// Insert ahead of any other children.
			oakzDiv.appendChild(spanKill);
			var lnkWarn = document.createElement("A");
			lnkWarn.name = 'lnkWarn_kz';
			lnkWarn.title = 'Warn a Long-Disabled Cache';
			lnkWarn.setAttribute('class', 'KZ_Background');
			lnkWarn.href = 'https://' + domain + '/bookmarks/mark.aspx?view=legacy&guid=' +
					CacheGUID + '&WptTypeID=2&kz=y';
			spanKill.appendChild(lnkWarn);

			var imgWarn = document.createElement("IMG");
			imgWarn.border = '0';
			imgWarn.align = 'absmiddle';
			imgWarn.src = imgsrcDisable;
			lnkWarn.appendChild(imgWarn);
			lnkWarn.appendChild(document.createTextNode(' Warn Disabled Cache'));
			lnkWarn.addEventListener('click', Warn_clicked, false);

			var lnkKill = document.createElement("A");
			lnkKill.title = 'Archive Long-Disabled Cache';
			lnkKill.setAttribute('class', 'KZ_Background');
			lnkKill.href = 'https://' + domain + '/seek/log.aspx?wid=' +
					CacheGUID + '&LogType=5&kz=y';
			spanKill.appendChild(lnkKill);

			var imgKill = document.createElement("IMG");
			imgKill.border = '0';
			imgKill.align = 'absmiddle';
			imgKill.src = imgsrcTrafficCone;
			lnkKill.appendChild(imgKill);
			lnkKill.appendChild(document.createTextNode(' Kill Disabled Cache'));

			// Add duplicate links at start of logs.
			var oakzDiv2 = document.getElementById('oakzDiv2');
			if (!oakzDiv2) {
				var oakzDiv2 = oakzDiv.cloneNode(false);
				oakzDiv2.id = 'oakzDiv2';
				var logsTable = document.evaluate(
				"//table[contains(@class, 'LogsTable')]",
				document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null
				).singleNodeValue;
				if (logsTable) {
					insertAheadOf(oakzDiv2, logsTable);
				}
			}

			var spanKill2 = spanKill.cloneNode(true);
			oakzDiv2.appendChild(spanKill2);
			var lnkWarn2 = document.getElementsByName('lnkWarn_kz')[1];
			lnkWarn2.addEventListener('click', Warn_clicked, false);
		}
	} else {
		// If not in launched from a Kill Zone Warning link, exit.
		if (UrlParm('kz') != 'y') { return; }

		// If log published (check for Edit button), then close window.
		if (PageUrl.search(/seek\/log\.aspx/gi)>=0) {
			if (document.getElementById("ctl00_ContentBody_LogBookPanel1_lnkBtnEdit")) {
				if (KZ_Auto_Close_Tabs) {
					window.close();
				}
				return;
			}
		}

		// Only apply changes if signed on as a reviewer.
		if (document.getElementById("ctl00_ContentBody_LogBookPanel1_chkCannotDelete") !== null) {
			// Get LogType.
			var LogType = UrlParm('LogType');

			// Function to click the log submit button.
			ClickLogSubmitButton = function() {
				var e_LogBookPanel1_LogButton = document.getElementById("ctl00_ContentBody_LogBookPanel1_btnSubmitLog");
				e_LogBookPanel1_LogButton.click();
			};

			// Get current note text, if any.
			var e_LogBookPanel1_tbLogInfo = document.getElementById("ctl00_ContentBody_LogBookPanel1_uxLogInfo");
			var NoteText = e_LogBookPanel1_tbLogInfo.value;

			// Default 'Cannot Delete' box to checked.
			var e_chkCannotDelete = document.getElementById("ctl00_ContentBody_LogBookPanel1_chkCannotDelete");
			if (e_chkCannotDelete) {
				e_chkCannotDelete.checked=true;
			}

			var subButtons = document.getElementsByName("ctl00$ContentBody$LogBookPanel1$btnSubmitLog");
			for (var subButton in subButtons) {
				subButton.disabled = false;
			}

			// If Archive note.
			if (LogType === '5') {
				if (NoteText === "") {
					e_LogBookPanel1_tbLogInfo.focus;
					e_LogBookPanel1_tbLogInfo.value = ArcNote.replace(/%D%/g, KZ_Days_to_Respond);
					// Start timer function to click Submit button.
					TimeOutID = window.setTimeout(ClickLogSubmitButton, 250);
				}
			}

			// If Review note.
			if (LogType == '68') {
				if (NoteText === "") {
					var wid = UrlParm('wid', true);
					var kzTag = '\n\n{#' + calcMD5('KZ' + wid) + '}';
					e_LogBookPanel1_tbLogInfo.focus; 
					e_LogBookPanel1_tbLogInfo.value = RevNote.replace(/%D%/g, KZ_Days_to_Respond) +
							kzTag;
					// Start timer function to click Submit button.
					TimeOutID = window.setTimeout(ClickLogSubmitButton, 250);
				}
			}
		}

		// If adding to a bookmark, default to the warning bookmark.
		if (PageUrl.search(/bookmarks\/mark\.aspx/gi)>=0) {
			// If bookmark already submitted, close window.
			if (!document.getElementById("ctl00_ContentBody_Bookmark_btnCreate")) {
				if (KZ_Auto_Close_Tabs) {
					window.close();
				}
				return;
			}

			// Function to click the bookmark submit button.
			ClickBookmarkSubmitButton = function() {
				var e_Bookmark_btnSubmit = document.getElementById("ctl00_ContentBody_Bookmark_btnCreate");
				if (e_Bookmark_btnSubmit) {
					e_Bookmark_btnSubmit.click();
				}
			};

			var e_BookmarkList = document.getElementById("ctl00_ContentBody_Bookmark_ddBookmarkList");
			if (e_BookmarkList) {
				e_BookmarkName = document.getElementById("ctl00_ContentBody_Bookmark_tbName");
				if (e_BookmarkName) {
					// Append current date to cache name.
					var IncDays = KZ_Days_to_Respond;
					var d = new Date();
					d.setDate(d.getDate() + IncDays);
					var curr_date = d.getDate(d) + '';
					if (curr_date.length < 2) {curr_date = '0' + curr_date}
					var curr_month = d.getMonth(d) + 1;	// Month is zero indexed.
					curr_month += '';
					if (curr_month.length < 2) {curr_month = '0' + curr_month}
					var curr_year = d.getFullYear(d) + '';
					var NewDate = curr_year  + "-" + curr_month + "-" + curr_date;

					// Insert date before cache name.
					e_BookmarkName.value = NewDate + ' . . . ' + e_BookmarkName.value;
				}

				//// Select bookmark, and submit. //bookmark list was not saved with this code, script stopped
				//for ( var op in e_BookmarkList.options) {
				//	if (op.value == KZ_BookMark) {
				//		op.selected = true;
				//		// Start timer function to click Submit button.
				//		TimeOutID = window.setTimeout(ClickBookmarkSubmitButton, 250);
				//		break;
				//	}
				//}
                // Set bookmark selector. //fix for creating bookmark
                var bmList = document.getElementById('ctl00_ContentBody_Bookmark_ddBookmarkList');
                var ic = bmList.options.length;
                var selId = 0;
                for (var i = 0; i < ic; i++) {
                    if (bmList.options[i].value == KZ_BookMark) {
                        selId = bmList.options[i].value;
                        break;
                    }
                }
                if (selId !== 0) {
                    bmList.value = selId;
                    var e_btnSubmit = document.getElementById("ctl00_ContentBody_Bookmark_btnCreate");
                    sessionStorage.setItem('TimedSubmitted', true);
                    e_btnSubmit.click();
                }
			}
		}
	}

	if (UrlParm('kzwarn', true) == 'y') {
		Warn_clicked();
		window.location.replace(lnkWarn.href);
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Bookmark this cache, in a separate tab.
	function Warn_clicked() {
		GM_openInTab('https://' + domain + '/seek/log.aspx?wid=' +
				CacheGUID + '&LogType=68&kz=y');
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}

	// Show Edit Settings box.
	function fEditSettings() {
		// If not on bookmark page.
		var ddBookmarkList = document.getElementById("ctl00_ContentBody_Bookmark_ddBookmarkList");
		if (!ddBookmarkList) {
			alert("You need to be on a bookmark entry page\n" +
					"to edit this script's settings.\n\n" +
					"Use the 'bookmark listing' link from the cache page.");
			return;
		}

		// If div already exists, reposition browser, and show alert.
		var divSet = document.getElementById("gm_divSet");
		if (divSet) {
			var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			window.scrollTo(window.pageXOffset, YOffsetVal);
			alert('Edit Setting interface already on screen.');
			return;
		}

		// Create blackout div.
		document.body.setAttribute('style', 'height:100%;');
		var divBlackout = document.createElement('div');
		divBlackout.id = 'divBlackout';
		divBlackout.setAttribute('style', 'z-index: 900; background-color: rgb(0, 0, 0); ' +
				'opacity: 0; position: fixed; left: 0px; top: 0px; '+
				'height: 100%; width: 100%; display: block;');

		// Create div.
		divSet = document.createElement('div');
		divSet.id = 'gm_divSet';
		divSet.setAttribute('style', 'position: absolute; z-index: 1000; ' +
				'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		var popwidth = parseInt(window.innerWidth * 0.5);
		divSet.style.width = popwidth + 'px';

		// Create heading.
		var ds_Heading = document.createElement('p');
		ds_Heading.style.fontSize = 'medium';
		ds_Heading.style.fontStyle = 'italic';
		ds_Heading.style.fontWeight = 'bold';
		ds_Heading.style.textAlign = 'center';
		ds_Heading.appendChild(document.createTextNode('Kill Zone - Script Settings'));
		divSet.appendChild(ds_Heading);

		// Create duration selector.
		var ds_DaysP = document.createElement('p');
		ds_DaysP.style.textAlign = 'left';
		ds_DaysP.style.marginLeft = '6px';
		ds_DaysP.style.marginRight = '6px';
		ds_DaysP.appendChild(document.createTextNode("Warning Duration in Days: "));

		var ds_DaysSel = document.createElement("select");
		ds_DaysSel.id = 'ds_DaysSel';
		for (var i = 1; i <= 90; i++) {
			var ds_DaysOpt = document.createElement("option");
			ds_DaysOpt.value = i;
			ds_DaysOpt.id = 'ds_DaysOpt' + i;
			ds_DaysOpt.text = i.toString();
			if (i == KZ_Days_to_Respond) { ds_DaysOpt.selected = true; }
			ds_DaysSel.appendChild(ds_DaysOpt);
		}

		ds_DaysP.appendChild(ds_DaysSel);
		ds_DaysP.appendChild(document.createTextNode(" Use %D% to insert value into text."));
		divSet.appendChild(ds_DaysP);

		// Create warning text area.
		var ds_WarnTextAreaP = document.createElement("p");
		ds_WarnTextAreaP.style.textAlign = 'left';
		ds_WarnTextAreaP.style.marginLeft = '6px';
		ds_WarnTextAreaP.style.marginRight = '6px';
		ds_WarnTextAreaP.appendChild(document.createTextNode("Enter warning text for Reviewer log:"));
		ds_WarnTextAreaP.appendChild(document.createElement('br'));

		var ds_WarnTextArea = document.createElement("textarea");
		ds_WarnTextArea.id = 'ds_WarnTextArea';
		ds_WarnTextArea.style.width = '100%';
		ds_WarnTextArea.style.height = '80px';
		ds_WarnTextAreaP.appendChild(ds_WarnTextArea);
		divSet.appendChild(ds_WarnTextAreaP);
		ds_WarnTextArea.value = RevNote;

		// Create archive text area.
		var ds_ArchiveTextAreaP = document.createElement("p");
		ds_ArchiveTextAreaP.style.textAlign = 'left';
		ds_ArchiveTextAreaP.style.marginLeft = '6px';
		ds_ArchiveTextAreaP.style.marginRight = '6px';
		ds_ArchiveTextAreaP.appendChild(document.createTextNode("Enter Archive log text:"));
		ds_ArchiveTextAreaP.appendChild(document.createElement('br'));

		var ds_ArchiveTextArea = ds_WarnTextArea.cloneNode(true);
		ds_ArchiveTextArea.id = 'ds_ArchiveTextArea';
		ds_ArchiveTextAreaP.appendChild(ds_ArchiveTextArea);
		divSet.appendChild(ds_ArchiveTextAreaP);
		ds_ArchiveTextArea.value = ArcNote;

		// Add bookmark selector to page.
		var ds_BookmarkListP = document.createElement("p");
		ds_BookmarkListP.style.textAlign = 'left';
		ds_BookmarkListP.style.marginLeft = '6px';
		ds_BookmarkListP.style.marginRight = '6px';
		ds_BookmarkListP.appendChild(document.createTextNode("Select Bookmark List to use:"));
		ds_BookmarkListP.appendChild(document.createElement('br'));

		var ds_BookmarkList = ddBookmarkList.cloneNode(true);
		ds_BookmarkList.id = "ds_BookmarkList";
		ds_BookmarkList.name = "ds_BookmarkList";
		ds_BookmarkList.setAttribute('style', '');

		//SatelliteSuzy: add 5 lines below
		ds_BookmarkList.disabled = false;
		ds_BookmarkList.options[0].removeAttribute("selected");
		for (var optie = 0; optie < ds_BookmarkList.options.length; optie++) {
			if (ds_BookmarkList.options[optie].value == KZ_BookMark) {
				ds_BookmarkList.options[optie].selected = true;
			}
		}
		//SatelliteSuzy: remove lines below
		/*
		for (var op in ds_BookmarkList.options) { //this for look is probably no longer needed
			if (op.value == KZ_BookMark) { op.selected = true; }
		}
		*/

		ds_BookmarkListP.appendChild(ds_BookmarkList);
		divSet.appendChild(ds_BookmarkListP);

		// Create auto-close checkbox.
		var ds_AutoCloseP = document.createElement("p");
		ds_AutoCloseP.style.textAlign = 'left';
		ds_AutoCloseP.style.marginLeft = '6px';
		ds_AutoCloseP.style.marginRight = '6px';

		var ds_AutoCloseLabel = document.createElement("label");
		ds_AutoCloseLabel.style.fontWeight = 'normal';
		ds_AutoCloseLabel.setAttribute('for', 'ds_AutoCloseCB_KZ');
		ds_AutoCloseLabel.appendChild(document.createTextNode("Auto Close Tabs on Completion:"));
		ds_AutoCloseP.appendChild(ds_AutoCloseLabel);

		var ds_AutoCloseCB = document.createElement("input");
		ds_AutoCloseCB.id = "ds_AutoCloseCB_KZ";
		ds_AutoCloseCB.name = "ds_AutoCloseCB";
		ds_AutoCloseCB.type = "Checkbox";
		ds_AutoCloseCB.style.marginLeft = '6px';
		ds_AutoCloseCB.checked = KZ_Auto_Close_Tabs;
		ds_AutoCloseP.appendChild(ds_AutoCloseCB);

		var ds_AutoCloseInfo = document.createElement("a");
		ds_AutoCloseInfo.style.fontSize = 'x-small';
		ds_AutoCloseInfo.style.marginLeft = '9px';
		ds_AutoCloseInfo.href = 'https://sites.google.com/site/allowscriptstoclosetabs/';
		ds_AutoCloseInfo.title = 'Click to view instructions in a new tab.';
		ds_AutoCloseInfo.target = '_blank';
		ds_AutoCloseInfo.appendChild(document.createTextNode("(Requires configuration change)"));
		ds_AutoCloseP.appendChild(ds_AutoCloseInfo);

		divSet.appendChild(ds_AutoCloseP);

		// Create buttons.
		divSet.appendChild(document.createElement('p'));
		var ds_ButtonsP = document.createElement('p');
		ds_ButtonsP.style.textAlign = 'right';

		var ds_SaveButton = document.createElement('button');
		ds_SaveButton = document.createElement('button');
		ds_SaveButton.appendChild(document.createTextNode("Save"));
		ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);

		var ds_CancelButton = document.createElement('button');
		ds_CancelButton.style.marginLeft = '6px';
		ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		ds_ButtonsP.appendChild(ds_SaveButton);
		ds_ButtonsP.appendChild(ds_CancelButton);
		divSet.appendChild(ds_ButtonsP);

		// Add div to page.
		var toppos =  parseInt(window.pageYOffset +  60);
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.top = toppos + 'px';
		divSet.style.left = leftpos + 'px';
		divSet.setAttribute('YOffsetVal', window.pageYOffset);

		// Resize/reposition on window resizing.
		function fSetLeftPos() {
			var divSet = document.getElementById('gm_divSet');
			if (divSet) {
				var popwidth = parseInt(window.innerWidth * 0.5);
				divSet.style.width = popwidth + 'px';
				var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
				divSet.style.left = leftpos + 'px';
			}
		}

		// Add blackout and setting divs.
		document.body.appendChild(divBlackout);
		document.body.appendChild(divSet);
		window.addEventListener('resize', fSetLeftPos, true);
		var op = 0;
		var si = window.setInterval(fShowBlackout, 40);

		// Function to fade-in blackout div.
		function fShowBlackout() {
			op = op + 0.05;
			divBlackout.style.opacity = op;
			if (op >= 0.75) {
				window.clearInterval(si);
			}
		}

		// Save values.
		function fSaveButtonClicked() {
			KZ_Days_to_Respond = ds_DaysSel.options[ds_DaysSel.selectedIndex].value;
			GM_setValue("KZ_Days_to_Respond", KZ_Days_to_Respond);
			RevNote = ds_WarnTextArea.value;
			GM_setValue("KZ_RevNote", RevNote);
			ArcNote = ds_ArchiveTextArea.value;
			GM_setValue("KZ_ArcNote", ArcNote);
			KZ_BookMark = ds_BookmarkList.options[ds_BookmarkList.selectedIndex].value;
			GM_setValue("KZ_BookMark", KZ_BookMark);
			KZ_Auto_Close_Tabs = ds_AutoCloseCB.checked;
			GM_setValue("KZ_Auto_Close_Tabs", KZ_Auto_Close_Tabs);
			alert('Settings have been saved.');
			removeNode(divSet);
			removeNode(divBlackout);
			window.removeEventListener('resize', fSetLeftPos, true);
		}

		// Cancel requeste.
		function fCancelButtonClicked() {
			var resp = confirm('Cancel requested. You will lose any changes.\n\n' +
					'Press OK to exit without saving changes. Otherwise, press Cancel.');
			if (resp) {
				removeNode(divSet);
				removeNode(divBlackout);
				window.removeEventListener('resize', fSetLeftPos, true);
			}
		}
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

	// Returns raw current domain ('www.geocaching.com' for example).
	function thisDomain() {
		var wDomain = document.location.href + '';
		var RegEx1 = new RegExp('^.*?:\/\/(.*?\..*?)($|\/)');
		RegRslt = RegEx1.exec(wDomain);
		wDomain = RegRslt[1];
		return wDomain;
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element aheadd of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}