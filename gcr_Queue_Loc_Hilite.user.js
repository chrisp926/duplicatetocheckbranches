﻿// ==UserScript==
// @name           GCR Queue Location Highlighter v2
// @description    Highlight Queue Entries Based On Location
// @namespace      http://www.geocaching.com/admin
// @version        02.01
// @icon           http://i.imgur.com/MleOpfw.png
// @run-at         document-end
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Queue_Loc_Hilite.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Queue_Loc_Hilite.user.js
// @include        http*://*.geocaching.com/admin/queue.aspx*
// @grant          GM_getValue
// @grant          GM_setValue
// @grant          GM_addStyle
// ==/UserScript==

/*

Function:
 Adds a specified background color to the first cell of review
 queue entries, based on the latitude or longitude of the cache.

Usage:
 Settings are passed through the URL, so users can create multiple
 bookmarks for various settings. The following parameters are valid:

	To define a band of a territory by either latitude or longitude:
		frl = From Latitude/Longitude, in signed degree-decimal format.
		tol = To Latitude/Longitude, in signed degree-decimal format.
		ltype = Either "lat" or "lon", indicating if the above values are
			latitude or longitude.

	To define a rectangle:
		nw = Northwest coordinates of the rectangle, in signed degree-decimal
			format, separated by a comma. i.e., nw=34.45215,-91.65741
		se = Southeast coordinates of the rectangle, same specs as above.

	Other settings that apply to both methods:
		bc = Background color. Can be either a named color, or an HTML-type
			color value, passed as a 6 hexadecimal value, without a leading "#*.
			If omitted, a light yellow (hex #FFFFB0) is used.
		invert = Either "y" (yes) or "n" (no). If set to "y", it inverts the
			normal operation, and all entries *outside* the specified
			boundary will be highlighted. If omitted, or any value other
			than "y" is used, a value of "n" is assumed, and the script
			functions normally.

Example: &frl=-90.0&tol=-96.5&ltype=lon
	Defines a band between longitudes -90.0 and -96.5

Example: &nw=30.8,-97.4&se=25.7,-95.3&bc=palegreen
	Defines a rectangle with the northwest corner at 30.8,-97.4 and the
	southeast corner at 25.7,-95.3, with palegreen used as the
	highlight background color.

Notes:
	The specified boundary is inclusive, meaning any caches exactly on a
	boundary line is considered within the boundary range. This makes the
	"invert" parameter handy, in that it is non-inclusive. If, for example,
	two people are managing the same area, they can use the same "frl"
	and "tol" setting, with only the "invert" setting different, and be
	assured that there will be no overlaps or omissions.

*/

	var unhlShowSrc =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAx9JREFUeNpMlF9IU1Ec" +
		"x7%2B797rpBraY7aY2jVk6G2qQFGImtoqUVCgEC%2BdjT1FQRA9JPfXUo9pbQRDuwWqKVG" +
		"BUJEWUltioTLe5obktncnm3J%2Fu3e13rmUe%2BN7zu%2Becz%2Fece37nXE17ezs0Gg04" +
		"jlNr0g5SJQCRlI%2BNEiNFFEX5RloiIZvNgtUC%2Fhcd6WBpaWl9R0fHOavVWiVJktohCA" +
		"J8Pp9ncHBwIBgMvqWmD6Q06%2BNtNhublcEnWltbu5xO55W36XHRFXajN3QPg0sjmI7NQt" +
		"ALotNx9hitlJ%2BZmWFwkCTzFRUVbCmH29raupqamrovenswwXugmDmYxAIYC7YjlpvA%2" +
		"B%2FgkXobf4HxNdw3P8xKZLDETTpZlsaSkpMHhOOq8NHsdGZMEo9GIUCiEB7t6VYXDYbVt" +
		"3ZjGZf9NNDY2OouLixsYywzsZ86c7hyaf4JVfRwK2UajUaRSqc3NYfHKyooaL2l%2F4Wlk" +
		"FC0tzZ2MFehRZLFY7He%2Fu5BXqMfCwoI6MJ1OY3l5GclkEoFgQN11VnaKIj7RJx4prLMz" +
		"lhkY4%2FE1eFJfYc4WYdwxikgkgrW1NdUgJycHE47naiaYDowdg4f%2FSsYpMFagVLHPgE" +
		"wp%2B5Vchdfr3cgvDWbwP%2FCf0hyBkoHBYCwziPO8gEq%2BHB%2BTX1A336IuVSNr8Lnh" +
		"tQrte1UPhVc2zkQuj0qUU8QxgzibfXF21ju931ADOZZWz1%2FGlIG0TdqclcWsjUmb4VCj" +
		"r8bcXGCasczgs8vlcjeLJ2FJFUGb4KHo6ahqs5sGLJbzZOSuC9grWXHcdBzDw243YzlaRs" +
		"jv944NDT1%2B1F91B3t%2BWqDzKZA1GRT77BC%2FV6hxrh%2BwRa3otfdjZGT40dycf4yx" +
		"vNlsZin6MTU1yVGmuBunbtnE3wXQLtLt8QShCwBHlUZ07ezGtaoeuFz3hx4%2BHHDTkX5G" +
		"GyFpqqur1RzHYrE8yv2JsrJyx4ULV1tqa%2BvKtlw0TEy88%2FX13X7q88280Ol0o%2Fn5" +
		"%2BUn1Bm8xQCKRQCaTsdD7IWJ2k0x%2F%2BSgpQMB7rVY7bzAYQAbqL%2BCPAAMAVUOTme" +
		"%2B7FysAAAAASUVORK5CYII%3D";

	var unhlHideSrc =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAvRJREFUeNp8VF1IU2EY" +
		"frazOXRrFanHv23iQmf%2BhGKKpck0RAL1yoJw3ZT9kN0U3irYZV2K913kQM3pTUEQpFg2" +
		"ndQsaMxOOmTTUNP9%2F3S29X6nIhPqg4dzzve9z%2FP%2BfkfW3d0NmUwGuVwuPQk5hHIA" +
		"PEGLnytA%2BJpOpz8RtglIpVJgTwX%2BLBWh3mAwnOvp6blSUlJSJYqidKBQKCAIwoeJiY" +
		"kxj8fzmrYWCXF2xplMJuaVkds7Ozt7LRbLfdHp5LfGx7E5OoqdqSkEBQEatZpvvXzpAkXK" +
		"ud1uRvYQklxZWRkLpamrq6vXbDZfXRsagnJ%2BHgbyasjPR2F2NjSBAPxzc9hyLKPK0nua" +
		"4ziRRLaZiDyZTPJ6vb65ra3VIgwNIo%2BM83ge4XAYOzs7EiKRCHjay97dxfrwMFpaWiyF" +
		"hYXNjMsZjcaGvr7rA2GHIzdr4S0yVSrskmEsFvsLoVAISqUSic0tRI5qoaut5e32xWUFqR" +
		"TodLoK95MxFGs08Hq9UuG%2BJxKS5whFkqD33wUtKCoClhzIv3WzgnGZwLFgMISo0wkxJw" +
		"eNs7Pw%2BXzY398H5Qq1Wg0NCWdkZEgCr2pqkCbbaDQGxlWQMqsDROpr1O%2BHy%2BWSvL" +
		"FwD5OltlE0MbKVOMRlAkGOU0BZUYmYYwkesxn%2FW1kkxmwBORMIMu%2B%2B1dXPLk1dPf" +
		"aoWHoqYgHL9R%2BIUlpZtWewtrbuYlwmsGK1Wm3ZTS2IV1bDTwZHaKw1ZHwYQUorRY6ONz" +
		"ZhZsZmY1wuNzc3tLf3TQnIMs%2FfuXfqy8cVbG94oGbhSoESkbBKCDecRfmDR5iennxqt7" +
		"%2Bx0QC%2BZwLsYnidzndyqo28Y2DQhJOl8HBKLHs34FYoIbZ2oODabZTeuAur9fH05OSY" +
		"jUb6OWmKsurqaulmBQKBzHg83m40lrb19w9crKtrNB4snsOxIIyMPHwmCO6XKpXqhVarjU" +
		"o3%2BICANL40NDr6biBOMeHEL%2F4uYZ0IdmrpBmsvCUi%2FgB8CDACXUnTearO2hAAAAA" +
		"BJRU5ErkJggg%3D%3D";

	GM_addStyle("img.smlbuttons {vertical-align:text-bottom;}");
	GM_addStyle("a.smlbuttonslnk {margin-left:7px;}");
	GM_addStyle("a.disabled {pointer-events: none; cursor: default; opacity:.4}");

	// Get currently signed-on geocaching.com profile.
	var SignedInAs = document.getElementById("ctl00_LoginUrl");
	if (!SignedInAs) { SignedInAs = document.getElementById('ctl00_LogoutUrl'); }
	var SignedInAs = SignedInAs.parentNode.childNodes[1].firstChild.data.trim();
	SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');

	var hideHlCaches = eval(GM_getValue('hideHlCaches_' + SignedInAs, false));

	// Globals.
	var AllLinks, xLink;

	// Get passed parms.
	var frl = UrlParm('frl', true);
	var tol = UrlParm('tol', true);
	var ltype = UrlParm('ltype', true);
	var bc = UrlParm('bc', true);
	var invert = UrlParm('invert', true);
	var nwLatLon = UrlParm('nw', true);
	var seLatLon = UrlParm('se', true);

	// If essential parms not found, exit.
	if (((ltype == '') && (frl == '') && (tol == '')) && (!nwLatLon || !seLatLon)) {return}

	// Alpha parms to lower case.
	ltype = ltype.toLowerCase();
	invert = invert.toLowerCase();

	// Defaults if omitted.
	if (frl == '') {frl = 0}
	if (tol == '') {tol = 0}
	if (bc == '') {bc = '#FFFFB0'}
	invert = (invert == 'y');	// Convert to a boolean value.

	// If hex code passes as background color...
	if (bc.length == 6) {
		if (bc.match(/^[0-9a-f]{6}$/i)) {
			bc = '#' + bc.toLowerCase();
		}
	}

	// Validations.
	if (ltype) {
		if (ltype != 'lat' && ltype != 'lon') {
			alert('The "ltype" parameter value of "' + ltype +
					'" in the ULR is invalid. \nValue needs to be "lat" or "lon".');
			return;
		}

		if (!IsNumeric(frl)) {
			alert('The "frl" parameter value of "' + frl +
					'" in the ULR is invalid. \nValue needs to be numeric.');
			return;
		}

		if (!IsNumeric(tol)) {
			alert('The "tol" parameter value of "' + tol +
					'" in the ULR is invalid. \nValue needs to be numeric.');
			return;
		}

		// Make sure lat/lon values are typed as numeric.
		frl = frl - 0;
		tol = tol - 0;

		// If range is backwards, silently swap it.
		if (tol < frl) {
			var swap = tol;
			tol = frl;
			frl = swap;
		}
	}

	// Separate corner coordinates into lat and lon values.
	if (nwLatLon && seLatLon) {
		var vArr = nwLatLon.split(',');
		var nwLat = vArr[0] - 0;
		var nwLon = vArr[1] - 0;
		vArr = seLatLon.split(',');
		var seLat = vArr[0] - 0;
		var seLon = vArr[1] - 0;
	}

	// Highlight if queue script loaded, or set timer to check for script being loaded.
	var timCounter = 0;
	if (document.getElementById('p_ecFilterMode')) {
		fAddShowHideControls();
		fHighlight();
	} else {
		var intTimer = self.setInterval(fCheckForQscript, 100)
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Interval function to check for load of queue script.
	function fCheckForQscript() {
		if (document.getElementById('p_ecFilterMode')) {
			self.clearInterval(intTimer);
			fAddShowHideControls();
			fHighlight();
		} else {
			timCounter++;
			if (timCounter >= 20) {
				self.clearInterval(intTimer);
				fAddShowHideControls();
				fHighlight();
			}
		}
	}

	// Add controls to show/hide non-highlighted caches.
	function fAddShowHideControls() {
		var spanTblSizer = document.getElementById("spanTblSizer");
		if (spanTblSizer) {
			spanShowHideHL = document.createElement('span');
			spanShowHideHL.id = 'spanShowHideHL';
			spanShowHideHL.style.marginLeft = '8px';
			insertAfter(spanShowHideHL, spanTblSizer);
			var lnkHideHL = document.createElement('a');
			lnkHideHL.title = 'Hide Unhighlighted Caches.';
			lnkHideHL.id = 'lnkHideHL';
			lnkHideHL.href = 'javascript:void(0)';
			var imgHideHL = document.createElement('img');
			imgHideHL.src = unhlHideSrc;
			lnkHideHL.classList.add('smlbuttonslnk');
			imgHideHL.classList.add('smlbuttons');
			lnkHideHL.appendChild(imgHideHL);
			var lnkShowHL = document.createElement('a');
			lnkShowHL.id = 'lnkShowHL';
			lnkShowHL.title = 'Show Unhighlighted Caches.';
			lnkShowHL.href = 'javascript:void(0)';
			var imgShowHL = document.createElement('img');
			imgShowHL.src = unhlShowSrc;
			lnkShowHL.classList.add('smlbuttonslnk');
			imgShowHL.classList.add('smlbuttons');
			lnkShowHL.appendChild(imgShowHL);
			spanShowHideHL.appendChild(lnkHideHL);
			spanShowHideHL.appendChild(lnkShowHL);

			if (hideHlCaches) {
				lnkHideHL.classList.add('disabled');
			} else {
				lnkShowHL.classList.add('disabled');
			}

			lnkShowHL.addEventListener('click', fToggleShowHideHL, false);
			lnkHideHL.addEventListener('click', fToggleShowHideHL, false);
		}
	}

	function fToggleShowHideHL() {
		hideHlCaches = !hideHlCaches;
		GM_setValue('hideHlCaches_' + SignedInAs, hideHlCaches);
		document.getElementById('lnkShowHL').classList.toggle('disabled');
		document.getElementById('lnkHideHL').classList.toggle('disabled');
		var cbEcFilterMode = document.getElementById("cbEcFilterMode");
		if (!hideHlCaches) {
			var allUnhilited = document.getElementsByClassName('unhilited');
			var ic = allUnhilited.length -1;
			for (var i = ic; i >= 0; i--) {
				var xRow = allUnhilited[i];
				var xID = xRow.id;
				xID = xID.replace('cacheRow', 'revlnk');
				var rLink = document.getElementById(xID);
				rLink.removeAttribute('hl_filtered');
				if (!(rLink.getAttribute('earthcache') == 'true' && cbEcFilterMode.checked)) {
					xRow.style.visibility = 'visible';
				}
				xRow.classList.remove('unhilited');
			}
		}

		fHighlight();
	}

	// Perform hightlighting.
	function fHighlight() {
		// Get collection of links.
		var AllLinks = document.getElementsByClassName("CacheData");
		var maxR = AllLinks.length

		// Loop through cache list.
		for (var i = 0; i < maxR; i++) {
			var xLink = AllLinks[i];
			if (ltype) {
				// Get map coordinate. Make sure it's typed as numeric.
				if (ltype == 'lat') {
					var chkVal = AllLinks[i].getAttribute('data-latitude') - 0;
				} else {
					chkVal = AllLinks[i].getAttribute('data-longitude') - 0;
				}

				// If within range (or out of range and inverted),
				// change row background color.
				var inside = ((chkVal >= frl) && (chkVal <= tol));
			} else if (nwLatLon && seLatLon) {
				var mapLat = AllLinks[i].getAttribute('data-latitude') - 0;
				var mapLon = AllLinks[i].getAttribute('data-longitude') - 0;
				inside = (fBetween(nwLat, seLat, mapLat, true) &&
						fBetween(nwLon, seLon, mapLon, true));
			}

			var xRow = xLink.parentNode.parentNode;
				var xID = xRow.id;
				xID = xID.replace('cacheRow', 'revlnk');
				var rLink = document.getElementById(xID);
			if ((inside && !invert) || (!inside && invert)) {
				var kids = xRow.getElementsByTagName('TD');
				kids[0].style.backgroundColor = bc;
				xRow.classList.add('hilited');
			} else {
				xRow.classList.add('unhilited');
				if (hideHlCaches) {
					rLink.setAttribute('hl_filtered', 'true');
					xRow.style.visibility = 'collapse';
				}
			}
		}
		var hitot = document.getElementsByClassName('hilited');
		var unhitot = document.getElementsByClassName('unhilited');
		var thing = document.getElementsByClassName('PageBuilderWidget')[0];
		thing.title =  'Highlighted:' + hitot.length + ' Unhighlighted:' + unhitot.length ;
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}

		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}

		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}

		return RtnVal;
	}

	// Checks for a valid numeric string.
	// Checks for 0-9, single decimal point, and leading negative sign.
	function IsNumeric(sText) {
		var Char, decpoint = false, ValidChars = "-0123456789.";
		for (i = 0; i < sText.length; i++) {
			Char = sText.charAt(i);
			if (ValidChars.indexOf(Char) == -1) {return false}
			if (ValidChars.indexOf(Char) == 11) {
				if (decpoint) {return false}
				decpoint = true;
			}
			if (ValidChars.indexOf(Char) == 0) {
				if (i != 0) {return false}
			}
		}

		return true;
	}

	// Return true or false, if value between limits.
	function fBetween(limit1, limit2, val, inclusive) {
		if (inclusive == null) { inclusive = false; }

		if (limit1 < limit2) {
			var lo = limit1;
			var hi = limit2;
		} else {
			lo = limit2;
			hi = limit1;
		}

		if (inclusive) {
			var result = (lo <= val && hi >= val);
		} else {
			var result = (lo < val && hi > val);
		}

		return result;
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element ahead of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}
