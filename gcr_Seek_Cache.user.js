// ==UserScript==
// @name           GCR Seek Cache v2
// @description    Search Results Page Enhancements for Reviewers
// @namespace      http://www.geocaching.com/admin
// @version        03.01
// @icon           http://i.imgur.com/a94PYHJ.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Seek_Cache.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Seek_Cache.user.js
// @include        http*://*.geocaching.com/play/search*
// @grant          GM_addStyle
// ==/UserScript==

/*
Change history:
 * 3.01		10/22/2021	Adjusted script to work on staging
 * 3.00 	05/05/2021	Adjusted script to work on new search output

Function:
 Changes Cache Page link to Review Page link on search results

*/

// Create review page URLS based on existing cache page URLS.
let revPageUrls = [];
let links = document.getElementsByTagName('a');

const regex = RegExp('https?:\/\/(?:(www)|(?:staging))\.geocaching.com\/geocache\/GC*');
for(let i=0, max=links.length; i<max; i++) {
	if(regex.test(links[i].href) === true) {
		let tempUrl = links[i].href.replace(/geocache\//g, 'admin/review.aspx?wp=');
		revPageUrls.push(tempUrl);
	}
};

// Add review page links to page & have links open in new tab.
let cacheLinkBoxes = document.getElementsByClassName('mobile-show cache-primary-details');
for (let i=0, max=cacheLinkBoxes.length; i<max; i++) {
    let revPageDiv = document.createElement('div');
    revPageDiv.style.padding = '5px';
	cacheLinkBoxes[i].appendChild(revPageDiv);
	let revPageLink = document.createElement('a');
	revPageLink.innerHTML = 'REVIEW PAGE';
	revPageLink.setAttribute('href', revPageUrls[i]);
	revPageLink.setAttribute('class', 'review_link');
	revPageLink.target = '_blank';
	revPageLink.title = 'Open Review Page';
	revPageDiv.appendChild(revPageLink);
};