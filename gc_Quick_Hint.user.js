// ==UserScript==
// @name           GCR Quick Hint v2
// @description    Quick Hint Decoding
// @namespace      http://www.geocaching.com/admin
// @version        02.01
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gc_Quick_Hint.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gc_Quick_Hint.user.js
// @include        http*://*.geocaching.com/seek/cache_details.aspx*
// ==/UserScript==

/*
Function:
  Decodes hint without reloading cache page. Does not reveal any encoded spoilers in logs.
*/

	// Reassign decrypt link to perform decryption, instead of reloading page.
	var e_Encrypt = document.getElementById("ctl00_ContentBody_Encrypt");
	if (e_Encrypt.href != '') {
		e_Encrypt.href = 'javascript:void(0)';
		e_Encrypt.addEventListener("click", rot13, true);
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	function rot13() {
		// ROT13 decoding string.
		var xlr13 = 'abcdefghijklmnopqrstuvwxyzabcdefghijklm' +
				'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLM'

		// Plaintext flag.
		var pt = false;

		// Toggle link label.
		var label = this.firstChild.data;
		if (label == 'Encrypt') {
			label = 'Decrypt';
		} else if (label == 'Decrypt') {
			label = 'Encrypt';
		}
		this.firstChild.data = label;

		// Get each hint line, and perform ROT13 encoding.
		var e_Hints = document.getElementById("ctl00_ContentBody_Hints");
		var xPathSearch = ".//text()";
		var SpanList = document.evaluate(
				xPathSearch,
				e_Hints,
				null,
				XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
				null);

		var ic = SpanList.snapshotLength;
		for (var i = 0; i < ic; i++) {
			xItem = SpanList.snapshotItem(i);
			var line = xItem.data;
			var kc = line.length;
			var r13line = '';
			for (var k = 0; k < kc; k++) {
				c = line.charAt(k);
				if (c == '[') {
					pt = true;
				} else if (c == ']') {
					pt = false;
				} else {
					var idx = xlr13.indexOf(c);
					if (idx >= 0 && !pt) {
						c = xlr13.charAt(idx + 13);
					}
				}
				r13line += c;
			}
			xItem.data = r13line;
		}
	}
